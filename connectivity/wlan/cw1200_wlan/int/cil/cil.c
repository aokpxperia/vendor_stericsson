/*===========================================================================
*
*Linux Driver for CW1200 series
*
*
*Copyright (c) ST-Ericsson SA, 2010
*
*This program is free software; you can redistribute it and/or modify it
*under the terms of the GNU General Public License version 2 as published
*by the Free Software Foundation.
*
*===========================================================================*/
/**
*\addtogroup Linux Driver CIL Layer
*\brief
*
*/
/**
*\file cil.c
*- <b>PROJECT</b>				: CW1200_LINUX_DRIVER
*- <b>FILE</b>				: cil.c
*\brief
*This module interfaces with the Linux Kernel CFG80211 layer.
*\ingroup CIL
*\date 25/02/2010
*/


/*****************************************************************************
*						INCLUDE FILES
******************************************************************************/

#include "cil.h"
#include "eil.h"  /*For EIL_Init_Complete */
#include <linux/proc_fs.h>
#include <net/cfg80211.h>
#include <linux/ieee80211.h>
#include <net/netlink.h>
#include <linux/netdevice.h>
#include <linux/etherdevice.h>
#ifdef KERNEL_2_6_32
#include <linux/sched.h>
#endif
#include "ste_nl80211_testmode_msg_copy.h"
#include "driver_nl80211_testmode_ste_copy.h"

#ifdef PROC_INTERFACE
static struct CW1200_priv *cil_priv;
#endif

/*****************************************************************
*				LOCAL funcs
******************************************************************/
static int cw1200_change_virtual_intf(struct wiphy *wiphy,
		struct net_device *dev,
		enum nl80211_iftype type, u32 *flags,
		struct vif_params *params);

static int cw1200_join_ibss(struct wiphy *wiphy, struct net_device *dev,
		struct cfg80211_ibss_params *params);

static int cw1200_scan(struct wiphy *wiphy, struct net_device *ndev,
		struct cfg80211_scan_request *request);

static int cw1200_auth(struct wiphy *wiphy, struct net_device *dev,
		struct cfg80211_auth_request *req);

static int cw1200_add_key(struct wiphy *wiphy, struct net_device *dev,
		u8 key_idx, bool pairwise, const u8 *mac_addr,
		struct key_params *params);

static int cw1200_del_key(struct wiphy *wiphy, struct net_device *ndev,
		u8 key_idx, bool pairwise, const u8 *mac_addr);

static int cw1200_leave_ibss(struct wiphy *wiphy,
		struct net_device *dev);

static int  cw1200_connect(struct wiphy *wiphy,
		struct net_device *dev,
		struct cfg80211_connect_params *sme);

static int cw1200_assoc(struct wiphy *wiphy,
		struct net_device *dev,
		struct cfg80211_assoc_request *req);

static int cw1200_disconnect(struct wiphy *wiphy,
		struct net_device *dev,
		u16 reason_code);

static int cw1200_set_default_key(struct wiphy *wiphy,
		struct net_device *ndev,
		u8 key_index, bool unicast, bool multicast);

static int cw1200_set_power_mgmt(struct wiphy *wiphy,
		struct net_device *dev,
		bool enabled, int timeout);

static int cw1200_testmode_cmd(struct wiphy *wiphy,
		void *data, int len);

static int tunnel_set_11n_conf(struct wiphy *wiphy,
		struct ste_msg_11n_conf *tdata , int len);

static int cw1200_rssi_config(struct wiphy *wiphy,
			struct net_device *dev,
			signed int rssi_thold,
			uint32 rssi_hyst);

static int cw1200_beacon_miss_config(struct wiphy *wiphy,
				struct net_device *dev,
				u32 beacon_thold);

static int cw1200_tx_fail_config(struct wiphy *wiphy,
				struct net_device *dev,
				u32 tx_thold);

static int cw1200_set_pmksa(struct wiphy *wiphy, struct net_device *netdev,
				struct cfg80211_pmksa *pmksa);

static int cw1200_del_pmksa(struct wiphy *wiphy, struct net_device *netdev,
				struct cfg80211_pmksa *pmksa);

void cw1200_bss_loss_work(struct work_struct *work);

void cw1200_connection_loss_work(struct work_struct *work);

void cw1200_handle_delayed_link_loss(struct CW1200_priv *priv);

void cw1200_check_connect(struct work_struct *work);

struct net_device * cw1200_add_virtual_intf(struct wiphy *wiphy, char *name,
				enum nl80211_iftype type, u32 *flags,
				struct vif_params *params);

int cw1200_del_virtual_intf(struct wiphy *wiphy, struct net_device *dev);

int cw1200_set_channel(struct wiphy *wiphy, struct net_device *dev,
			struct ieee80211_channel *chan,
			enum nl80211_channel_type channel_type);

int cw1200_add_beacon(struct wiphy *wiphy, struct net_device *dev,
			      struct beacon_parameters *info);

int cw1200_set_beacon(struct wiphy *wiphy, struct net_device *dev,
			      struct beacon_parameters *info);

int cw1200_deauth(struct wiphy *wiphy, struct net_device *dev,
			  struct cfg80211_deauth_request *req,
			  void *cookie);

int cw1200_del_beacon(struct wiphy *wiphy, struct net_device *dev);

int cw1200_get_station(struct wiphy *wiphy, struct net_device *dev,
			 u8 *mac, struct station_info *sinfo);

int cw1200_remain_on_channel(struct wiphy *wiphy,
				     struct net_device *dev,
				     struct ieee80211_channel *chan,
				     enum nl80211_channel_type channel_type,
				     unsigned int duration,
				     uint64_t *cookie);

int cw1200_cancel_remain_on_channel(struct wiphy *wiphy,
					struct net_device *dev,
					uint64_t cookie);

int cw1200_mgmt_tx(struct wiphy *wiphy, struct net_device *dev,
                          struct ieee80211_channel *chan, bool offchan,
                          enum nl80211_channel_type channel_type,
                          bool channel_type_valid, unsigned int wait,
                          const u8 *buf, size_t len, u64 *cookie);

int cw1200_mgmt_tx_cancel_wait(struct wiphy *wiphy,
			       struct net_device *dev,
			       u64 cookie);

void cw1200_mgmt_frame_register(struct wiphy *wiphy,
				struct net_device *dev,
				uint16_t frame_type, bool reg);

void cw1200_roc_timeout(struct work_struct *work);

static int tunnel_set_p2p_state(struct wiphy *wiphy,
		struct ste_msg_set_p2p *tdata , int len);

static int tunnel_set_uapsd(struct wiphy *wiphy,
		struct ste_msg_set_uapsd *tdata , int len);

static int tunnel_set_p2p_power_save(struct wiphy *wiphy,
		void *tdata, int len, int p2p_ps_type);

static int hif_debug(struct file *file, const char __user *buffer,
			unsigned long count, void *data);

static int __cw1200_add_key(struct wiphy *wiphy, struct net_device *ndev,
				u8 key_idx, bool pairwise, const u8 *mac_addr,
				struct key_params *params);

static const u32 cw1200_cipher_suites[] = {
	WLAN_CIPHER_SUITE_WEP40,
	WLAN_CIPHER_SUITE_WEP104,
	WLAN_CIPHER_SUITE_TKIP,
	WLAN_CIPHER_SUITE_CCMP,
#ifdef WAPI
	WLAN_CIPHER_SUITE_SMS4
#endif
};

static const struct ieee80211_txrx_stypes
ieee80211_default_mgmt_stypes[NUM_NL80211_IFTYPES] = {
	[NL80211_IFTYPE_ADHOC] = {
		.tx = 0xffff,
		.rx = BIT(IEEE80211_STYPE_ACTION >> 4),
	},
	[NL80211_IFTYPE_STATION] = {
		.tx = 0xffff,
		.rx = BIT(IEEE80211_STYPE_ACTION >> 4) |
			BIT(IEEE80211_STYPE_PROBE_REQ >> 4),
	},
	[NL80211_IFTYPE_AP] = {
		.tx = 0xffff,
		.rx = BIT(IEEE80211_STYPE_ASSOC_REQ >> 4) |
			BIT(IEEE80211_STYPE_REASSOC_REQ >> 4) |
			BIT(IEEE80211_STYPE_PROBE_REQ >> 4) |
			BIT(IEEE80211_STYPE_DISASSOC >> 4) |
			BIT(IEEE80211_STYPE_AUTH >> 4) |
			BIT(IEEE80211_STYPE_DEAUTH >> 4) |
			BIT(IEEE80211_STYPE_ACTION >> 4),
	},
	[NL80211_IFTYPE_AP_VLAN] = {
		/* copy AP */
		.tx = 0xffff,
		.rx = 0xffff,
		},
	[NL80211_IFTYPE_P2P_CLIENT] = {
		.tx = 0xffff,
		.rx = BIT(IEEE80211_STYPE_ACTION >> 4) |
			BIT(IEEE80211_STYPE_PROBE_REQ >> 4),
	},
	[NL80211_IFTYPE_P2P_GO] = {
		.tx = 0xffff,
		.rx = BIT(IEEE80211_STYPE_ASSOC_REQ >> 4) |
			BIT(IEEE80211_STYPE_REASSOC_REQ >> 4) |
			BIT(IEEE80211_STYPE_PROBE_REQ >> 4) |
			BIT(IEEE80211_STYPE_DISASSOC >> 4) |
			BIT(IEEE80211_STYPE_AUTH >> 4) |
			BIT(IEEE80211_STYPE_DEAUTH >> 4) |
			BIT(IEEE80211_STYPE_ACTION >> 4),
	},
	[NL80211_IFTYPE_MESH_POINT] = {
		.tx = 0x00,
		.rx = 0x00,
	},
};

/* indicated the link speed rate (Mbit/s) type used in Upper MAC */
static int link_speed[22] = {1, 2, 5, 11, 22, 33, 6, 9, 12, 18,
			     24, 36, 48, 54, 6, 13, 19, 26, 39,
			     52, 58, 65};


static int CIL_AP_Open(struct net_device *dev)
{
	DEBUG(DBG_CIL, "%s, Called \n", __func__);
	return SUCCESS;
}

static int CIL_AP_Stop(struct net_device *dev)
{
	DEBUG(DBG_CIL, "%s, Called \n", __func__);
	return SUCCESS;
}

static int CIL_AP_Transmit(struct sk_buff *skb, struct net_device *dev)
{
	u8 *pos= NULL;
	struct ieee80211_hdr *hdr= NULL;
	struct cfg_priv *c_priv = NULL;
	int32_t retval = SUCCESS;
	u8 sa[ETH_ALEN];
	u8 da[ETH_ALEN];

	c_priv = netdev_priv(dev);


	pos = (u8 *) (skb->data + 24 + 14);
	/* Skip rfc1042_header */
	pos += 6;

	if (ETH_P_PAE == GET_BE16(pos) ) {
		DEBUG(DBG_CIL, "EAPOL frame \n");
		/* 802.11 -> 802.3 conversion */
		memcpy(sa, &skb->data[18+6], ETH_ALEN);
		memcpy(da, &skb->data[18], ETH_ALEN);

		memcpy(&skb->data[18+6], da, ETH_ALEN);
		memcpy(&skb->data[18+6+6], sa, ETH_ALEN);

		/* Update SKB pointer */
		skb_pull(skb, 14 + 4 + 6);
		retval = EIL_Transmit(skb, c_priv->driver_priv->netdev);
	}

	DEBUG(DBG_CIL, "%s, Called:Retval:[%d] \n", __func__, retval);

	return retval;
}

static const struct net_device_ops ap_netdev_ops = {
        .ndo_open                = CIL_AP_Open,
        .ndo_stop                = CIL_AP_Stop,
        .ndo_start_xmit          = CIL_AP_Transmit,
};

static struct cfg80211_ops cw1200_config_ops = {
	.change_virtual_intf = cw1200_change_virtual_intf,
	.scan = cw1200_scan,
	.join_ibss = cw1200_join_ibss,
	.leave_ibss = cw1200_leave_ibss,
	.connect = cw1200_connect,
	.add_key = cw1200_add_key,
	.del_key = cw1200_del_key,
	.disconnect = cw1200_disconnect,
	.set_default_key = cw1200_set_default_key,
	.testmode_cmd = cw1200_testmode_cmd,
	.set_cqm_rssi_config = cw1200_rssi_config,
	.set_cqm_beacon_miss_config = cw1200_beacon_miss_config,
	.set_cqm_tx_fail_config = cw1200_tx_fail_config,
	.set_pmksa = cw1200_set_pmksa,
	.del_pmksa = cw1200_del_pmksa,
	.add_virtual_intf = cw1200_add_virtual_intf,
	.del_virtual_intf  = cw1200_del_virtual_intf,
	.set_channel = cw1200_set_channel,
	.add_beacon = cw1200_add_beacon,
	.set_beacon = cw1200_set_beacon,
	.del_beacon = cw1200_del_beacon,
	.deauth = cw1200_deauth,
	.get_station = cw1200_get_station,
	.remain_on_channel = cw1200_remain_on_channel,
	.cancel_remain_on_channel = cw1200_cancel_remain_on_channel,
	.mgmt_tx = cw1200_mgmt_tx,
	.mgmt_tx_cancel_wait = cw1200_mgmt_tx_cancel_wait,
	.mgmt_frame_register = cw1200_mgmt_frame_register,
};

#ifdef PROC_INTERFACE
static uint8_t wep_key[5] =  {0x12, 0x34, 0x56, 0x78, 0x90};

static uint8_t peer_addr[6] = {0x00, 0x09, 0x5b, 0x66, 0x2e, 0x12};

static uint8_t ssid[32];
static uint32_t ssid_len;
#endif

#define RATETAB_ENT(_rate, _rateid, _flags)		\
	{						\
	.bitrate		= (_rate),		\
	.hw_value		= (_rateid),		\
	.flags			= (_flags),		\
	}


static struct ieee80211_rate cw1200_rates[] = {
	RATETAB_ENT(10,  0x1,   0),
	RATETAB_ENT(20,  0x2,   0),
	RATETAB_ENT(55,  0x4,   0),
	RATETAB_ENT(110, 0x8,   0),
	RATETAB_ENT(60,  0x10,  0),
	RATETAB_ENT(90,  0x20,  0),
	RATETAB_ENT(120, 0x40,  0),
	RATETAB_ENT(180, 0x80,  0),
	RATETAB_ENT(240, 0x100, 0),
	RATETAB_ENT(360, 0x200, 0),
	RATETAB_ENT(480, 0x400, 0),
	RATETAB_ENT(540, 0x800, 0),
};

#define cw1200_a_rates		(cw1200_rates + 4)
#define cw1200_a_rates_size		8
#define cw1200_g_rates		(cw1200_rates + 0)
#define cw1200_g_rates_size		12


#define CHAN2G(_channel, _freq, _flags) {			\
	.band			= IEEE80211_BAND_2GHZ,		\
	.center_freq	= 	(_freq),			\
	.hw_value		= (_channel),			\
	.flags			= (_flags),			\
	.max_antenna_gain	= 0,				\
	.max_power		= 30,				\
}

#define CHAN5G(_channel, _flags) {				\
	.band			= IEEE80211_BAND_5GHZ,		\
	.center_freq	= 5000 + (5 * (_channel)),		\
	.hw_value		= (_channel),			\
	.flags			= (_flags),			\
	.max_antenna_gain	= 0,				\
	.max_power		= 30,				\
}

static struct ieee80211_channel cw1200_2ghz_chantable[] = {
	CHAN2G(1, 2412, 0),
	CHAN2G(2, 2417, 0),
	CHAN2G(3, 2422, 0),
	CHAN2G(4, 2427, 0),
	CHAN2G(5, 2432, 0),
	CHAN2G(6, 2437, 0),
	CHAN2G(7, 2442, 0),
	CHAN2G(8, 2447, 0),
	CHAN2G(9, 2452, 0),
	CHAN2G(10, 2457, 0),
	CHAN2G(11, 2462, 0),
	CHAN2G(12, 2467, 0),
	CHAN2G(13, 2472, 0),
	CHAN2G(14, 2484, 0),
};

static struct ieee80211_channel cw1200_5ghz_chantable[] = {
	CHAN5G(34, 0), 		CHAN5G(36, 0),
	CHAN5G(38, 0), 		CHAN5G(40, 0),
	CHAN5G(42, 0), 		CHAN5G(44, 0),
	CHAN5G(46, 0), 		CHAN5G(48, 0),
	CHAN5G(52, 0), 		CHAN5G(56, 0),
	CHAN5G(60, 0), 		CHAN5G(64, 0),
	CHAN5G(100, 0),		CHAN5G(104, 0),
	CHAN5G(108, 0),		CHAN5G(112, 0),
	CHAN5G(116, 0),		CHAN5G(120, 0),
	CHAN5G(124, 0),		CHAN5G(128, 0),
	CHAN5G(132, 0),		CHAN5G(136, 0),
	CHAN5G(140, 0),		CHAN5G(149, 0),
	CHAN5G(153, 0),		CHAN5G(157, 0),
	CHAN5G(161, 0),		CHAN5G(165, 0),
	CHAN5G(184, 0),		CHAN5G(188, 0),
	CHAN5G(192, 0),		CHAN5G(196, 0),
	CHAN5G(200, 0),		CHAN5G(204, 0),
	CHAN5G(208, 0),		CHAN5G(212, 0),
	CHAN5G(216, 0),
};


static struct ieee80211_supported_band cw1200_band_2ghz = {
	.channels = cw1200_2ghz_chantable,
	.n_channels = ARRAY_SIZE(cw1200_2ghz_chantable),
	.bitrates = cw1200_g_rates,
	.n_bitrates = cw1200_g_rates_size,
};

static struct ieee80211_supported_band cw1200_band_5ghz = {
	.channels = cw1200_5ghz_chantable,
	.n_channels = ARRAY_SIZE(cw1200_5ghz_chantable),
	.bitrates = cw1200_a_rates,
	.n_bitrates = cw1200_a_rates_size,
};

extern uint32_t hif_dbg_flag;

#ifdef PROC_INTERFACE
static int getmacaddr(char *buf, char **start, off_t offset, int len, int *eof
			, void *data)
{
	UMI_STATUS_CODE retval = SUCCESS;
	uint16_t *macaddr = NULL;
	macaddr = (uint16_t *)CIL_Get(cil_priv,
	UMI_DEVICE_OID_802_11_STATION_ID, 6);
	if (SUCCESS != retval) {
		DEBUG(DBG_CIL, "GETMACADDR Retval [%x] \n", retval);
		goto err_out;
	}
	PRINT("MACADDR [%x].[%x], [%x] \n", macaddr[0], macaddr[1], macaddr[2]);
	kfree(macaddr);
err_out:
	*eof = 1;
	return 0;
}

static int scan(char *buf, char **start, off_t offset, int len,
		int *eof, void *data)
{
	UMI_BSS_LIST_SCAN  scanreq;
	UMI_STATUS_CODE retval = SUCCESS;
	scanreq.flags = ((1<<0) | (1<<31));
	scanreq.powerLevel = 0;
	scanreq.ssid[0] = 0;
	scanreq.ssid[1] = 0;
	scanreq.ssidLength = 0;
	retval = CIL_Set(cil_priv,
			UMI_DEVICE_OID_802_11_BSSID_LIST_SCAN,
			&scanreq, sizeof(UMI_BSS_LIST_SCAN));
err_out:
	*eof = 1;
	return 0;
}

static int wep(char *buf, char **start, off_t offset,
		int len, int *eof, void *data)
{
	UMI_STATUS_CODE retval = SUCCESS;
	uint32_t encr = 0x1;
	UMI_OID_802_11_KEY wep_key_s;
	static UMI_OID_802_11_KEY wep_key_g;
	retval = CIL_Set(cil_priv,
			UMI_DEVICE_OID_802_11_ENCRYPTION_STATUS
			, &encr, 4)
	PRINT("WEP_ENCR_STATUS [%d] \n", retval);
	msleep(1000);
	wep_key_s.keyType = 1;
	wep_key_s.entryIndex = 0;
	memcpy(&(wep_key_s.Key.WepPairwiseKey.peerAddress[0]),
		&peer_addr[0], 6);
	wep_key_s.Key.WepPairwiseKey.keyLength = sizeof(wep_key);
	memcpy(&(wep_key_s.Key.WepPairwiseKey.keyData[0]),
		&wep_key[0], sizeof(wep_key));
	retval = CIL_Set(cil_priv,
			UMI_DEVICE_OID_802_11_ADD_KEY,
			&wep_key_s, sizeof(UMI_OID_802_11_KEY));
	PRINT("KEY_SET_STATUS [%d] \n", retval);
	msleep(1000);
	wep_key_g.keyType = 0;
	wep_key_g.entryIndex = 0;
	wep_key_g.Key.WepGroupKey.keyId = 0;
	wep_key_g.Key.WepGroupKey.keyLength = 5;
	memcpy(&(wep_key_g.Key.WepGroupKey.keyData[0]), &wep_key[0], 5);
	retval = CIL_Set(cil_priv,
			UMI_DEVICE_OID_802_11_ADD_KEY,
			&wep_key_g, sizeof(UMI_OID_802_11_KEY));
	PRINT("GROUP_KEY_SET_STATUS [%d] \n", retval);
err_out:
	*eof = 1;
	return 0;
}

static int connect(char *buf, char **start, off_t offset,
			int len, int *eof, void *data)
{
	UMI_STATUS_CODE retval = SUCCESS;
	UMI_802_11_SSID ssid_info;
	uint32_t inframode = 1;
	uint8_t auth_mode = 0;
	uint32_t power_save = 0;
	retval = CIL_Set(cil_priv,
			UMI_DEVICE_OID_802_11_INFRASTRUCTURE_MODE,
			&inframode, 4);
	PRINT("CONNECT [%d] \n", retval);
	retval = CIL_Set(cil_priv,
			UMI_DEVICE_OID_802_11_POWER_MODE,
			&power_save, 4);
	retval = CIL_Set(cil_priv,
			UMI_DEVICE_OID_802_11_AUTHENTICATION_MODE,
			&auth_mode, 1);
	PRINT("CONNECT [%d] \n", retval);
	ssid_info.ssidLength = ssid_len;
	memcpy(&ssid_info.ssid, &ssid[0], ssid_len);
	retval = CIL_Set(cil_priv,
			UMI_DEVICE_OID_802_11_SSID,
			&ssid_info, sizeof(UMI_802_11_SSID));
	PRINT("CONNECT [%d] \n", retval);
err_out:
	*eof = 1;
	return 0;
}


static int write_ssid(struct file *file, const char __user *buffer,
			unsigned long count, void *data)
{
	char *page;
	page = kmalloc(count, GFP_KERNEL);
	if (!page)
		return -ENOMEM;
	if (copy_from_user(page, buffer, count)) {
		kfree(page);
		return -EFAULT;
	}
	if (count > 1) {
		memcpy(&ssid[0], page, count - 1);
		ssid_len = count - 1;
	} else {
		ssid_len = 0;
		ssid[0] = 0;
	}
	kfree(page);
	return count;
}

#endif

static int hif_debug(struct file *file, const char __user *buffer,
			unsigned long count, void *data)
{
	char *page;
	unsigned long val = 0;

	page = kmalloc(count, GFP_KERNEL);
	if (!page)
		return -ENOMEM;

	if (copy_from_user(page, buffer, count)) {
		kfree(page);
		return -EFAULT;
	}
	if (count > 1)
		val = simple_strtoul(page, NULL, 10);

	if (val)
		DEBUG(DBG_CIL,"WLAN HIF Debugging ENABLED \n");
	else
		DEBUG(DBG_CIL,"WLAN HIF Debugging DISABLED \n");

	hif_dbg_flag = val;

	kfree(page);
	return count;
}


/*! \fn  CW1200_STATUS_E CIL_Init(struct CW1200_priv *priv)

		brief This func initialises the CIL layer.It registers
		with the CFG80211.
		param priv -> pointer to the driver private structure
		return -> Status code. Pass or failure code.
*/

#define DBG_SEM DBG_CIL
//#define DBG_SEM DBG_NONE

#define DOWN(__sem__, __buf__) do { \
	DEBUG(DBG_SEM, "--> down begin %s\n", __func__); \
	if (down_interruptible(__sem__) < 0) { \
		DEBUG(DBG_SEM, "--> interrupted %s", __func__); \
		return -EINTR; \
	} \
	DEBUG(DBG_SEM, "--> down ok %s %s\n", __func__, (__buf__)); \
} while(0)

#define UP(__sem__, __buf__) do { \
	DEBUG(DBG_SEM, "--> up %s %s\n", __func__, (__buf__)); \
	up(__sem__); \
} while(0)

CW1200_STATUS_E CIL_Init(struct CW1200_priv *priv)
{

	struct wireless_dev *wdev;
	struct device *dev = &(priv->func->dev);
	int32_t  ret = 0;

	struct cfg_priv *c_priv = NULL;
#ifdef PROC_INTERFACE
	cil_priv = priv;
#endif
	sema_init(&(priv->cil_sem), 1);
	spin_lock_init(&(priv->cil_lock));
	init_waitqueue_head(&priv->cil_set_wait);
	init_waitqueue_head(&priv->cil_get_wait);
	atomic_set(&priv->cil_set_cond, 0);
	priv->get_buff_pointer = NULL;
	priv->get_buff_len = 0;
	priv->connection_status = CW1200_DISCONNECTED;
	priv->wait_flag_set = FALSE;
	priv->wait_flag_get = FALSE;
	/* Add runtime control for enabling various WLAN driver params */
	priv->proc =  create_proc_entry("wlan_hif_debug", S_IFREG | 0644 ,init_net.proc_net);
	if (!priv->proc) {
		PRINT("CIL: Unable to create proc entry\n");
		return ERROR;
	}
	priv->proc->write_proc = hif_debug;
	priv->proc->read_proc = NULL;
	priv->proc->uid = 0;
	priv->proc->gid = 0;

#ifdef PROC_INTERFACE
	proc =  create_proc_entry("ssid", S_IFREG | 0644 , NULL);
	if (!proc) {
		PRINT("CIL: Unable to create proc entry\n");
		return ERROR;
	}
	proc->write_proc = write_ssid;
	proc->read_proc = NULL;
	proc->owner = THIS_MODULE;
	proc->uid = 0;
	proc->gid = 0;
	create_proc_read_entry("getmac", 0, NULL, getmacaddr, NULL);
	create_proc_read_entry("scan", 0, NULL, scan, NULL);
	create_proc_read_entry("connect", 0, NULL, connect, NULL);
	create_proc_read_entry("wep", 0, NULL, wep, NULL);
#endif
	wdev = kzalloc(sizeof(struct wireless_dev), GFP_KERNEL);
	if (!wdev) {
		remove_proc_entry("wlan_hif_debug", init_net.proc_net);
		return ERROR;
	}
	wdev->wiphy = wiphy_new(&cw1200_config_ops,
			sizeof(struct cfg_priv));
	if (!wdev->wiphy)
		ret = -ERROR;
	c_priv = wiphy_priv(wdev->wiphy);
	c_priv->driver_priv = priv;
	set_wiphy_dev(wdev->wiphy, dev);
	wdev->wiphy->max_scan_ssids = MAX_PROBE;
	wdev->wiphy->interface_modes = BIT(NL80211_IFTYPE_STATION) |
		BIT(NL80211_IFTYPE_ADHOC) | BIT(NL80211_IFTYPE_AP) | BIT(NL80211_IFTYPE_MONITOR) |
		BIT(NL80211_IFTYPE_P2P_GO) | BIT(NL80211_IFTYPE_P2P_CLIENT);

	wdev->wiphy->bands[IEEE80211_BAND_2GHZ] = &cw1200_band_2ghz;
#ifdef ENABLE_5GHZ
	wdev->wiphy->bands[IEEE80211_BAND_5GHZ] = &cw1200_band_5ghz;
#endif
	wdev->wiphy->signal_type = CFG80211_SIGNAL_TYPE_MBM;
	wdev->wiphy->cipher_suites = cw1200_cipher_suites;
	wdev->wiphy->n_cipher_suites = ARRAY_SIZE(cw1200_cipher_suites);
	wdev->wiphy->max_scan_ie_len = IE_LEN_MAX ;
	wdev->wiphy->max_scan_ssids = 2;
	wdev->wiphy->mgmt_stypes = ieee80211_default_mgmt_stypes;
	wdev->wiphy->max_remain_on_channel_duration = 5000;
	ret = wiphy_register(wdev->wiphy);
	if (ret < 0) {
		DEBUG(DBG_CIL, "Couldn't register wiphy device\n");
		wiphy_free(wdev->wiphy);
		kfree(wdev);
		remove_proc_entry("wlan_hif_debug", init_net.proc_net);
		return ERROR;
	}

		/*Update Wireless Device Pointer */
	priv->netdev->ieee80211_ptr = wdev;
	SET_NETDEV_DEV(priv->netdev, wiphy_dev(wdev->wiphy));
	wdev->netdev = priv->netdev;
	wdev->iftype = NL80211_IFTYPE_ADHOC;

	/* Initialise default values for BLOCK ACK */
	priv->tx_block_ack = TX_BLOCK_ACK_TIDS;
	priv->rx_block_ack = RX_BLOCK_ACK_TIDS;

	priv->bss_loss_WQ = create_singlethread_workqueue("bss_loss_work");
	INIT_DELAYED_WORK(&priv->bss_loss_work, cw1200_bss_loss_work);
	INIT_DELAYED_WORK(&priv->connection_loss_work,
				cw1200_connection_loss_work);
	INIT_DELAYED_WORK(&priv->connect_result, cw1200_check_connect);
	/* Init delayed work for handling Remain on Chan timeout */
	INIT_DELAYED_WORK(&priv->roc_work, cw1200_roc_timeout);

	priv->cqm_beacon_loss_count = CQM_BEACON_LOSS;
	priv->cqm_link_loss_count = CQM_LINK_LOSS;

	memset(&priv->pmk_list, 0, sizeof(UMI_BSSID_PMKID_BKID));
	memset(&priv->connected_ssid, 0, 32);
	c_priv->driver_priv->sta_count = 0;

	mutex_init(&priv->mutex_p2p);
	mutex_init(&priv->mutex_cil);
	priv->minAutoTriggerInterval = 0;
	priv->maxAutoTriggerInterval = 0;
	priv->autoTriggerStep = 0;

	return SUCCESS;
}

/*! \fn  CW1200_STATUS_E CIL_Shutdown(struct CW1200_priv *priv)

		brief This func de-initialises the CIL layer.
		param priv -> pointer to the driver private structure
		return -> Status code. Pass or failure code.
*/
CW1200_STATUS_E CIL_Shutdown(struct CW1200_priv *priv)
{

	wiphy_unregister(priv->netdev->ieee80211_ptr->wiphy);
	wiphy_free(priv->netdev->ieee80211_ptr->wiphy);
	cancel_delayed_work_sync(&priv->bss_loss_work);
	cancel_delayed_work_sync(&priv->connection_loss_work);
	destroy_workqueue(priv->bss_loss_WQ);
	kfree(priv->netdev->ieee80211_ptr);
	remove_proc_entry("wlan_hif_debug", init_net.proc_net);

	return SUCCESS;
}

/*! \fn  CW1200_STATUS_E CIL_Set(struct CW1200_priv *priv,
				UMI_DEVICE_OID_E oid_to_set,
				void *oid_data, int32_t length)
	brief This func is called to pass a new OID value to the UMAC.
	param priv -> pointer to the driver private structure
	param UMI_DEVICE_OID_E -> The OID value to be set.
	param oid_data -> pointer to the OID data to be set.
	return -> Length of the OID data to be set.
*/

CW1200_STATUS_E CIL_Set(struct CW1200_priv *priv,
			enum UMI_DEVICE_OID_E oid_to_set,
			void *oid_data, int32_t length)
{
	UMI_STATUS_CODE umi_status = UMI_STATUS_SUCCESS;
	CW1200_STATUS_E status = ERROR;
	long val = 0;

	val = atomic_read(&priv->cil_set_cond);
	priv->set_status = WAIT_INIT_VAL;
	WARN_ON(val);

	if (val)
		DEBUG(DBG_CIL, "cil_set_cond: %ld\n", val);

	atomic_set(&priv->cil_set_cond, 0);
	/* Acquire spinlock as this context
	can be broken by the SBUS bottom half */
	spin_lock_bh(&(priv->cil_lock));
	umi_status = UMI_SetParameter(priv->umac_handle, oid_to_set,
					length, priv->setget_linkid, oid_data);
	spin_unlock_bh(&(priv->cil_lock));
	if (UMI_STATUS_SUCCESS == umi_status) {
		status = SUCCESS;
	} else if (UMI_STATUS_PENDING == umi_status) {
		DEBUG(DBG_CIL, "CIL_Set():Going to sleep \n");
		if (atomic_add_return(1, &priv->cil_set_cond) == 1) {
			wait_event_interruptible(priv->cil_set_wait ,
						priv->wait_flag_set);
			priv->wait_flag_set = FALSE ;
		}
		DEBUG(DBG_CIL, "CIL_Set(): Woken up \n");
		/*Control comes here after wakeup or wait completion */
		if (priv->set_status != SUCCESS) {
			DEBUG(DBG_CIL, "UMI_SetParameter():"
					"Complete return err[%x]\n",
					umi_status);
		} else
			status = SUCCESS;
	} else 	{
		DEBUG(DBG_CIL, "UMI_SetParameter() return error [%x] \n",
				umi_status);
	}
	return status;
}


/*! \fn  CW1200_STATUS_E CIL_Get(struct CW1200_priv *priv,
				UMI_DEVICE_OID_E oid_to_set,
				void *oid_data, int32_t length)
	brief This func is called to read an OID value from the UMAC.
	param priv -> pointer to the driver private structure
	param UMI_DEVICE_OID_E -> The OID value to be read.
	param oid_data -> pointer to the memory buffer
	where the OID data will be stored.
	return -> Length of the OID data to be read.
*/
void *CIL_Get(struct CW1200_priv *priv,
		enum UMI_DEVICE_OID_E oid_to_get,
		int32_t length)
{
	UMI_STATUS_CODE umi_status = UMI_STATUS_SUCCESS;
	UMI_GET_PARAM_STATUS *pUMIGetParamStatus = NULL;
	void *oid_data = NULL;
	int32_t retval = SUCCESS;
	int count = 0;

	pUMIGetParamStatus = UMI_GetParameter(priv->umac_handle,
						oid_to_get,priv->setget_linkid);

	if (NULL == pUMIGetParamStatus) {
		return NULL;
	}

	if (UMI_STATUS_SUCCESS == pUMIGetParamStatus->status) {
		DEBUG(DBG_CIL, "UMI_GetParameter() returned success \n");
		oid_data = pUMIGetParamStatus->pValue;
		if (pUMIGetParamStatus->length != length)
			DEBUG(DBG_CIL, "Returned Len is more than"
					"the buffer len \n");
		/*Free container structure The oid_data will be
		* freed by the caller */
		kfree(pUMIGetParamStatus);
		pUMIGetParamStatus = NULL;
	} else if (UMI_STATUS_PENDING == pUMIGetParamStatus->status) {
			DEBUG(DBG_CIL, "CIL_Get():Going to sleep \n");
			priv->get_buff_pointer = NULL;
			priv->get_buff_len = 0;
			/*Wait for the response */
			wait_event_interruptible(priv->cil_get_wait ,
						priv->wait_flag_get);
			priv->wait_flag_get = FALSE;
			if (NULL != priv->get_buff_pointer) {
				oid_data = priv->get_buff_pointer;
				if (length != priv->get_buff_len)
					DEBUG(DBG_CIL, "Return Len is"
							"more than buf len \n");
				kfree(pUMIGetParamStatus);
			} else
				goto err_out;
	} else
		goto err_out;

	return oid_data;
err_out:
	kfree(pUMIGetParamStatus);
	return NULL;
}

/*! \fn  CIL_get_sta_linkid(struct CW1200_priv *priv,uint8_t * mac_addr)
brief This func gets the linkID for the given MAC address
param priv -> pointer to the driver private structure
return -> MAC address to search
*/
int CIL_get_sta_linkid(struct CW1200_priv *priv,uint8_t * mac_addr)
{
	uint32_t i=0;
	int sta_link_id = -1;

	UMI_EVT_DATA_CONNECT_INFO *sta_list = NULL;
	if (NL80211_IFTYPE_AP < priv->device_mode) {
		return 0;
	}
	if ((!mac_addr) || (is_multicast_ether_addr(mac_addr))) {
		sta_link_id = 0;
	} else {
		for (i=0; i < MAX_SUPPORTED_STA; i++) {
			sta_list = (UMI_EVT_DATA_CONNECT_INFO *)(priv->sta_info + i);
			if (compare_ether_addr(mac_addr,
				&(sta_list->stationMacAddr[0]) ) == 0) {
				sta_link_id = sta_list->linkId;
				break;
			}
		}

		if ( i >= MAX_SUPPORTED_STA) {
			DEBUG(DBG_ERROR, "CIL_get_sta_linkid:STA not found"
				" in internal list \n");
		}
	}
	return sta_link_id;
}

static int _UMI_mc_list(struct CW1200_priv *priv)
{
	UMI_OID_802_11_MULTICAST_ADDR_FILTER *mcfilter;
	int retval;

	mcfilter = &(priv->ste_mc_filter);
	DEBUG(DBG_CIL, "Multicast filter: %p enable: %d\n"
		, mcfilter,mcfilter->enable);
	retval = CIL_Set(priv,
		UMI_DEVICE_OID_802_11_MULTICAST_ADDR_FILTER,
		mcfilter, sizeof(UMI_OID_802_11_MULTICAST_ADDR_FILTER));
	if (retval) {
		DEBUG(DBG_ERROR, "CIL_Set for"
			"Multicast address list returned error\n");
	}
	return retval;
}

/*! \fn  UMI_mc_list(struct CW1200_priv *priv)
brief This func is called to set Multicast filter OID in the UMAC.
param priv -> pointer to the driver private structure
return -> SUCCESS/FAILURE from UMAC
*/
int UMI_mc_list(struct CW1200_priv *priv)
{
	int result;

	DOWN(&priv->cil_sem, "from eil");
	result = _UMI_mc_list(priv);
	UP(&priv->cil_sem, "from eil");

	return result;
}


/****************************************************************************
*				UMAC Callback funcs
*****************************************************************************/


/*! \fn void UMI_CB_Indicate_Event
				(UL_HANDLE UpperHandle,
				UMI_STATUS_COD	Status,
				UMI_EVENT Event,
				uint16_t EventDataLength,void *EventData)
	brief This func is called by UMAC to indicate
	to the driver that UMAC has received a packet from the device.
	param	UpperHandle	- upper layer driver instance for UMIcallback.
	para	Status	-	Further error code associated with the event.
	param	Event	-	The event to be indicated.
	param	EventDataLength - Length of the data associated with this event
	param	EventData	- Data Associated with this event
	return 	-None
*/
void UMI_CB_Indicate_Event(UL_HANDLE UpperHandle,
			UMI_STATUS_CODE Status,
			UMI_EVENT Event,
			uint16_t EventDataLength,
			void *EventData)
{
	struct CW1200_priv *priv = (struct CW1200_priv *)UpperHandle;
	UMI_OID_802_11_ASSOCIATION_INFORMATION *assoc_info = NULL;
	uint8_t *ie_ptr = NULL;
	uint32_t retval = SUCCESS;
	uint32_t rcode = 0;
	UMI_EVT_DATA_MIC_FAILURE *mic_failure_data = NULL;
	enum nl80211_key_type key_type;
	struct cfg80211_bss *bss = NULL;
	uint32_t lock_status = FALSE;
	UMI_EVT_DATA_CONNECT_INFO  *sta_info;
	struct station_info sinfo;
	UMI_EVT_DATA_CONNECT_INFO *sta_list = NULL;
	uint8_t *mac_addr = NULL;


	switch (Event) {
	case UMI_EVT_START_COMPLETED:
	{
		DEBUG(DBG_CIL, "CIL:START_COMPLETED received from UMAC \n");
		/*Completed the Init Process */
		EIL_Init_Complete(priv);
		break;
	}
	case UMI_EVT_SCAN_COMPLETED:
	{
		DEBUG(DBG_CIL, "CIL:SCAN EVENT COMPLETED Received \n");
		cw1200_handle_delayed_link_loss(priv);
		mutex_lock(&priv->mutex_cil);
		if (priv->request != NULL)
			cfg80211_scan_done(priv->request , 0);
		priv->request = NULL;
		mutex_unlock(&priv->mutex_cil);
		if (priv->wait_event == UMI_EVT_SCAN_COMPLETED) {
			priv->wait_event = 0;
			UP(&priv->cil_sem, "UMI_EVT_SCAN_COMPLETED");
		}
		break;
	}
	case UMI_EVT_STOP_COMPLETED:
	{
		DEBUG(DBG_CIL, "CIL:STOP Completed Event Received \n");
		if (spin_is_locked(&priv->cil_lock))
			lock_status = TRUE;

		if (lock_status)
			spin_unlock_bh(&(priv->cil_lock));

		unregister_netdev(priv->netdev);
		CIL_Shutdown(priv);

		if (lock_status)
			spin_lock_bh(&(priv->cil_lock));
		priv->stopped = TRUE;
		if (priv->wait_event) {
			priv->wait_event = 0;
			UP(&priv->cil_sem, "UMI_EVT_STOP_COMPLETED");
		}
		break;
	}
	case UMI_EVT_CONNECTING:
	{
		DEBUG(DBG_CIL, "CIL: EVENT RECEIVE->Device Connecting\n");
		break;
	}
	case UMI_EVT_RECONNECTED:
		DEBUG(DBG_CIL , "CIL : EVENT RECEIVED RECONNECTED.... \n");
		/* Fall through is intentional */
	case UMI_EVT_CONNECTED:
	{
		DEBUG(DBG_CIL, "CIL :EVENT Connected/RECONNECTED....\n");
		/* Stop the CONNECTION Watchdog */
		sta_info = (UMI_EVT_DATA_CONNECT_INFO *)EventData;
		cancel_delayed_work(&priv->connect_result);
		cancel_delayed_work(&priv->bss_loss_work);
		cancel_delayed_work(&priv->connection_loss_work);

		/* Release spinlock as this context might sleep.
		Safe to release spinklock here	*/
		if (spin_is_locked(&priv->cil_lock))
			lock_status = TRUE;
		if ((NL80211_IFTYPE_AP == priv->device_mode) &&
				(( sta_info->linkId < 1 ) ||
					(sta_info->linkId >= MAX_SUPPORTED_STA))) {
			 DEBUG(DBG_CIL, "CIL: Invalid LinkId reported from UMAC"
							"linkId = <%d>\n",sta_info->linkId);
			WARN_ON(1);
			break;
		}
		if (lock_status)
			spin_unlock_bh(&(priv->cil_lock));
		/* Update linkID for the associated STA */
		if (NL80211_IFTYPE_AP == priv->device_mode) {
			priv->setget_linkid = sta_info->linkId;
		} else {
			priv->setget_linkid = 0;
		}
		assoc_info = (UMI_OID_802_11_ASSOCIATION_INFORMATION *)
		CIL_Get(priv, UMI_DEVICE_OID_802_11_ASSOCIATION_INFORMATION ,
			sizeof(UMI_OID_802_11_ASSOCIATION_INFORMATION));
		/* Reset Set/Get Link ID */
		priv->setget_linkid = 0;

		if (lock_status)
			spin_lock_bh(&(priv->cil_lock));
		if (NL80211_IFTYPE_AP == priv->device_mode) {
			/* Store the list of STAs connected in internal list */
			sta_list = (UMI_EVT_DATA_CONNECT_INFO *)priv->sta_info;
			memcpy(sta_list + sta_info->linkId, sta_info,
				sizeof(UMI_EVT_DATA_CONNECT_INFO));
			priv->sta_count++;
			DEBUG(DBG_CIL, "CIL :linkid = %d\n",sta_info->linkId);
			mac_addr = &sta_info->stationMacAddr[0];
		} else {
			mac_addr = &assoc_info->currentApAddress[0];
		}

		if (NULL != assoc_info) {
			DEBUG(DBG_CIL, "CIL:ASSOC INFO OID Succeded \n");
			ie_ptr = (uint8_t *)((uint8_t *)
				(&assoc_info->variableIELenRsp) + 2);
			cfg80211_connect_result(priv->netdev,
					mac_addr,
					ie_ptr, assoc_info->variableIELenReq,
					(ie_ptr + assoc_info->variableIELenReq),
					assoc_info->variableIELenRsp,
					WLAN_STATUS_SUCCESS, GFP_ATOMIC);
			if (NL80211_IFTYPE_AP != priv->device_mode) {
				/* Get information on BEACON interval */
				bss = cfg80211_get_bss(priv->netdev->ieee80211_ptr->wiphy, NULL,
						&assoc_info->currentApAddress[0],
						NULL, 0, 0, 0);
				if (!bss) {
					DEBUG(DBG_ERROR, "CIL:Unable to get Beacon_Interval\n");
					priv->beacon_interval = DEFAULT_BEACON_INT;
				} else {
					DEBUG(DBG_CIL, "CIL:EVENT:BEACON INTERVAL [%d]\n",
						bss->beacon_interval);
					priv->beacon_interval = bss->beacon_interval;
					cfg80211_put_bss(bss);
				}
				if (priv->wait_event == UMI_EVT_CONNECTED) {
					priv->wait_event = 0;
					UP(&priv->cil_sem, "UMI_EVT_CONNECTED STA");
				}
			}
			priv->connection_status = CW1200_CONNECTED;
			kfree(assoc_info);
		} else {
			DEBUG(DBG_ERROR, "CIL:UMI_CB_Indicate_Event:error\n");

			if (priv->connection_status == CW1200_CONNECTING) {
			cfg80211_connect_result(priv->netdev,
					NULL, NULL, 0, NULL, 0,
					WLAN_STATUS_UNSPECIFIED_FAILURE, GFP_ATOMIC);
			}
			priv->connection_status = CW1200_DISCONNECTED;
			cfg80211_disconnected(priv->netdev,
						rcode, NULL, NULL, 0, GFP_ATOMIC);
			if (priv->wait_event == UMI_EVT_CONNECTED) {
				priv->wait_event = 0;
				UP(&priv->cil_sem, "UMI_CB_Indicate_Event:error");
			}
		}
		break;
	}
	case UMI_EVT_CONNECT_FAILED:
	{
		DEBUG(DBG_CIL, "CIL : EVENT RECEIVED -> Connect Failed...\n");
		/* Stop the CONNECTION Watchdog */
		cancel_delayed_work(&priv->connect_result);

		priv->connection_status = CW1200_DISCONNECTED;
		/* Abort Scanning if active */
		if (priv->request != NULL) {
			cfg80211_scan_done(priv->request , 0);
			priv->request = NULL;
		}
		cfg80211_connect_result(priv->netdev,
				NULL, NULL, 0, NULL, 0,
				WLAN_STATUS_UNSPECIFIED_FAILURE, GFP_ATOMIC);
		if (priv->wait_event == UMI_EVT_CONNECTED) {
			priv->wait_event = 0;
			UP(&priv->cil_sem, "UMI_EVT_CONNECT_FAILED");
		}
		break;
	}
	case UMI_EVT_BSS_LOST:
	{
		DEBUG(DBG_CIL, "CIL :EVENT RECEIVED BSS LOST...\n");
		cancel_delayed_work(&priv->bss_loss_work);
		cancel_delayed_work(&priv->connection_loss_work);

		if (NULL == priv->request) {
			priv->delayed_link_loss = FALSE;
			queue_delayed_work(priv->bss_loss_WQ,
					&priv->bss_loss_work, 0);
		} else {
			/* Scan is in progress. Delay reporting. */
			/* Scan complete will trigger bss_loss_work */
			priv->delayed_link_loss = TRUE;
			/* Also start a watchdog. */
			queue_delayed_work(priv->bss_loss_WQ,
					&priv->bss_loss_work, 10 * HZ);
		}
		break;
	}
	case UMI_EVT_BSS_REGAINED:
	{
		DEBUG(DBG_CIL, "CIL :EVENT RECEIVED BSS REGAINED...\n");
		priv->delayed_link_loss = FALSE;
		cancel_delayed_work(&priv->bss_loss_work);
		cancel_delayed_work(&priv->connection_loss_work);
		break;
	}
	/* Fall through intended */
	case UMI_EVT_DISCONNECTED:
	{
		DEBUG(DBG_CIL, "CIL :EVENT RECEIVED DISCONNECTED...\n");
		UMI_EVT_DATA_DISCONNECT_INFO *disc = (UMI_EVT_DATA_DISCONNECT_INFO *) EventData;
		/* If connection was in progress we should send the status first */
		if (priv->connection_status == CW1200_CONNECTING) {
			/* Stop the CONNECTION Watchdog */
			cancel_delayed_work(&priv->connect_result);
			cfg80211_connect_result(priv->netdev,
					NULL, NULL, 0, NULL, 0,
					WLAN_STATUS_UNSPECIFIED_FAILURE, GFP_ATOMIC);
		}

		if (NL80211_IFTYPE_AP == priv->device_mode) {
			int i = 0;
			uint8 macAddr[UMI_MAC_ADDRESS_SIZE];

			sta_list = (UMI_EVT_DATA_CONNECT_INFO *)priv->sta_info + disc->linkId;
			memcpy(macAddr, sta_list->stationMacAddr, UMI_MAC_ADDRESS_SIZE);
			DEBUG(DBG_CIL, "CIL: STA: %pM\n", macAddr);
			cfg80211_disconnected(priv->netdev, rcode,
				(u8 *)macAddr, NULL, 0, GFP_ATOMIC);
			memset(sta_list, 0, sizeof(UMI_EVT_DATA_CONNECT_INFO));
			priv->sta_count--;
			break;
		} else {
			priv->connection_status = CW1200_DISCONNECTED;
		        cancel_delayed_work(&priv->bss_loss_work);
		        cancel_delayed_work(&priv->connection_loss_work);
			priv->delayed_link_loss = FALSE;

			cfg80211_disconnected(priv->netdev,
					rcode, NULL, NULL, 0, GFP_ATOMIC);
			/* Abort Scanning if active */
			if (priv->request != NULL) {
				cfg80211_scan_done(priv->request , 0);
				priv->request = NULL;
			}

			if (priv->wait_event == UMI_EVT_DISCONNECTED ||
			    priv->wait_event == UMI_EVT_SCAN_COMPLETED) {
				priv->wait_event = 0;
				UP(&priv->cil_sem, "UMI_EVT_DISCONNECTED");
			}
		}
		break;
	}
	case UMI_EVT_MICFAILURE:
	{
		DEBUG(DBG_CIL, "CIL:MICFAILURE Event Received... \n");
		mic_failure_data =
			(UMI_EVT_DATA_MIC_FAILURE *)EventData;
		if (mic_failure_data->IsPairwiseKey) {
			DEBUG(DBG_CIL, "CIL: MICFAILURE for Pairwise Key \n");
			key_type = NL80211_KEYTYPE_PAIRWISE;
		} else {
			DEBUG(DBG_CIL, "CIL:MICFAILURE for Group Key \n");
			key_type = NL80211_KEYTYPE_GROUP ;
		}
		cfg80211_michael_mic_failure(priv->netdev ,
				&(mic_failure_data->PeerAddress[0]) ,
				key_type, mic_failure_data->KeyIndex ,
				&(mic_failure_data->Rsc[0]) , GFP_ATOMIC);
		break;
	}
	case UMI_EVT_RCPI_RSSI:
	{
		DEBUG(DBG_CIL, "CIL:UMI_EVT_RCPI_RSSI received ... \n");
		/* Device RSSI reporting is in unsigned char format
 * 		so no need for converting before comparing with u_rssi/l_rssi
 * 		as we stored converted u_rssi/l_rssi */
		if (*((unsigned char *)EventData) > priv->u_rssi) {
			cfg80211_cqm_rssi_notify(priv->netdev,
					NL80211_CQM_RSSI_THRESHOLD_EVENT_HIGH,
					GFP_ATOMIC);
		} else if (*((unsigned char *)EventData) < priv->l_rssi) {
			cfg80211_cqm_rssi_notify(priv->netdev,
					NL80211_CQM_RSSI_THRESHOLD_EVENT_LOW,
					GFP_ATOMIC);
		} else {
			DEBUG(DBG_CIL, "CIL:EVENT:Invalid RSSI Event received \n");
		}
		break;
	}
	case UMI_EVT_TX_FAILURE_THRESHOLD_EXCEEDED:
	{
		DEBUG(DBG_CIL, "CIL:UMI_EVT_TX_FAILURE_THRESHOLD_EXCEEDED"
				"received ... \n");
		cfg80211_cqm_tx_fail_notify(priv->netdev, GFP_ATOMIC);
		break;
	}
	case UMI_EVT_BSS_STARTED:
	{
		DEBUG(DBG_CIL, "CIL:UMI_EVT_BSS_STARTED Received \n");
		break;
	}
	case UMI_EVT_STOP_AP_COMPLETED:
	{
		DEBUG(DBG_CIL, "CIL:UMI_EVT_STOP_AP_COMPLETED Received \n");
		break;
	}
	case UMI_EVT_RX_MGMT_FRAME:
	{
		bool cfg_retval;

		DEBUG(DBG_CIL, "CIL:UMI_EVT_RX_MGMT_FRAME Received \n");
		/* Check if REMAIN on CHANNEL is Active */
		if (priv->rem_on_chan) {
			cfg_retval = cfg80211_rx_mgmt(priv->netdev,
				ieee80211_channel_to_frequency(priv->rem_on_chan->hw_value, IEEE80211_BAND_2GHZ),
				(uint8_t *)EventData, EventDataLength, GFP_ATOMIC);

			if (false == cfg_retval) {
				DEBUG(DBG_CIL, "CIL:P2P:cfg80211_rx_mgmt()"
					"returned error \n");
			}
		}
		break;
	}
	case UMI_EVT_RX_ACTION_FRAME:
	{
		bool cfg_retval;
		UMI_EVT_DATA_ACTION_FRAME *pActionFrame =
			(UMI_EVT_DATA_ACTION_FRAME *)EventData;

		DEBUG(DBG_CIL, "CIL:UMI_EVT_RX_ACTION_FRAME Received \n");
				

		cfg_retval = cfg80211_rx_mgmt(priv->netdev,
				ieee80211_channel_to_frequency(pActionFrame->Channel, IEEE80211_BAND_2GHZ),
				pActionFrame->pFrame,
				pActionFrame->FrameLength, GFP_ATOMIC);

		if (false == cfg_retval) {
			DEBUG(DBG_CIL, "CIL:P2P:cfg80211_rx_mgmt()"
					"returned error \n");
		}
		break;
	}
	default:
		DEBUG(DBG_CIL, "CIL:Event not recognized [%d] \n", Event);
	}
}

/*! \fn  void UMI_CB_ScanInfo(UL_HANDLE UpperHandle, uint32_t  cacheInfoLen,
		UMI_BSS_CACHE_INFO_IND		*pCacheInfo)
	brief 	This func is called by UMAC to indicate BSS List Entry
	param	UpperHandle	-upper layer driver instance for UMI callback.
	param	Oid	-OID number that associated with the parameter.
	param	Status	-Status code for the "get" operation.
	param	Length	-Number of bytes in the data.
	param	pValue	-The parameter data.
	return 	-None
*/
void UMI_CB_ScanInfo(UL_HANDLE UpperHandle,
			uint32_t  cacheInfoLen,
			UMI_BSS_CACHE_INFO_IND *pCacheInfo)
{
	struct CW1200_priv *priv = (struct CW1200_priv *)UpperHandle;
	struct cfg80211_bss *res = NULL;
	struct ieee80211_channel *channel = NULL;

	channel = ieee80211_get_channel(priv->netdev->ieee80211_ptr->wiphy,
			ieee80211_channel_to_frequency(pCacheInfo->channelNumber, IEEE80211_BAND_2GHZ));

#ifdef ENABLE_5GHZ
	if(!channel) {
		channel =ieee80211_get_channel(priv->netdev->ieee80211_ptr->wiphy,
			ieee80211_channel_to_frequency(pCacheInfo->channelNumber, IEEE80211_BAND_5GHZ));
	}
#endif

	if (!channel) {
		DEBUG(DBG_ERROR, "UMI_CB_ScanInfo():Unable to get channel \n");
		return;
	}

	res = cfg80211_inform_bss(priv->netdev->ieee80211_ptr->wiphy,
				channel, pCacheInfo->bssId,
				pCacheInfo->timeStamp,
				pCacheInfo->capability,
				pCacheInfo->beaconInterval,
				&pCacheInfo->ieElements[0],
				pCacheInfo->ieLength,
				pCacheInfo->rssi * 100, GFP_KERNEL);

	if (NULL == res)
		DEBUG(DBG_ERROR, "UMI_CB_ScanInfo():Error in creating BSS\n");
}

/*! \fn  void UMI_CB_GetParameterComplete (UL_HANDLE UpperHandle,
				uint16_t Oid, UMI_STATUS_CODE Status,
				uint16_t Length, void *pValue)
	brief  	This func is called by UMAC to indicate
			that a pending GET operation initiated
			by the driver is complete
	param	UpperHandle	- Upper layer driver instance for UMI callback.
	param	Oid	- The OID number that associated with the parameter.
	param	Status	-		Status code for the "get" operation.
	param	Length	-		Number of bytes in the data.
	param	pValue	-		The parameter data.
	return 	-	None
*/
void UMI_CB_GetParameterComplete(UL_HANDLE UpperHandle, uint16_t Oid,
				UMI_STATUS_CODE Status,
				uint16_t Length, void *pValue)
{
	struct CW1200_priv *priv = (struct CW1200_priv *)UpperHandle;

	DEBUG(DBG_CIL, "UMI_CB_GetParameterComplete() Called \n");

	if (UMI_STATUS_SUCCESS == Status) {
		/*Allocate a buffer to store the results */
		priv->get_buff_pointer = kmalloc(Length , GFP_KERNEL);
		if (NULL == priv->get_buff_pointer) {
			DEBUG(DBG_ERROR,
				"CIL - GetParameterComplete:Out of memory \n");
			EIL_Shutdown(priv);
			return;
		}
		memcpy(priv->get_buff_pointer , pValue, Length);
		priv->get_buff_len = Length;
	} else {
		priv->get_buff_pointer = NULL;
		priv->get_buff_len = 0;
	}

	/*Wake up the CIL_Get() thread */
	priv->wait_flag_get = TRUE;
	wake_up_interruptible(&priv->cil_get_wait);
}



/*! \fn  void UMI_CB_SetParameterCompletei (
				UL_HANDLE UpperHandle, uint16_t Oid,
				UMI_STATUS_CODE  Status)
		brief 	This func is called by UMAC to
				indicate that a pending SET operation
				initiated by the driver is complete
		param	UpperHandle	- The upper layer driver
				instance for UMI callback.
		param	Oid	- OID number
				associated with the parameter.
		param	Status	-	Status code for the "set" operation.
		return 		- 	None
*/
void UMI_CB_SetParameterComplete(UL_HANDLE UpperHandle,
			uint16_t Oid, UMI_STATUS_CODE  Status)
{
	struct CW1200_priv *priv = (struct CW1200_priv *)UpperHandle;

	DEBUG(DBG_CIL, "UMI_CB_SetParameterComplete() Called [%x] \n", Oid);

	if (UMI_STATUS_SUCCESS == Status) {
		/*Allocate a buffer to store the results */
		priv->set_status = SUCCESS;

		if (UMI_DEVICE_OID_802_11_CHANGE_CHANNEL == Oid) {
			/* Inform WPA Supplicant that REMAIN on CHANNEL has started */
			cfg80211_ready_on_channel(priv->netdev,
				priv->rem_on_chan_cookie,
				priv->rem_on_chan,
				priv->rem_on_channel_type,
				priv->rem_on_chan_duration, GFP_ATOMIC);
			/* Scheduled Delayed WQ for RESTORE CHAN */
			/* Due to a FW issue starting Timer for a longer value */
			queue_delayed_work(priv->bss_loss_WQ,
					&priv->roc_work,
					msecs_to_jiffies(priv->rem_on_chan_duration));
		}
	} else {
		priv->set_status = ERROR;
		DEBUG(DBG_CIL, "UMI_CB_SetParameterComplete()"
				"return error [%x] \n", Status);
	}

	/*Wake up the CIL_Set() thread */
	if (atomic_sub_return(1, &priv->cil_set_cond) == 0) {
		priv->wait_flag_set = TRUE;
		wake_up_interruptible(&priv->cil_set_wait);
	}
}



/******************************************************************************
*				CFG80211 Ops
*******************************************************************************/
static int cw1200_change_virtual_intf(struct wiphy *wiphy,
			struct  net_device *dev,
			enum nl80211_iftype type, u32 *flags,
			struct vif_params *params)
{
	struct cfg_priv *c_priv = NULL;
	uint32_t mode;
	int32_t retval = SUCCESS;
	uint32_t oprPowerMode = 0;
	int result = SUCCESS;

	DEBUG(DBG_CIL, "cw1200_change_virtual_intf called [%d] \n" , type);

	c_priv = wdev_priv(dev->ieee80211_ptr);

	DOWN(&c_priv->driver_priv->cil_sem, NULL);

	switch (type) {
	case NL80211_IFTYPE_ADHOC:
		dev->ieee80211_ptr->iftype = NL80211_IFTYPE_ADHOC;
		c_priv->driver_priv->device_mode = NL80211_IFTYPE_ADHOC;
		mode = UMI_802_IBSS;
		break;
	case NL80211_IFTYPE_STATION:
		dev->ieee80211_ptr->iftype = NL80211_IFTYPE_STATION;
		c_priv->driver_priv->device_mode = NL80211_IFTYPE_STATION;
		mode = UMI_802_INFRASTRUCTURE;
		break;
	case NL80211_IFTYPE_AP:
		dev->ieee80211_ptr->iftype = NL80211_IFTYPE_AP;
		c_priv->driver_priv->device_mode = NL80211_IFTYPE_AP;
		/* Set mode to AP*/
		mode = 2;
		/* Allocate internal structures needed for AP mode */
		c_priv->driver_priv->sta_info = kzalloc(
				sizeof(UMI_EVT_DATA_CONNECT_INFO) * MAX_SUPPORTED_STA,
				GFP_KERNEL);

		if (!c_priv->driver_priv->sta_info) {
			DEBUG(DBG_CIL , "%s, Out of Memory \n");
			result = -ENOMEM;
			goto fail;
		}
		break;
	case NL80211_IFTYPE_MONITOR:
		dev->ieee80211_ptr->iftype = NL80211_IFTYPE_MONITOR;
		c_priv->driver_priv->device_mode = NL80211_IFTYPE_MONITOR;
		/* Set mode to AP*/
		mode = 6;
		break;
	case NL80211_IFTYPE_P2P_CLIENT:
		dev->ieee80211_ptr->iftype = NL80211_IFTYPE_P2P_CLIENT;
		/* We will keep internal mode as STATION */
		c_priv->driver_priv->device_mode = NL80211_IFTYPE_STATION;
		break;
	case NL80211_IFTYPE_P2P_GO:
		dev->ieee80211_ptr->iftype = NL80211_IFTYPE_P2P_GO;
		/* We will keep internal mode as AP */
		c_priv->driver_priv->device_mode = NL80211_IFTYPE_AP;
		break;
	default:
		result = -EINVAL;
		goto fail;
	}

	if (mode < 2) {
		retval = CIL_Set(c_priv->driver_priv,
			UMI_DEVICE_OID_802_11_INFRASTRUCTURE_MODE,
			&mode, 4);
		if (retval) {
			result = -EIO;
			goto fail;
		}
	}
	/* Set operational power mode */
	oprPowerMode = c_priv->driver_priv->operational_power_mode;
	retval = CIL_Set(c_priv->driver_priv,
			UMI_DEVICE_OID_802_11_OPERATIONAL_POWER_MODE,
			&oprPowerMode, 4);
	if (retval) {
		DEBUG(DBG_CIL, "CIL_Set() for UMI_DEVICE"
			"_OID_802_11_OPERATIONAL_POWER_MODE returned error  \n");
	}

fail:
	UP(&c_priv->driver_priv->cil_sem, NULL);
	return result;
}


static int cw1200_scan(struct wiphy *wiphy,
		struct net_device *ndev,
		struct cfg80211_scan_request *request)
{
	struct cfg_priv *c_priv = NULL;
	UMI_BSS_LIST_SCAN *scanreq = NULL;
	CW1200_STATUS_E retval = SUCCESS;
	struct CW1200_priv *priv = NULL;
	uint32_t i;
	UMI_CHANNELS *u_channel;
	struct cfg80211_ssid *cssids;
	UMI_UPDATE_VENDOR_IE vendor_ie;
	int result = SUCCESS;

	cssids = request->ssids;
	DEBUG(DBG_CIL, "cw1200_scan Called \n");
	uint32_t num_2ghz_chan=0;
#ifdef ENABLE_5GHZ
	uint32_t num_5ghz_chan=0;
#endif
	c_priv = wdev_priv(ndev->ieee80211_ptr);
	priv = netdev_priv(ndev);

	DOWN(&priv->cil_sem, "expected: SCAN_EVENT_COMPLETE");
	priv->wait_event = UMI_EVT_SCAN_COMPLETED;

	if (priv->connection_status == CW1200_CONNECTING) {
		DEBUG(DBG_CIL, "cw1200_scan when connecting. Discard.\n");
		result = -EBUSY;
		goto fail;
	}

	scanreq = (UMI_BSS_LIST_SCAN *)kzalloc
			((request->n_channels)*sizeof(UMI_CHANNELS) +
			sizeof(UMI_BSS_LIST_SCAN), GFP_KERNEL);
	if (NULL == scanreq) {
		DEBUG(DBG_CIL, "CIL:cw1200_scan():Out of memory \n");
		result = -ENOMEM;
		goto fail;
	}
	/*Currently the UMAC has support for scanning only one SSID
	So issue a passive scan for all channels */
	if (request->n_ssids > 2)
		request->n_ssids = 2;
	if (priv->request == NULL) {
		/*Store the SCAN request param to be used in SCAN done*/
		priv->request = request;
	} else {
		DEBUG(DBG_CIL, "CIL:cw1200_scan():already in progress\n");
		result = -EAGAIN;
		goto fail;
	}

	u_channel = scanreq->Channels;

	for (i = 0; i < request->n_channels; i++) {

		if (!(request->channels[i]->flags & IEEE80211_CHAN_PASSIVE_SCAN)) {
			scanreq->NumOfProbeRequests = 1;
		}
		if (NL80211_BAND_2GHZ == request->channels[i]->band) {
			num_2ghz_chan++;
		}
#ifdef ENABLE_5GHZ
		else {
			num_5ghz_chan++;
		}
#endif
		u_channel[i].ChannelNum = request->channels[i]->hw_value;
		u_channel[i].MinChannelTime = 40;
		u_channel[i].TxPowerLevel = request->channels[i]->max_power * 10;
		u_channel[i].MaxChannelTime = 120;
	}
	scanreq->NumberOfChannels_2_4Ghz = num_2ghz_chan;
#ifdef ENABLE_5GHZ
	scanreq->NumberOfChannels_5Ghz = num_5ghz_chan;
#endif

	if (cssids->ssid_len) {
		memcpy(scanreq->Ssids[0].ssid, cssids->ssid, cssids->ssid_len);
		scanreq->Ssids[0].ssidLength = cssids->ssid_len;
		scanreq->NumOfSSIDs = request->n_ssids;
	}

	if (request->ie) {
		memset(&vendor_ie, 0, sizeof(UMI_UPDATE_VENDOR_IE));
		vendor_ie.pProbeReqIE = request->ie;
		vendor_ie.probeReqIElength = request->ie_len;

		retval = CIL_Set(c_priv->driver_priv,
			UMI_DEVICE_OID_802_11_UPDATE_VENDOR_IE ,
			&vendor_ie, sizeof(UMI_UPDATE_VENDOR_IE));
		if (retval) {
			DEBUG(DBG_CIL, "cw1200_scan():CIL_Set"
				"for VENDOR IE returned error \n");
			priv->request = NULL;
			result = -EIO;
			goto fail;
		}
	}

	/* Enable AUTO scan */
#ifdef ENABLE_5GHZ
 	scanreq->flags = 1;
#else
	scanreq->flags = 2;
#endif

	retval = CIL_Set(c_priv->driver_priv,
			UMI_DEVICE_OID_802_11_BSSID_LIST_SCAN,
			scanreq, sizeof(UMI_BSS_LIST_SCAN) +
			(request->n_channels)*sizeof(UMI_CHANNELS));
	kfree(scanreq);
	scanreq = NULL;
	if (retval) {
		DEBUG(DBG_CIL, "cw1200_scan():returned error\n");
		priv->request = NULL;
		cw1200_handle_delayed_link_loss(priv);
		result = -EAGAIN;
		goto fail;
	}
	return SUCCESS;

fail:
	if (scanreq)
		kfree(scanreq);
	UP(&priv->cil_sem, NULL);

	return result;
}


static int cw1200_join_ibss(struct wiphy *wiphy,
			struct net_device *ndev,
			struct cfg80211_ibss_params *params)
{
	UMI_802_11_SSID ssid_info;
	CW1200_STATUS_E retval = SUCCESS;
	struct cfg_priv *c_priv = NULL;
	int result = SUCCESS;

	DEBUG(DBG_CIL, "cw1200_join_ibss Called \n");

	ssid_info.ssidLength = params->ssid_len;
	memcpy(&ssid_info.ssid, params->ssid, params->ssid_len);
	c_priv = wdev_priv(ndev->ieee80211_ptr);

	DOWN(&c_priv->driver_priv->cil_sem, NULL);

	retval = CIL_Set(c_priv->driver_priv, UMI_DEVICE_OID_802_11_SSID,
			&ssid_info, sizeof(UMI_802_11_SSID));

	if (retval) {
		DEBUG(DBG_CIL, "cw1200_join_ibss():Error[%d]\n", retval);
		result -EIO;
	}
	UP(&c_priv->driver_priv->cil_sem, NULL);

	return result;
}

static int cw1200_auth(struct wiphy *wiphy,
		struct net_device *ndev,
		struct cfg80211_auth_request *req)
{
	uint8_t auth_mode = 0;
	struct cfg_priv *c_priv = NULL;
	uint8_t bssid[ETH_ALEN];
	CW1200_STATUS_E retval = SUCCESS;
	uint32_t power_save = 0;
	int result = SUCCESS;

	DEBUG(DBG_CIL, "cw1200_auth Called \n");

	switch (req->auth_type) {
	case NL80211_AUTHTYPE_OPEN_SYSTEM:
		auth_mode = UMI_OPEN_SYSTEM;
		break;
	case NL80211_AUTHTYPE_SHARED_KEY:
		auth_mode = UMI_SHARED_KEY;
		break;
	default:
		return -EOPNOTSUPP;
	}

	c_priv = wdev_priv(ndev->ieee80211_ptr);

	DOWN(&c_priv->driver_priv->cil_sem, NULL);
	retval = CIL_Set(c_priv->driver_priv,
			UMI_DEVICE_OID_802_11_AUTHENTICATION_MODE,
			&auth_mode, sizeof(uint8_t));
	if (retval) {
		DEBUG(DBG_CIL, "cw1200_auth():AUTH MODE returned error\n");
		result = -EIO;
		goto fail;
	}

	/*By default power save is enabled in the device.
		Disabling Power Save */
	retval = CIL_Set(c_priv->driver_priv,
			UMI_DEVICE_OID_802_11_POWER_MODE,
			&power_save, 4);
	if (retval) {
		DEBUG(DBG_CIL, "CIL_Set for POWER MODE returned error \n");
		result = -EIO;
		goto fail;
	}

	memcpy(&bssid[0], req->bss->bssid, ETH_ALEN);
	retval = CIL_Set(c_priv->driver_priv,
			UMI_DEVICE_OID_802_11_BSSID,
			&bssid[0], ETH_ALEN);
	if (retval) {
		DEBUG(DBG_CIL, "cw1200_scan():CIL_Set for"
				"BSSID returned error\n");
		result = -EIO;
		goto fail;
	}

fail:
	UP(&c_priv->driver_priv->cil_sem, NULL);
	return result;
}


static int cw1200_add_key(struct wiphy *wiphy, struct net_device *ndev,
				u8 key_idx, bool pairwise, const u8 *mac_addr,
				struct key_params *params)
{
	struct cfg_priv *c_priv = NULL;
	CW1200_STATUS_E retval = SUCCESS;

	DEBUG(DBG_CIL, "cw1200_add_key Called \n");

	c_priv = wdev_priv(ndev->ieee80211_ptr);
	DOWN(&c_priv->driver_priv->cil_sem, NULL);
	retval = __cw1200_add_key(wiphy, ndev, key_idx, pairwise, mac_addr, params);
	UP(&c_priv->driver_priv->cil_sem, NULL);

	return retval;
}

static int __cw1200_add_key(struct wiphy *wiphy, struct net_device *ndev,
				u8 key_idx, bool pairwise, const u8 *mac_addr,
				struct key_params *params)
{
	UMI_OID_802_11_KEY s_key;
	uint32_t encr = 0;
	struct cfg_priv *c_priv = NULL;
	CW1200_STATUS_E retval = SUCCESS;
	int result = SUCCESS;

	DEBUG(DBG_CIL, "__cw1200_add_key Called \n");

	c_priv = wdev_priv(ndev->ieee80211_ptr);

	/* Moved this up as this will bbe used for key_index map */
	if (NL80211_IFTYPE_AP <= c_priv->driver_priv->device_mode) {
		c_priv->driver_priv->setget_linkid = CIL_get_sta_linkid(c_priv->driver_priv, mac_addr);
		if ( c_priv->driver_priv->setget_linkid < 0) {
			result = -EIO;
			goto fail;
		}
	} else {
		/* Hardcoding right now */
		c_priv->driver_priv->setget_linkid = 1;
	}

	switch (params->cipher) {
	case WLAN_CIPHER_SUITE_WEP40:
	case WLAN_CIPHER_SUITE_WEP104:
		encr = 0x1;/*WEP Mode:Ref API doc */
		DEBUG(DBG_CIL, "WLAN_CIPHER_SUITE_WEP40/104:[%d]\n", key_idx);
		/* keys to be placed at 0,3,4,5, */
		if (0 == key_idx) {
			s_key.entryIndex = 0;
		} else {
			s_key.entryIndex = key_idx + 2;
		}
		s_key.keyType = WEP_GROUP;
		s_key.Key.WepGroupKey.keyId = key_idx ;
		memcpy(&(s_key.Key.WepGroupKey.keyData[0]),
				params->key, params->key_len);
		s_key.Key.WepGroupKey.keyLength = params->key_len;
		break;
	case WLAN_CIPHER_SUITE_TKIP:
		encr = 0x2;
		DEBUG(DBG_CIL, "WLAN_CIPHER_SUITE_TKIP\n");
		if (mac_addr) {
			DEBUG(DBG_CIL, "CIL:TKIP PAIRWISE\n");
			s_key.keyType = TKIP_PAIR;
			s_key.entryIndex = c_priv->driver_priv->setget_linkid;
			memcpy(&(s_key.Key.TkipPairwiseKey.peerAddress[0]),
					mac_addr, ETH_ALEN);
			memcpy(&(s_key.Key.TkipPairwiseKey.tkipKeyData[0]),
					params->key, params->key_len);
			memcpy(&(s_key.Key.TkipPairwiseKey.rxMicKey[0]),
					params->key + 24, 8);
			memcpy(&(s_key.Key.TkipPairwiseKey.txMicKey[0]),
					params->key + 16, 8);
		} else {
			DEBUG(DBG_CIL, "CIL:TKIP GROUPWISE \n");
			s_key.keyType = TKIP_GROUP;
			s_key.entryIndex = 0;
			s_key.Key.TkipGroupKey.keyId = key_idx;
			memcpy(&(s_key.Key.TkipGroupKey.tkipKeyData[0]),
					params->key, 16);
			memcpy(&(s_key.Key.TkipGroupKey.rxMicKey),
					params->key + 24, 8);
			memcpy(&(s_key.Key.TkipGroupKey.rxSequenceCounter),
					params->seq, params->seq_len);
		}
		break;

	case WLAN_CIPHER_SUITE_CCMP:
		encr = 0x4;
		DEBUG(DBG_CIL, "WLAN_CIPHER_SUITE_CCMP \n");
		if (mac_addr) {
			s_key.keyType = AES_PAIR;
			s_key.entryIndex = c_priv->driver_priv->setget_linkid;
			memcpy(&(s_key.Key.AesPairwiseKey.aesKeyData[0]),
				params->key, params->key_len);
			memcpy(&(s_key.Key.AesPairwiseKey.peerAddress[0]),
					mac_addr, ETH_ALEN);
		} else {
			s_key.keyType = AES_GROUP;
			s_key.entryIndex = 0;
			s_key.Key.AesGroupKey.keyId = key_idx ;
			memcpy(&(s_key.Key.AesGroupKey.aesKeyData[0]),
				params->key, params->key_len);
			memcpy(&(s_key.Key.AesGroupKey.rxSequenceCounter),
					params->seq , params->seq_len);
		}
		break;
#ifdef WAPI
	case WLAN_CIPHER_SUITE_SMS4:
		encr = 0x8;
		DEBUG(DBG_CIL, "WLAN_CIPHER_SUITE_SMS4/WAPI \n");
		if (mac_addr) {
			s_key.keyType = WAPI_PAIR;
			s_key.entryIndex = key_idx * 2 + 1;
			WARN_ON(params->key_len != 16 + 16); // key + mic
			s_key.Key.WapiPairwiseKey.keyId = key_idx;
			memcpy(&(s_key.Key.WapiPairwiseKey.wapiKeyData[0]),
					params->key, 16);
			memcpy(&(s_key.Key.WapiPairwiseKey.peerAddress[0]),
					mac_addr, ETH_ALEN);
			/* Copy MIC key of length 16 bytes from 32th offset */
			memcpy(&(s_key.Key.WapiPairwiseKey.micKeyData[0]),
					params->key + 16, 16);

		} else {
			s_key.keyType = WAPI_GROUP;
			s_key.entryIndex = key_idx * 2 + 0;
			s_key.Key.WapiGroupKey.keyId = key_idx;
			WARN_ON(params->key_len != 16 + 16); // key + mic
			memcpy(&(s_key.Key.WapiGroupKey.wapiKeyData[0]),
					params->key, 16);
			/* Copy MIC key of length 16 bytes from 32th offset */
			memcpy(&(s_key.Key.WapiGroupKey.micKeyData[0]),
					params->key + 16, 16);
		}
		break;
#endif
	default:
		result = -EINVAL;
		goto fail;
	}

	retval = CIL_Set(c_priv->driver_priv,
			UMI_DEVICE_OID_802_11_ENCRYPTION_STATUS,
			&encr, sizeof(uint32_t));
	if (retval) {
		DEBUG(DBG_CIL, "cw1200_add_key():CIL_Set"
				"for ENCRYPTION status \n");
		result = -EIO;
		goto fail;
	}
	DEBUG(DBG_CIL, "cw1200_add_key : linkId = %d\n",c_priv->driver_priv->setget_linkid);
	/* STA mode reset linkId to 0*/
	if (NL80211_IFTYPE_AP > c_priv->driver_priv->device_mode) {
		c_priv->driver_priv->setget_linkid = 0;
	}
	retval = CIL_Set(c_priv->driver_priv,
			UMI_DEVICE_OID_802_11_ADD_KEY,
			&s_key, sizeof(UMI_OID_802_11_KEY));
	/* Reset */
	c_priv->driver_priv->setget_linkid = 0;
	if (retval) {
		DEBUG(DBG_CIL, "cw1200_add_key():returned error \n");
		result = -EIO;
		goto fail;
	}

fail:
	return result;
}


static int cw1200_del_key(struct wiphy *wiphy,
			struct net_device *ndev,
			u8 key_idx, bool pairwise, const u8 *mac_addr)
{
	struct cfg_priv *c_priv = NULL;
	uint32_t  entryid = 0;
	CW1200_STATUS_E retval = SUCCESS;
	UMI_OID_802_11_REMOVE_KEY removekey = {0};
	int result = SUCCESS;

	DEBUG(DBG_CIL, "cw1200_del_key Called for MAC addr %pM key_idx=%d\n",mac_addr,key_idx);
	c_priv = wdev_priv(ndev->ieee80211_ptr);

	DOWN(&c_priv->driver_priv->cil_sem, NULL);
	/* Entry index will be 0 for group key and will be mapped to linkId in case of SoftAP
	 * In STATION mode peer key Entry Index will be 1.
	 */
	if (NL80211_IFTYPE_AP <= c_priv->driver_priv->device_mode) {
		c_priv->driver_priv->setget_linkid = CIL_get_sta_linkid(c_priv->driver_priv, mac_addr);
		if ( c_priv->driver_priv->setget_linkid < 0) {
			result = -EIO;
			goto fail;
		}
		if (mac_addr)
			removekey.EntryIndex = c_priv->driver_priv->setget_linkid; /*Peer Key */
		else
			removekey.EntryIndex = 0;/*Group Key */
	} else {
		/* Hardcoding right now */
		removekey.EntryIndex = key_idx;
	}
	if (CW1200_DISCONNECTED != c_priv->driver_priv->connection_status) {
		DEBUG(DBG_CIL, "cw1200_del_key Called for linkId = %d and Entry Index is = %d\n",
					c_priv->driver_priv->setget_linkid,removekey.EntryIndex);
		/* STA mode reset linkId to 0*/
		retval = CIL_Set(c_priv->driver_priv,
				UMI_DEVICE_OID_802_11_REMOVE_KEY,
				&removekey, sizeof(UMI_OID_802_11_REMOVE_KEY));
		c_priv->driver_priv->setget_linkid = 0;
		if (retval) {
			DEBUG(DBG_CIL, "cw1200_del_key():returned error \n");
			result = -EIO;
			goto fail;
		}
	}
fail:
	UP(&c_priv->driver_priv->cil_sem, NULL);
	return result;
}

static int cw1200_leave_ibss(struct wiphy *wiphy, struct net_device *ndev)
{
	struct cfg_priv *c_priv = NULL;
	CW1200_STATUS_E retval = SUCCESS;
	int result = SUCCESS;

	DEBUG(DBG_CIL, "cw1200_leave_ibss Called \n");

	c_priv = wdev_priv(ndev->ieee80211_ptr);

	DOWN(&c_priv->driver_priv->cil_sem, NULL);
	retval = CIL_Set(c_priv->driver_priv,
				UMI_DEVICE_OID_802_11_DISASSOCIATE, NULL, 0);
	if (retval) {
		DEBUG(DBG_CIL, "cw1200_leave_ibss():"
				"CIL_Set for DISASSOC returned error \n");
		result = -EIO;
	}
	UP(&c_priv->driver_priv->cil_sem, NULL);

	return result;
}


static int  cw1200_connect(struct wiphy *wiphy, struct net_device *dev,
			struct cfg80211_connect_params *sme)
{
	uint8_t auth_mode = 0;
	struct cfg_priv *c_priv = NULL;
	uint8_t bssid[ETH_ALEN];
	CW1200_STATUS_E retval = SUCCESS;
	uint32_t PowerLevel = 0;
	UMI_802_11_SSID ssid_info;
	struct key_params key_param;
	struct cfg80211_crypto_settings *csettings = NULL;
	UMI_BSSID_OID bssid_oid = {0};
	struct ieee80211_channel *channel = sme->channel;
	UMI_UPDATE_VENDOR_IE ie;
	UMI_BLOCK_ACK_POLICY block_ack;
	uint32_t encr = 0;
	UMI_BEACON_LOST_COUNT beacon_threshold;
	UMI_UPDATE_VENDOR_IE vendor_ie;
	UMI_OID_802_11_SET_UAPSD uapsd_oid;
	int result = SUCCESS;

	DEBUG(DBG_CIL, "cw1200_connect Called \n");

	csettings = &sme->crypto;
	if (channel)
		PowerLevel = channel->max_power * 10;
	/*
	* WPA_Vesrions
	* WPA_1   for wpa
	* WPA_2  for wpa2
	*/
	if (csettings->wpa_versions == NL80211_WPA_VERSION_1) {
		/*WPA */
		/*
		* #define WLAN_AKM_SUITE_8021X            0x000FAC01
		* #define WLAN_AKM_SUITE_PSK              0x000FAC02
		* #define WPA_AKM_SUITE_802_1X            0x0050F201
		* #define WPA_AKM_SUITE_PSK               0x0050F202
		*/

		if (csettings->akm_suites[0] == WLAN_AKM_SUITE_PSK) {
			DEBUG(DBG_CIL, "CIL: %s UMI_RSN_PSK \n", __func__);
			auth_mode = UMI_WPA_PSK;
		} else if (csettings->akm_suites[0] == WPA_AKM_SUITE_PSK) {
			DEBUG(DBG_CIL, "CIL: %s UMI_WPA_PSK \n", __func__);
			auth_mode = UMI_WPA_PSK;
		} else if (csettings->akm_suites[0] == WLAN_AKM_SUITE_8021X
				||
			csettings->akm_suites[0] == WPA_AKM_SUITE_802_1X) {
			DEBUG(DBG_CIL, "CIL: %s UMI_WPA_UNSPEC_TKIP \n", __func__);
			auth_mode = UMI_WPA_TKIP;
		} else {
			DEBUG(DBG_CIL, "CIL: %s unsupported akm suite %0X \n", __func__, csettings->akm_suites[0]);
			auth_mode = UMI_OPEN_SYSTEM;
		}

	} else if (csettings->wpa_versions == NL80211_WPA_VERSION_2) {
		/*WPA2*/
		if (csettings->akm_suites[0] == WLAN_AKM_SUITE_PSK) {
			DEBUG(DBG_CIL, "CIL: %s UMI_RSN_PSK \n", __func__);
			auth_mode = UMI_WPA2_PSK;
		} else {
			auth_mode = UMI_WPA2_AES;
			DEBUG(DBG_CIL, "CIL: %s UMI_RSN_UNSPEC_AES \n", __func__);
		}
	}
#ifdef WAPI
	else if (csettings->wpa_versions == NL80211_WAPI_VERSION_1) {
			/* WAPI */
		auth_mode = UMI_WAPI_1 ;
		}
#endif
	else {
		/*If WPA/WPA2 is not set then it must be OPEN */
		switch (sme->auth_type) {
		case NL80211_AUTHTYPE_AUTOMATIC:
		case NL80211_AUTHTYPE_OPEN_SYSTEM:
			auth_mode = UMI_OPEN_SYSTEM;
			break;
		case NL80211_AUTHTYPE_SHARED_KEY:
			auth_mode = UMI_SHARED_KEY;
			break;
		default:
			return -EOPNOTSUPP;
		}
	}
	c_priv = wdev_priv(dev->ieee80211_ptr);

	DOWN(&c_priv->driver_priv->cil_sem, "expected: UMI_EVT_CONNECTED");
	c_priv->driver_priv->wait_event = UMI_EVT_CONNECTED;

	retval = CIL_Set(c_priv->driver_priv,
			UMI_DEVICE_OID_802_11_AUTHENTICATION_MODE,
			&auth_mode, sizeof(uint8_t));

	if (retval) {
		DEBUG(DBG_CIL, "cw1200_connect():CIL_Set"
				"for AUTH MODE returned error \n");
		result = -EIO;
		goto fail;
	}

	/* Enable all ENCR modes */
	encr = 0xF;
	retval = CIL_Set(c_priv->driver_priv,
			UMI_DEVICE_OID_802_11_ENCRYPTION_STATUS,
			&encr, sizeof(uint32_t));

	if (retval) {
		DEBUG(DBG_CIL, "cw1200_connect():CIL_Set"
				"for ENCRYPTION status \n");
		result = -EIO;
		goto fail;
	}

	retval = CIL_Set(c_priv->driver_priv,
			UMI_DEVICE_OID_802_11_TX_POWER_LEVEL,
			&PowerLevel, 4);
	if (retval) {
		DEBUG(DBG_CIL, "cw1200_connect():CIL_Set"
				"Power Level returned error \n");
		result = -EIO;
		goto fail;
	}

	/* Set UAPSD flags if enabled */
	if (sme->uapsd != -1) {
		memset(&uapsd_oid, 0, sizeof(UMI_OID_802_11_SET_UAPSD));
		DEBUG(DBG_CIL, "%s,UAPSD flags [%x] \n", __func__, sme->uapsd);

		uapsd_oid.uapsdFlags = sme->uapsd;

		if (c_priv->driver_priv->minAutoTriggerInterval) {
			/* Enable Pseudo UAPSD */
			uapsd_oid.uapsdFlags |= 0x10;
			uapsd_oid.minAutoTriggerInterval =
				c_priv->driver_priv->minAutoTriggerInterval ;
			uapsd_oid.maxAutoTriggerInterval =
				c_priv->driver_priv->maxAutoTriggerInterval;
			uapsd_oid.autoTriggerStep =
				c_priv->driver_priv->autoTriggerStep;
		}

		retval = CIL_Set(c_priv->driver_priv,
			UMI_DEVICE_OID_802_11_SET_UAPSD,
			&uapsd_oid, sizeof(UMI_OID_802_11_SET_UAPSD));
		if (retval) {
			DEBUG(DBG_CIL, "cw1200_connect():CIL_Set"
				"for UMI_OID_802_11_SET_UAPSD status \n");
			result = -EIO;
			goto fail;
		}
	}

	/*By default power save is enabled in the device.Disabling Power Save */
	/*Set the KEY Info */
	if (sme->key) {
		key_param.key = kmemdup(sme->key, sme->key_len, GFP_KERNEL);
		if (key_param.key == NULL) {
			result = -ENOMEM;
			goto fail;
		}

		key_param.key_len = sme->key_len;
		key_param.seq_len = 0;
		key_param.cipher = sme->crypto.ciphers_pairwise[0];
		retval = __cw1200_add_key(wiphy, dev, sme->key_idx, sme->bssid ,
					1, &key_param);

		kfree(key_param.key);
		if (retval < 0) {
			DEBUG(DBG_CIL, "CIL:Invalid key_params\n");
			result = retval;
			goto fail;
		}
	}
	/* Update WPS IE to be sent in ASSOC request */
	if (sme->ie_len != 0) {
		DEBUG(DBG_CIL, "cw1200_connect():IE received \n");
		/* Check if WPS IE is received */
		if (0xdd == sme->ie[0] && (GET_BE32(&sme->ie[2]) == WPS_IE_VENDOR_TYPE ) ) {
			DEBUG(DBG_CIL, "cw1200_connect():WPS IE found\n");

			memset(&vendor_ie, 0, sizeof(UMI_UPDATE_VENDOR_IE));
			vendor_ie.probeReqIElength = vendor_ie.assocReqIElength = sme->ie_len;
			vendor_ie.pProbeReqIE = vendor_ie.pAssocReqIE = sme->ie;
			retval = CIL_Set(c_priv->driver_priv,
				UMI_DEVICE_OID_802_11_UPDATE_VENDOR_IE ,
				&vendor_ie, sizeof(UMI_UPDATE_VENDOR_IE));

			if (retval) {
				DEBUG(DBG_CIL, "cw1200_connect():CIL_Set"
					"for VENDOR IE returned error \n");
				result = -EIO;
				goto fail;
			}
		} else {
			/* Check if P2P IE is received, we have to find it among
			 * other IEs. */
			u8 *pos = sme->ie;
			while (pos < &sme->ie[sme->ie_len-7]) {
				if (0xdd == *pos && (GET_BE32(&pos[2]) == P2P_IE_VENDOR_TYPE ) ) {
					DEBUG(DBG_CIL, "cw1200_connect(): P2P IE found\n");

					memset(&vendor_ie, 0, sizeof(UMI_UPDATE_VENDOR_IE));
					vendor_ie.probeReqIElength =
						vendor_ie.assocReqIElength = pos[1] + 2;
					vendor_ie.pProbeReqIE = vendor_ie.pAssocReqIE = pos;
					retval = CIL_Set(c_priv->driver_priv,
							UMI_DEVICE_OID_802_11_UPDATE_VENDOR_IE ,
							&vendor_ie, sizeof(UMI_UPDATE_VENDOR_IE));

					if (retval) {
						DEBUG(DBG_CIL, "cw1200_connect():CIL_Set"
								"for VENDOR IE returned error \n");
						result = -EIO;
						goto fail;
					}
				}
				pos += pos[1] + 2;
			}
		}
	}

	/* SET Block ACK policy for 11n AMPDU */
	block_ack.blockAckTxTidPolicy = c_priv->driver_priv->tx_block_ack;
	block_ack.blockAckRxTidPolicy = c_priv->driver_priv->rx_block_ack;
	retval = CIL_Set(c_priv->driver_priv,
			UMI_DEVICE_OID_802_11_BLOCK_ACK_POLICY ,
			&block_ack, sizeof(UMI_BLOCK_ACK_POLICY));
	if (retval) {
		DEBUG(DBG_CIL, "cw1200_connect():CIL_Set"
				"for BLOCK ACK returned error \n");
		result = -EIO;
		goto fail;
	}

	beacon_threshold.BeaconLostCount =
			c_priv->driver_priv->cqm_beacon_loss_count;

	retval = CIL_Set(c_priv->driver_priv,
			UMI_DEVICE_OID_802_11_SET_BEACON_LOST_COUNT,
			&beacon_threshold, sizeof(UMI_BEACON_LOST_COUNT));

	if (retval) {
		DEBUG(DBG_CIL, "cw1200_connect():"
				"SET_BEACON_LOSS value falied \n");
		retval = -EIO;
	}
	if (sme->bssid) {
		memcpy(&bssid_oid.Bssid[0], sme->bssid, ETH_ALEN);
		/* If ROAM flag set and Same SSID send reassoc */
		if (sme->prev_bssid &&
		    memcmp(&c_priv->driver_priv->connected_ssid, sme->ssid, sme->ssid_len) == 0) {
			DEBUG(DBG_CIL, "cw1200_connect(): reassoc \n");
			bssid_oid.RoamingEnable = 1;
		} else {
			bssid_oid.RoamingEnable = 0;
		}

		c_priv->driver_priv->connection_status = CW1200_CONNECTING;
		retval = CIL_Set(c_priv->driver_priv,
				UMI_DEVICE_OID_802_11_BSSID,
				&bssid_oid, sizeof(UMI_BSSID_OID));
		if (retval) {
			/* It was observed that sometimes UMAC is
			internally connected. Force disconnect.*/
			c_priv->driver_priv->connection_status = CW1200_DISCONNECTED;
			retval = CIL_Set(c_priv->driver_priv,
				UMI_DEVICE_OID_802_11_DISASSOCIATE, NULL, 0);
			if (retval) {
				DEBUG(DBG_CIL, "cw1200_connect():CIL_Set"
						"for BSSID returned error\n");
			}
			result = -EIO;
			goto fail;
		}
		queue_delayed_work(c_priv->driver_priv->bss_loss_WQ,
				&c_priv->driver_priv->connect_result, CONNECT_TIMEOUT);
	} else if (sme->ssid) {
		ssid_info.ssidLength = sme->ssid_len;
		memcpy(&ssid_info.ssid, sme->ssid, sme->ssid_len);

		c_priv->driver_priv->connection_status = CW1200_CONNECTING;
		retval = CIL_Set(c_priv->driver_priv,
				UMI_DEVICE_OID_802_11_SSID,
				&ssid_info, sizeof(UMI_802_11_SSID));
		if (retval) {
			/* It was observed that sometimes UMAC is
			internally connected. Force disconnect.*/
			c_priv->driver_priv->connection_status = CW1200_DISCONNECTED;
			retval = CIL_Set(c_priv->driver_priv,
					UMI_DEVICE_OID_802_11_DISASSOCIATE,
					NULL, 0);
			if (retval) {
				DEBUG(DBG_CIL, "cw1200_connect():CIL_Set"
					"for SSID returned error\n");
			}
			result = -EIO;
			goto fail;
		}
		queue_delayed_work(c_priv->driver_priv->bss_loss_WQ,
				&c_priv->driver_priv->connect_result, CONNECT_TIMEOUT);
	} else
	{
		result = -EINVAL;
		goto fail;
	}
	/* Store the SSID to check for Reassoc req */
	memcpy(&c_priv->driver_priv->connected_ssid, sme->ssid, sme->ssid_len);

	return SUCCESS;

fail:
	UP(&c_priv->driver_priv->cil_sem, NULL);
	return result;
}

static int cw1200_assoc(struct wiphy *wiphy, struct net_device *dev,
			struct cfg80211_assoc_request *req)
{
	DEBUG(DBG_CIL, "cw1200_assoc, Called \n");
	return SUCCESS;
}

static int cw1200_disconnect(struct wiphy *wiphy, struct net_device *dev,
				u16 reason_code)
{
	struct cfg_priv *c_priv = NULL;
	CW1200_STATUS_E retval = SUCCESS;

	DEBUG(DBG_CIL, "cw1200_disconnect, Called:Reason Code: [%d] \n",
		reason_code);

	netdev_priv(dev);
	c_priv = wdev_priv(dev->ieee80211_ptr);
	DOWN(&c_priv->driver_priv->cil_sem, "expected: UMI_EVT_DISCONNECTED");
	c_priv->driver_priv->wait_event = UMI_EVT_DISCONNECTED;

	retval = CIL_Set(c_priv->driver_priv,
			UMI_DEVICE_OID_802_11_DISASSOCIATE,
			NULL, 0);
	if (retval) {
		DEBUG(DBG_CIL, "cw1200_disconnect():CIL_Set"
				"for DISCONN returned error\n");
	/* TODO: Check for return code and report error only if fatal.*/
	/* return -EIO;*/
		c_priv->driver_priv->wait_event = 0;
		UP(&c_priv->driver_priv->cil_sem, "UMI_EVT_DISCONNECTED");
	}
	return SUCCESS;
}


static int cw1200_set_default_key(struct wiphy *wiphy,
				struct net_device *ndev,
				u8 key_index, bool unicast, bool multicast)
{
	UMI_OID_802_11_WEP_DEFAULT_KEY_ID wep_default;
	CW1200_STATUS_E retval = SUCCESS;
	struct cfg_priv *c_priv = NULL;
	int result = SUCCESS;

	c_priv = wdev_priv(ndev->ieee80211_ptr);

	DOWN(&c_priv->driver_priv->cil_sem, NULL);
	DEBUG(DBG_CIL, "cw1200_set_default_key() called:Key_index:[%d]\n",
			key_index);

	wep_default.wepDefaultKeyId = key_index;

	retval = CIL_Set(c_priv->driver_priv, UMI_DEVICE_OID_802_11_WEP_DEFAULT_KEY_ID,
		&wep_default, sizeof(UMI_OID_802_11_WEP_DEFAULT_KEY_ID));

	if (retval) {
		DEBUG(DBG_CIL, "cw1200_set_default_key():CIL_Set"
				"for set default key returned error \n");
		result = -EIO;
	}
	UP(&c_priv->driver_priv->cil_sem, NULL);

	return result;
}

static int cw1200_set_power_mgmt(struct wiphy *wiphy,
		struct net_device *dev,
		bool enabled, int timeout)
{
	struct cfg_priv *c_priv = NULL;
	CW1200_STATUS_E retval = SUCCESS;
	struct CW1200_priv *priv = NULL;
	UMI_POWER_MODE power_mode;
	uint32_t oprPowerMode = 0;

	DEBUG(DBG_CIL, "cw1200_set_power_mgmt()"
			"called:Power Mode :[%d] \n", enabled);

	c_priv = wdev_priv(dev->ieee80211_ptr);

	/* Set operational power mode */
	oprPowerMode = c_priv->driver_priv->operational_power_mode;
	retval = CIL_Set(c_priv->driver_priv,
			UMI_DEVICE_OID_802_11_OPERATIONAL_POWER_MODE,
			&oprPowerMode, 4);
	if (retval) {
		DEBUG(DBG_CIL, "CIL_Set() for UMI_DEVICE"
			"_OID_802_11_OPERATIONAL_POWER_MODE returned error  \n");
	}

	/* Let device use default values for now */
	power_mode.ApPsmChangePeriod = 0;

	/* Enable POWER mode with FAST PSM. BIT:7 for Fast PSM*/
	if (enabled) {
		power_mode.PmMode = 0x81;
		/* Multiply by 2 as UMAC unit is 500us */
		power_mode.FastPsmIdlePeriod = timeout * 2;
	} else {
		power_mode.FastPsmIdlePeriod = 0;
		power_mode.PmMode = 0;
	}

	retval = CIL_Set(c_priv->driver_priv,
			UMI_DEVICE_OID_802_11_POWER_MODE,
			&power_mode, sizeof(UMI_POWER_MODE));
	if (retval) {
		DEBUG(DBG_CIL, "cw1200_set_power_mgmt():CIL_Set"
				"for POWER MODE returned error \n");
		return -EIO;
	}
	return SUCCESS;
}


static int tunnel_set_ps(struct wiphy *wiphy,
		struct ste_msg_ps_conf *tdata , int len)
{
	int retval = 0;
	struct CW1200_priv *priv = NULL;
	struct cfg_priv *c_priv = NULL;
	UMI_SET_ARP_IP_ADDR_FILTER set_arp_filter;

	c_priv = wiphy_priv(wiphy);
	priv = c_priv->driver_priv;

	DEBUG(DBG_CIL, "tunnel_set_ps called, "
			"tdata len= %d\n", len);

	if (tdata->flags & STE_PS_CONF_IEEE_PS) {

		DEBUG(DBG_CIL, "Enabling powersave \n");
		retval = cw1200_set_power_mgmt(wiphy, priv->netdev, 1,
						tdata->dynamic_ps_timeout);
		if (retval != SUCCESS)
			DEBUG(DBG_CIL, "error in enabling powersave");

	} else if (tdata->flags & STE_PS_CONF_IDLE) {

		retval = cw1200_set_power_mgmt(wiphy, priv->netdev, 1,
						tdata->dynamic_ps_timeout);
		DEBUG(DBG_CIL, "Enabling powersave \n");
		if (retval != SUCCESS)
			DEBUG(DBG_CIL, "error in enabling powersave");

	} else {

		retval = cw1200_set_power_mgmt(wiphy, priv->netdev, 0, 0);
		DEBUG(DBG_CIL, "Disabling powersave \n");
		if (retval != SUCCESS)
			DEBUG(DBG_CIL, "error in disabling powersave");
	}

	/*ARP FILTER Setting*/
	DEBUG(DBG_CIL, "%s: Enabling-Disabling ARP filtering\n", __func__);
	set_arp_filter.filterMode =
		(uint32)tdata->arp_filtering_list.enable;
	if(set_arp_filter.filterMode) {
		memcpy(set_arp_filter.ipV4Addr
				, tdata->arp_filtering_list.ip_addr
				, IP_ALEN);
		DEBUG(DBG_CIL, "%s: Setting IPADDR %d.%d.%d.%d \n"
				, __func__,
				set_arp_filter.ipV4Addr[0],
				set_arp_filter.ipV4Addr[1],
				set_arp_filter.ipV4Addr[2],
				set_arp_filter.ipV4Addr[3]);
	}
	retval = CIL_Set(priv
			, UMI_DEVICE_OID_802_11_SET_ARP_IP_ADDR_FILTER
			, &set_arp_filter, sizeof(UMI_SET_ARP_IP_ADDR_FILTER));
	if (retval != SUCCESS)
		DEBUG(DBG_ERROR, "%s: error in Setting ARP filter",__func__);

	if(tdata->flags & STE_PS_CONF_MC_FILTERING) {
		if(priv->ste_mc_filter.enable == 0) {
			DEBUG(DBG_CIL, "Enabling MC filtering \n");
			priv->ste_mc_filter.enable = 1;
			if(priv->ste_mc_filter.numOfAddresses)
				retval = _UMI_mc_list(priv);
		}
	} else {
		if(priv->ste_mc_filter.enable == 1){
			DEBUG(DBG_CIL, "Disabling MC filtering \n");
			priv->ste_mc_filter.enable = 0;
			retval = _UMI_mc_list(priv);
		}
	}
	return retval;
}

static int cw1200_testmode_cmd(struct wiphy *wiphy, void *data, int len)
{
	struct nlattr *type_p = nla_find(data, len, STE_TM_MSG_ID);
	struct nlattr *data_p = nla_find(data, len, STE_TM_MSG_DATA);
	CW1200_STATUS_E retval = SUCCESS;
	struct cfg_priv *c_priv = NULL;

	if (!type_p || !data_p)
		return -EINVAL;

	DEBUG(DBG_CIL, "\t%s-type:%i\n", __func__, nla_get_u32(type_p));
	c_priv = wiphy_priv(wiphy);
	DOWN(&c_priv->driver_priv->cil_sem, NULL);

	switch (nla_get_u32(type_p)) {
	case  STE_MSG_PS_CONF_SET: {
		tunnel_set_ps(wiphy, nla_data(data_p), nla_len(data_p));
		break;
	}
	case  STE_MSG_11N_CONF_SET: {
		retval = tunnel_set_11n_conf(wiphy, nla_data(data_p),
						nla_len(data_p));
		if (retval) {
			retval = -EIO;
		}
		break;
	}
	case STE_MSG_SET_P2P:
		retval = tunnel_set_p2p_state(wiphy, nla_data(data_p),
						nla_len(data_p));
		if (retval) {
			retval = -EIO;
		}
		break;
	case STE_MSG_SET_UAPSD:
		retval = tunnel_set_uapsd(wiphy, nla_data(data_p),
						nla_len(data_p));
		if (retval) {
			retval = -EIO;
		}
		break;
	case STE_MSG_SET_P2P_POWER_SAVE:
		retval = tunnel_set_p2p_power_save(wiphy, nla_data(data_p),
							 nla_len(data_p),CIL_P2P_OPS);
		if (retval) {
			retval = -EIO;
		}
		break;
	case STE_MSG_SET_NOA:
		retval = tunnel_set_p2p_power_save(wiphy, nla_data(data_p),
							 nla_len(data_p),CIL_P2P_NOA);

		if (retval) {
			retval = -EIO;
		}
		break;
	default:
		break;
	}
	UP(&c_priv->driver_priv->cil_sem, NULL);
	return retval;
}

/**
 * static int tunnel_set_11n_conf - 11n configuration of the device
 *
 * @wiphy: Pointer to wiphy device describing the WLAN interface.
 *
 * @tdata: Pointer to buffer of type - struct ste_msg_11n_conf *
 *
 * @len: Length of buffer pointed by tdata
 */
static int tunnel_set_11n_conf(struct wiphy *wiphy,
		struct ste_msg_11n_conf *tdata , int len)
{
	struct cfg_priv *c_priv = NULL;
	CW1200_STATUS_E retval = SUCCESS;
	uint32_t amsdu_size;

	DEBUG(DBG_CIL, "tunnel_set_11n_conf()"
			"called:11n_conf FLAGS :[%d] \n", tdata->flags);

	c_priv = wiphy_priv(wiphy);

	/* Check if TID policy flag is set and pass the settings to UMAC */
	if (tdata->flags & STE_11N_CONF_USE_TID_POLICY) {
		/* Update BLOCK ACK TID policy which will be
		* set in cw1200_connect() function */
		DEBUG(DBG_CIL, "tunnel_set_11n_conf():CONF_TID_POLICY \n");
		c_priv->driver_priv->tx_block_ack = tdata->block_ack_tx_tid_policy;
		c_priv->driver_priv->rx_block_ack = tdata->block_ack_rx_tid_policy;
	}

	if (tdata->flags & STE_11N_CONF_USE_AMSDU_DEFAULT_SIZE) {

		if (tdata->amsdu_default_size == 4096)
			amsdu_size = 0;
		else
			amsdu_size = 1;

		DEBUG(DBG_CIL, "tunnel_set_11n_conf():AMSDU_DEFAULT_SIZE \n");
		retval = CIL_Set(c_priv->driver_priv,
				UMI_DEVICE_OID_802_11_MAX_RX_AMSDU_SIZE ,
				&amsdu_size, sizeof(uint32_t));
		if (retval) {
			DEBUG(DBG_CIL, "tunnel_set_11n_conf():CIL_Set"
					"for AMSDU size returned error \n");
			return ERROR;
		}
	}
	return SUCCESS;
}

static int tunnel_set_p2p_state(struct wiphy *wiphy,
		struct ste_msg_set_p2p *tdata , int len)
{
	struct cfg_priv *c_priv = NULL;
	CW1200_STATUS_E retval = SUCCESS;
	UMI_OID_USE_P2P p2p_oid;

	DEBUG(DBG_CIL, "tunnel_set_p2p_state()"
		"called: :[%d] \n", tdata->enable);

	c_priv = wiphy_priv(wiphy);

	if (tdata->enable)
		p2p_oid.useP2P = 1;
	else
		p2p_oid.useP2P = 0;

	retval = CIL_Set(c_priv->driver_priv,
		UMI_DEVICE_OID_802_11_USE_P2P,
		&p2p_oid, sizeof(UMI_OID_USE_P2P));

	if (retval)
		retval = -EIO;

	return retval;
}


static int tunnel_set_uapsd(struct wiphy *wiphy,
		struct ste_msg_set_uapsd *tdata , int len)
{
	struct cfg_priv *c_priv = NULL;
	CW1200_STATUS_E retval = SUCCESS;
	UMI_OID_802_11_SET_UAPSD uapsd_oid;

	c_priv = wiphy_priv(wiphy);
	DEBUG(DBG_CIL, "%s,Tunnel UAPSD flags [%x] \n", __func__, tdata->flags);

	if (tdata->flags != -1) {
		memset(&uapsd_oid, 0, sizeof(UMI_OID_802_11_SET_UAPSD));

		uapsd_oid.uapsdFlags = tdata->flags;
		/* Enable Pseudo UAPSD */
		if (tdata->minAutoTriggerInterval) {
			uapsd_oid.uapsdFlags |= 0x10;
			c_priv->driver_priv->minAutoTriggerInterval =
				uapsd_oid.minAutoTriggerInterval = tdata->minAutoTriggerInterval ;
			c_priv->driver_priv->maxAutoTriggerInterval =
				uapsd_oid.maxAutoTriggerInterval = tdata->maxAutoTriggerInterval;
			c_priv->driver_priv->autoTriggerStep =
				uapsd_oid.autoTriggerStep = tdata->autoTriggerStep;
		} else {
			c_priv->driver_priv->minAutoTriggerInterval = 0;
		}
		retval = CIL_Set(c_priv->driver_priv,
			UMI_DEVICE_OID_802_11_SET_UAPSD,
			&uapsd_oid, sizeof(UMI_OID_802_11_SET_UAPSD));

		if (retval) {
			DEBUG(DBG_CIL, "tunnel_set_uapsd():CIL_Set"
				"for UMI_OID_802_11_SET_UAPSD status \n");
			retval = -EIO;
		}
	}

	return retval;
}

static int tunnel_set_p2p_power_save(struct wiphy *wiphy,
		void *tdata , int len, int p2p_ps_type)
{
	struct cfg_priv *c_priv = NULL;
	CW1200_STATUS_E retval = SUCCESS;
	UMI_OID_802_11_P2P_PS_MODE p2p_ps;
	struct ste_msg_set_p2p_power_save * p2p_ops;
	struct ste_msg_set_noa * p2p_noa;

	DEBUG(DBG_CIL, "tunnel_set_p2p_power_save() called:\n");

	c_priv = wiphy_priv(wiphy);

	memset(&p2p_ps, 0, sizeof(UMI_OID_802_11_P2P_PS_MODE));

	if (CIL_P2P_OPS == p2p_ps_type) {
		/* Parameters updated by supplicant for P2P Ops*/
		p2p_ops = ( struct ste_msg_set_p2p_power_save *)tdata;
		if ( (p2p_ops->opp_ps == 1) && (p2p_ops->ctwindow) ) {
			p2p_ps.oppPsCTWindow = P2P_OPS_ENABLE;
			p2p_ps.oppPsCTWindow |= p2p_ops->ctwindow & 0x7F;
		} if (p2p_ops->opp_ps == 0) {
			p2p_ps.oppPsCTWindow = 0;
		}

		c_priv->driver_priv->ctwindow = p2p_ps.oppPsCTWindow;
		/* Set NOA attributes */
		p2p_ps.count = c_priv->driver_priv->count;
		p2p_ps.startTime = c_priv->driver_priv->startTime;
		p2p_ps.duration = c_priv->driver_priv->duration;

	} else {
		/* Params updated for NOA */
		p2p_noa = (struct ste_msg_set_noa *)tdata;
		p2p_ps.count = c_priv->driver_priv->count = p2p_noa->count;
		p2p_ps.startTime = c_priv->driver_priv->startTime = p2p_noa->start;
		p2p_ps.duration = c_priv->driver_priv->duration = p2p_noa->duration;
		p2p_ps.oppPsCTWindow = c_priv->driver_priv->ctwindow;
	}

	/* Currently keeping it one */
	p2p_ps.DtimCount = 1;
	p2p_ps.interval = p2p_ps.duration * 2;

	retval = CIL_Set(c_priv->driver_priv,
			UMI_DEVICE_OID_802_11_P2P_PS_MODE,
			&p2p_ps, sizeof(UMI_OID_802_11_P2P_PS_MODE));

	if (retval)
		retval = -EIO;

	return retval;
}

/**
 * static int cw1200_rssi_config - CFG80211 callback function for RSSIThreshold
 *
 * @wiphy: Pointer to wiphy device describing the WLAN interface.
 *
 * @rssi_thold: RSSI Threshold value.
 *
 * @rssi_hyst: RSSI hysteris value
 */

static int cw1200_rssi_config(struct wiphy *wiphy,
			struct net_device *ndev,
			signed int rssi_thold,
			uint32 rssi_hyst)
{

	UMI_RCPI_RSSI_THRESHOLD rssi_oid;
	uint32_t retval = SUCCESS;
	struct cfg_priv *c_priv = NULL;

	DEBUG(DBG_CIL, "%s Called:RSSI[%d],RSSI_HYST[%d] \n", __func__ ,
						rssi_thold , rssi_hyst);

	rssi_oid.RssiRcpiMode = RSSI_RCPI_MODE ;
	/* Enabling RSSI Threshold for both upper
 * 		and lower threshold detection */
	rssi_oid.LowerThreshold = (uint8_t)(rssi_thold);
	rssi_oid.UpperThreshold = (uint8_t)(rssi_thold + rssi_hyst);
	/* Number of samples to use for average */
	rssi_oid.RollingAverageCount = 5;

	c_priv = wdev_priv(ndev->ieee80211_ptr);

	DOWN(&c_priv->driver_priv->cil_sem, NULL);
	retval = CIL_Set(c_priv->driver_priv,
			UMI_DEVICE_OID_802_11_SET_RCPI_RSSI_THRESHOLD,
			&rssi_oid, sizeof(UMI_RCPI_RSSI_THRESHOLD));

	if (retval) {
		DEBUG(DBG_CIL, "cw1200_rssi_config():SET_RSSI value falied \n");
		retval = -EIO;
	} else {
		c_priv->driver_priv->u_rssi = (uint8_t)rssi_oid.UpperThreshold;
		c_priv->driver_priv->l_rssi = (uint8_t)rssi_oid.LowerThreshold;
	}

	UP(&c_priv->driver_priv->cil_sem, NULL);

	return retval;
}

/**
 * static int cw1200_beacon_miss_config - CFG80211 callback function
 * for Beacon miss configuration
 *
 * @wiphy: Pointer to wiphy device describing the WLAN interface.
 *
 * @beacon_thold: Beacon miss Threshold value.
 *
 */
static int cw1200_beacon_miss_config(struct wiphy *wiphy,
				struct net_device *dev,
				u32 beacon_thold)
{
	uint32_t retval = SUCCESS;
	struct cfg_priv *c_priv = NULL;
	UMI_BEACON_LOST_COUNT beacon_threshold;

	DEBUG(DBG_CIL, "%s Called:Beacon_Thold[%d] \n", __func__ ,
							beacon_thold);

	c_priv = wdev_priv(dev->ieee80211_ptr);

	DOWN(&c_priv->driver_priv->cil_sem, NULL);
	/* Store in priv.Will be set in cw1200_connect() function */
	if (beacon_thold)
	{
		c_priv->driver_priv->cqm_beacon_loss_count = beacon_thold;
		c_priv->driver_priv->cqm_beacon_loss_enabled = TRUE;
	}
	else
	{
		/* Don't notify beacon_loss but connection_loss should
		 * work fine */
		c_priv->driver_priv->cqm_beacon_loss_count = CQM_BEACON_LOSS;
		c_priv->driver_priv->cqm_beacon_loss_enabled = FALSE;
	}
	UP(&c_priv->driver_priv->cil_sem, NULL);

	return retval;
}

/**
 * static int cw1200_tx_fail_config - CFG80211 callback function
 * for TX fails configuration
 *
 * @wiphy: Pointer to wiphy device describing the WLAN interface.
 *
 * @beacon_thold: TX Fails Threshold value.
 *
 */
static int cw1200_tx_fail_config(struct wiphy *wiphy,
				struct net_device *dev,
				u32 tx_thold)
{
	uint32_t retval = SUCCESS;
	struct cfg_priv *c_priv = NULL;
	UMI_TX_FAILURE_THRESHOLD_COUNT tx_threshold;

	DEBUG(DBG_CIL, "%s Called:TX_Thold[%d] \n", __func__ ,
							tx_thold);

	c_priv = wdev_priv(dev->ieee80211_ptr);
	tx_threshold.TransmitFailureThresholdCount = tx_thold;

	DOWN(&c_priv->driver_priv->cil_sem, NULL);
	retval = CIL_Set(c_priv->driver_priv,
			UMI_DEVICE_OID_802_11_TX_FAILURE_THRESHOLD_COUNT,
			&tx_threshold, sizeof(UMI_TX_FAILURE_THRESHOLD_COUNT));

	if (retval) {
		DEBUG(DBG_CIL, "cw1200_tx_fail_config():"
				"SET_TX_HOLD value falied \n");
		retval = -EIO;
	}
	UP(&c_priv->driver_priv->cil_sem, NULL);

	return retval;
}

void cw1200_handle_delayed_link_loss(struct CW1200_priv *priv)
{
	if (priv->delayed_link_loss) {
		priv->delayed_link_loss = FALSE;
		/* Restart beacon loss timer and requeue BSS loss work. */
		DEBUG(DBG_CIL, "[CQM] Requeue BSS loss in %d beacons.\n",
				priv->cqm_beacon_loss_count);
		cancel_delayed_work_sync(&priv->bss_loss_work);
		queue_delayed_work(priv->bss_loss_WQ, &priv->bss_loss_work,
			msecs_to_jiffies(priv->cqm_beacon_loss_count * priv->beacon_interval));
	}
}


void cw1200_bss_loss_work(struct work_struct *work)
{
	struct CW1200_priv *priv =
		container_of(work, struct CW1200_priv, bss_loss_work);
	int timeout; /* in beacons */

	DEBUG(DBG_CIL, "cw1200_bss_loss_work called \n");

	timeout = priv->cqm_link_loss_count -
		priv->cqm_beacon_loss_count;

	if (priv->cqm_beacon_loss_enabled) {
		DEBUG(DBG_CIL, "[CQM] Beacon loss.\n");
		if (timeout <= 0)
			timeout = 0;
		cfg80211_cqm_beacon_miss_notify(priv->netdev, GFP_KERNEL);
	} else {
		timeout = 0;
	}
	cancel_delayed_work_sync(&priv->connection_loss_work);
	queue_delayed_work(priv->bss_loss_WQ,
		&priv->connection_loss_work,
		msecs_to_jiffies(timeout * priv->beacon_interval)) ;
}

void cw1200_connection_loss_work(struct work_struct *work)
{
	struct CW1200_priv *priv =
		container_of(work, struct CW1200_priv, connection_loss_work);

	DEBUG(DBG_CIL, "[CQM] Reporting connection loss.\n");
	cfg80211_disconnected(priv->netdev,
		WLAN_REASON_DISASSOC_STA_HAS_LEFT, NULL, NULL, 0, GFP_KERNEL);
	if (priv->wait_event == UMI_EVT_CONNECTED) {
		priv->wait_event = 0;
		UP(&priv->cil_sem, NULL);
	}
}

void cw1200_check_connect(struct work_struct *work)
{
	struct CW1200_priv *priv =
		container_of(work, struct CW1200_priv, connect_result);

	DEBUG(DBG_CIL, "CIL:Timeout waiting for connect result\n");

	if (priv->connection_status == CW1200_CONNECTING) {
		priv->connection_status = CW1200_DISCONNECTED;
		cfg80211_connect_result(priv->netdev,
			NULL, NULL, 0, NULL, 0,
			WLAN_STATUS_UNSPECIFIED_FAILURE, GFP_KERNEL);
	}

	cancel_delayed_work_sync(&priv->bss_loss_work);
	cancel_delayed_work_sync(&priv->connection_loss_work);
	priv->delayed_link_loss = FALSE;

	cfg80211_disconnected(priv->netdev,
				0, NULL, NULL, 0, GFP_KERNEL);
	if (priv->wait_event == UMI_EVT_CONNECTED) {
		priv->wait_event = 0;
		UP(&priv->cil_sem, "connection timeout");
	}
}


/**
 * static int cw1200_roc_timeout - This function is called on
 * REMAIN on CHANNEL timeout.
 *
 * @work: Pointer to work_struct
 */
void cw1200_roc_timeout(struct work_struct *work)
{
	struct CW1200_priv *priv =
		container_of(work, struct CW1200_priv, roc_work);

	uint32_t retval = SUCCESS;

	DEBUG(DBG_CIL, "CIL:P2P:REMAIN on CHANNEL Timeout \n");
	DOWN(&priv->cil_sem, NULL);

	/* TBD : To check if Action Frame exchange on ON */
	retval = CIL_Set(priv,
			UMI_DEVICE_OID_802_11_RESTORE_CHANNEL,
			0,0);
	if (retval) {
		DEBUG(DBG_CIL, "%s:P2P:RESTORE_CHANNEL falied \n", __func__);
	}

	cfg80211_remain_on_channel_expired(priv->netdev, priv->rem_on_chan_cookie,
				priv->rem_on_chan,
				priv->rem_on_channel_type,
				GFP_KERNEL);
	/* Restore */
	priv->rem_on_chan = NULL;
	priv->rem_on_chan_duration = 0;
	priv->rem_on_chan_cookie = 0;

	UP(&priv->cil_sem, NULL);
}


static int cw1200_set_pmksa(struct wiphy *wiphy, struct net_device *netdev,
				struct cfg80211_pmksa *pmksa)
{
	uint32_t retval = SUCCESS;
	struct cfg_priv *c_priv = NULL;
	struct CW1200_priv *priv = NULL;
	uint32_t bss_count=0, i = 0;
	int result = SUCCESS;

	DEBUG(DBG_CIL, "%s ,called for BSSID: %pM \n", __func__, pmksa->bssid);

	c_priv = wdev_priv(netdev->ieee80211_ptr);
	priv = c_priv->driver_priv;

	DOWN(&priv->cil_sem, NULL);
	/* Check if UMAC can accept any more */
	bss_count = priv->pmk_list.bssidInfoCount;
	if (bss_count >= UMI_MAX_BSSID_INFO_ENTRIES) {
		DEBUG(DBG_CIL, "CIL: Exceeded max entries allowed by UMAC \n");
		result = -EIO;
		goto fail;
	}
	for (i=0; i < bss_count; i++) {
		if (memcmp(pmksa->bssid, &priv->pmk_list.bssidInfo[i].bssid , ETH_ALEN) == 0)
			break;
	}

	memcpy(&priv->pmk_list.bssidInfo[i].bssid,
		pmksa->bssid, ETH_ALEN);

	memcpy(&priv->pmk_list.bssidInfo[i].pmkidBkid,
		pmksa->pmkid, UMI_PMKID_BKID_SIZE);

	if ( i >= bss_count)
		priv->pmk_list.bssidInfoCount++;

	retval = CIL_Set(priv,
			UMI_DEVICE_OID_802_11_SET_PMKID_BKID,
			&priv->pmk_list, sizeof(UMI_BSSID_PMKID_BKID));

	if (retval) {
		DEBUG(DBG_CIL, "%s:SET_PMKID value falied \n", __func__);
		priv->pmk_list.bssidInfoCount--;
		result = -EIO;
		goto fail;
	}

fail:
	UP(&priv->cil_sem, NULL);
	return retval;

}

static int cw1200_del_pmksa(struct wiphy *wiphy, struct net_device *netdev,
				struct cfg80211_pmksa *pmksa)
{
	uint32_t retval = SUCCESS;
	struct cfg_priv *c_priv = NULL;
	struct CW1200_priv *priv = NULL;
	uint32_t i=0,j=0;
	uint32_t bss_count=0;
	int result = SUCCESS;

	DEBUG(DBG_CIL, "%s ,called for BSSID: %pM \n", __func__, pmksa->bssid);

	c_priv = wdev_priv(netdev->ieee80211_ptr);
	priv = c_priv->driver_priv;

	DOWN(&priv->cil_sem, NULL);
	bss_count = priv->pmk_list.bssidInfoCount;
	if (bss_count == 0) {
		DEBUG(DBG_CIL, "%s ,Invalid value of BSSCount \n", __func__);
		result = -EIO;
		goto fail;
	}

	for (i=0; i < bss_count; i++) {
		if (memcmp(pmksa->bssid, &priv->pmk_list.bssidInfo[i].bssid , ETH_ALEN) == 0)
		break;
	}

	for (j=i; j < (bss_count -1); j++) {
		memcpy(&priv->pmk_list.bssidInfo[j], &priv->pmk_list.bssidInfo[j+1],
			sizeof(UMI_BSSID_INFO));
	}

	priv->pmk_list.bssidInfoCount--;
	memset(&priv->pmk_list.bssidInfo[bss_count], 0,  sizeof(UMI_BSSID_INFO));

fail:
	UP(&priv->cil_sem, NULL);
	return result;
}


struct net_device * cw1200_add_virtual_intf(struct wiphy *wiphy, char *name,
				enum nl80211_iftype type, u32 *flags,
				struct vif_params *params)
{
	struct net_device *netdev = NULL;
	struct cfg_priv *c_priv = NULL;
	struct cfg_priv *c_new_priv = NULL;
	struct wireless_dev *wdev = NULL;

	DEBUG(DBG_CIL, "%s,IF-NAME [%s] \n",__func__, name);
	c_priv = wiphy_priv(wiphy);

	DOWN(&c_priv->driver_priv->cil_sem, NULL);
	netdev = alloc_etherdev(sizeof(struct cfg_priv));
	if (!netdev)
		goto fail;

	c_new_priv = netdev_priv(netdev);
	c_new_priv->driver_priv = c_priv->driver_priv;

	strcpy(netdev->name, name);
	memcpy(netdev->dev_addr, c_priv->driver_priv->netdev->dev_addr, ETH_ALEN);
	SET_NETDEV_DEV(netdev, wiphy_dev(c_priv->driver_priv->netdev->ieee80211_ptr->wiphy));

	wdev = kzalloc(sizeof(struct wireless_dev), GFP_KERNEL);
	if (!wdev)
	{
		free_netdev(netdev);
		netdev = NULL;
		goto fail;
	}
	wdev->iftype = NL80211_IFTYPE_AP;
	wdev->netdev = netdev;
	wdev->wiphy = c_priv->driver_priv->netdev->ieee80211_ptr->wiphy;
	netdev->ieee80211_ptr = wdev;
	netdev->netdev_ops = &ap_netdev_ops;
	c_priv->driver_priv->mon_netdev = netdev;
	register_netdevice(netdev);

fail:
	UP(&c_priv->driver_priv->cil_sem, NULL);
	return netdev;
}

int cw1200_del_virtual_intf(struct wiphy *wiphy, struct net_device *dev)
{
	struct CW1200_priv *priv = NULL;
	struct cfg_priv *c_priv = NULL;

	DEBUG(DBG_CIL, "%s, Called \n", __func__);

	c_priv = wiphy_priv(wiphy);
	priv = c_priv->driver_priv;

	DOWN(&priv->cil_sem, NULL);
	unregister_netdevice(dev);
	//free_netdev(dev);
	priv->mon_netdev = NULL;
	UP(&priv->cil_sem, NULL);

	return SUCCESS;
}


int cw1200_set_channel(struct wiphy *wiphy, struct net_device *dev,
			struct ieee80211_channel *chan,
			enum nl80211_channel_type channel_type)
{
	struct cfg_priv *c_priv = NULL;
	struct CW1200_priv *priv = NULL;

	DEBUG(DBG_CIL, "%s, Called.Channel No:%d \n", __func__, chan->hw_value);
	c_priv = wiphy_priv(wiphy);
	priv = c_priv->driver_priv;

	DOWN(&priv->cil_sem, NULL);
	priv->ap_channel = chan->hw_value;
	UP(&priv->cil_sem, NULL);

	return SUCCESS;
}


int cw1200_add_beacon(struct wiphy *wiphy, struct net_device *dev,
			      struct beacon_parameters *info)
{
	DEBUG(DBG_CIL, "%s, Called \n", __func__);
	/* TBD : Store params in priv */
	return SUCCESS;
}


int cw1200_set_beacon(struct wiphy *wiphy, struct net_device *dev,
			      struct beacon_parameters *info)
{
	struct cfg_priv *c_priv = NULL;
	struct CW1200_priv *priv = NULL;
	UMI_OID_802_11_START_AP ap;
	int32_t retval = SUCCESS;
	uint8_t *pos = NULL;
	uint32_t ssid_len = 0;
	struct ieee80211_mgmt *head = (struct ieee80211_mgmt *)info->head;
	u8 *tail = info->tail;
	int tail_len = info->tail_len;
	int i;
	u8 *p2p_ie = NULL;
	u8 p2p_ie_len = 0;
	u8 *wps_ie = NULL;
	u8 wps_ie_len = 0;
	uint32_t encr = 0x0;
	uint32_t auth_mode = UMI_OPEN_SYSTEM;
	UMI_UPDATE_VENDOR_IE vendor_ie;
	bool p2p_active = false;
	struct wmm_parameter_element *wmm_param = NULL;
	UMI_OID_802_11_CONFIG_WMM_PARAMS wmm_oid;
	int result = SUCCESS;

	DEBUG(DBG_CIL, "%s, Called \n", __func__);
	c_priv = wiphy_priv(wiphy);
	priv = c_priv->driver_priv;

	if ( NULL == head) {
		DEBUG(DBG_CIL, "%s,Invalid Param \n", __func__);
		return -EINVAL;
	}

	pos = &head->u.beacon.variable[0];

	if (*pos++ == WLAN_EID_SSID) {
		ssid_len = *pos++;
		DEBUG(DBG_CIL , "SSID IE found,SSID length [%d] \n", ssid_len);
	} else {
		DEBUG(DBG_CIL, "SSID IE not found \n");
		return -EINVAL;
	}

	if(tail)
	{
		for(i=0; i<tail_len; i++)
		{
			/* Search for P2P IE - OUI_WFA 0x506f9a
			P2P_OUI_TYPE 9 */
			if ( (tail[i] == 0xdd) && (tail[i+2] == 0x50) && (tail[i+3] == 0x6f)
				&& (tail[i+4] == 0x9a) && (tail[i+5] == 9) ) {
				DEBUG(DBG_CIL, "%s,P2P IE found \n",__func__);
				p2p_ie = &tail[i];
				p2p_ie_len  = tail[i+1] + 2;
				p2p_active = true;
			} else
			/* Search for WPS IE */
			if ((tail[i] == 0xdd) &&
				(GET_BE32(&tail[i+2]) == WPS_IE_VENDOR_TYPE) ) {
				DEBUG(DBG_CIL, "%s,WPS IE found \n",__func__);
				wps_ie = &tail[i];
				wps_ie_len = tail[i+1] + 2;
			} else
			if ((tail[i] == 0xdd) &&
				(GET_BE32(&tail[i+2]) == WMM_IE_VENDOR_TYPE) ) {

				DEBUG(DBG_CIL, "%s,WMM IE found \n", __func__);
				wmm_param = &tail[i+2];
				wmm_oid.qosInfo = wmm_param->qos_info;
				wmm_oid.reserved1 = 0;
				memcpy(&wmm_oid.ac_BestEffort, &wmm_param->ac[0], sizeof(WMM_AC_PARAM));
				memcpy(&wmm_oid.ac_BackGround, &wmm_param->ac[1], sizeof(WMM_AC_PARAM));
				memcpy(&wmm_oid.ac_Video, &wmm_param->ac[2], sizeof(WMM_AC_PARAM));
				memcpy(&wmm_oid.ac_Voice, &wmm_param->ac[3], sizeof(WMM_AC_PARAM));
			} else
			// WPA - skip WPS
			if(tail[i] == 0xdd && tail[i+5] == 0x01)
			{
				DEBUG(DBG_CIL, "WPA\n");
				encr |= 0x02;
				auth_mode = UMI_WPA_PSK;
			} else
			// WPA/RSN
			if(tail[i] == 0x30)
			{
				DEBUG(DBG_CIL, "WPA2/RSN\n");
				encr |= 0x04;
				auth_mode = UMI_WPA2_PSK;
			} else
			{
				DEBUG(DBG_CIL, "skiping EID: 0x%02X, len: %d\n",
				      tail[i], tail[i+1]);
			}
			i = i + tail[i+1] + 1;
		}
	}

	if (p2p_active)
		ap.mode = 1;
	else
		ap.mode = 0;

	ap.band = 0;
	ap.networkType = 2;
	ap.channelNum = priv->ap_channel;
	ap.CTWindow = 0;
	ap.beaconInterval = info->interval;
	ap.DTIMperiod = info->dtim_period;
	ap.preambleType = 1;
	ap.probeDelay = 0;
	ap.ssidLength = ssid_len;
	/* Hard coded SSID */
	memcpy(&ap.ssid[0],pos,ssid_len);

	if (p2p_active)
		ap.basicRates = 0x540;
	else
		ap.basicRates = 0xF;

	memcpy(&ap.bssid ,priv->netdev->dev_addr, ETH_ALEN);

	DOWN(&priv->cil_sem, NULL);
	retval = CIL_Set(c_priv->driver_priv,
			UMI_DEVICE_OID_802_11_ENCRYPTION_STATUS,
			&encr, sizeof(uint32_t));
	if (retval) {
		DEBUG(DBG_CIL, "cw1200_set_beacon:CIL_Set"
				"for ENCRYPTION status \n");
		result = -EIO;
		goto fail;
	}

	retval = CIL_Set(c_priv->driver_priv,
			UMI_DEVICE_OID_802_11_AUTHENTICATION_MODE,
			&auth_mode, sizeof(uint8_t));

	if (retval) {
		DEBUG(DBG_CIL, "cw1200_set_beacon():CIL_Set"
				"for AUTH MODE returned error \n");
		result = -EIO;
		goto fail;
	}

	if (p2p_ie_len + wps_ie_len) {
		/* Set the VENDOR P2P_IE and WPS_IE to UMAC */
		memset(&vendor_ie, 0, sizeof(UMI_UPDATE_VENDOR_IE));
		vendor_ie.pBeaconIE = kmalloc(p2p_ie_len + wps_ie_len, GFP_KERNEL);
		if (!vendor_ie.pBeaconIE) {
			DEBUG(DBG_CIL, "cw1200_set_beacon():Memory"
				"allocation failure\n");
			result = -ENOMEM;
			goto fail;
		}

		if (p2p_ie_len) {
			memcpy(vendor_ie.pBeaconIE, p2p_ie, p2p_ie_len);
			if (wps_ie_len) {
				memcpy(&vendor_ie.pBeaconIE[0] + p2p_ie_len , wps_ie, wps_ie_len);
				vendor_ie.beaconIElength = p2p_ie_len + wps_ie_len;
			} else
				vendor_ie.beaconIElength = p2p_ie_len;
		} else {
			memcpy(&vendor_ie.pBeaconIE[0], wps_ie, wps_ie_len);
			vendor_ie.beaconIElength = wps_ie_len;
		}

		/* Update Vendor IE for AssocResp and ProbeResp */
		vendor_ie.pAssocRespIE = vendor_ie.pProbeRespIE =
						vendor_ie.pBeaconIE;
		vendor_ie.assocRespIElength = vendor_ie.probeRespIElength =
				vendor_ie.beaconIElength;
	
		retval = CIL_Set(priv,
				UMI_DEVICE_OID_802_11_UPDATE_VENDOR_IE,
				&vendor_ie, sizeof(UMI_UPDATE_VENDOR_IE));

		kfree(vendor_ie.pBeaconIE);

		if (retval) {
		DEBUG(DBG_CIL, "%s:UMI_DEVICE_OID_802_11_UPDATE_VENDOR_IE"
				"set falied \n", __func__);
		result = -EIO;
		goto fail;
		}
	}

	/* Set the WMM information */
	if (wmm_param) {
		retval = CIL_Set(priv,
				UMI_DEVICE_OID_802_11_CONFIGURE_WMM_PARAMS,
				&wmm_oid, sizeof(UMI_OID_802_11_CONFIG_WMM_PARAMS));

		if (retval) {
			DEBUG(DBG_CIL, "%s:UMI_DEVICE_OID_802_11_CONFIGURE_WMM_PARAMS"
					"set falied \n", __func__);
			return -EIO;
		}
	}
	/* Check if the call was for updating IEs or 
	starting AP */
	if (priv->soft_ap_started == false) {
		retval = CIL_Set(priv,
				UMI_DEVICE_OID_802_11_START_AP,
				&ap, sizeof(UMI_OID_802_11_START_AP));

		if (retval) {
			DEBUG(DBG_CIL, "%s:UMI_DEVICE_OID_802_11_START_AP"
					"set falied \n", __func__);
			result = -EIO;
			goto fail;
		} else
			priv->soft_ap_started = true;
	}

fail:
	UP(&priv->cil_sem, NULL);
	return result;
}


int cw1200_deauth(struct wiphy *wiphy, struct net_device *dev,
			  struct cfg80211_deauth_request *req,
			  void *cookie)
{

	DEBUG(DBG_CIL, "%s, Called \n", __func__);

	return SUCCESS;
}

int cw1200_del_beacon(struct wiphy *wiphy, struct net_device *dev)
{
	struct cfg_priv *c_priv = NULL;
	struct CW1200_priv *priv = NULL;
	int32_t retval = SUCCESS;
	int result = SUCCESS;

	DEBUG(DBG_CIL, "%s, Called \n", __func__);

	c_priv = wiphy_priv(wiphy);
	priv = c_priv->driver_priv;

	DOWN(&priv->cil_sem, NULL);
	retval = CIL_Set(priv,
			UMI_DEVICE_OID_802_11_STOP_AP,
			0,0);

	if (retval) {
		DEBUG(DBG_CIL, "%s:UMI_DEVICE_OID_802_11_STOP_AP set falied \n",
				 __func__);
		result = -EIO;
	} else
		priv->soft_ap_started = false;

	UP(&priv->cil_sem, NULL);

	return result;
}

/**
 * cw1200_get_station -This function fills the current unicast bitrate (in Mbits/s) to the station.
 *
 * @wiphy: Pointer to wiphy device describing the WLAN interface.
 * @dev: Network Device describing this driver.
 * @sinfo: Station information filled by driver
 */
int cw1200_get_station(struct wiphy *wiphy,
				struct net_device *dev,
				u8 *mac,
				struct station_info *sinfo)
{
	int rate;
	int *rate_index = NULL;

	struct cfg_priv *c_priv = NULL;
	struct CW1200_priv *priv = NULL;
	c_priv = wiphy_priv(wiphy);
	priv = c_priv->driver_priv;

	DEBUG(DBG_CIL, "%s Called \n", __func__);

	DOWN(&priv->cil_sem, NULL);
	rate_index =(int *) CIL_Get(priv, UMI_DEVICE_OID_802_11_GET_LINK_SPEED,
				sizeof(UMI_DEVICE_OID_802_11_GET_LINK_SPEED));

	if(rate_index != NULL)
	{
		if((*rate_index)>=0 && (*rate_index)<22)
		{
			sinfo->filled |= STATION_INFO_TX_BITRATE;
			sinfo->txrate.legacy= link_speed[*rate_index]*10;
		}
	}
	kfree(rate_index);
	UP(&priv->cil_sem, NULL);

	return 0;
}

int cw1200_remain_on_channel(struct wiphy *wiphy,
				     struct net_device *dev,
				     struct ieee80211_channel *chan,
				     enum nl80211_channel_type channel_type,
				     unsigned int duration,
				     uint64_t *cookie)
{
	struct cfg_priv *c_priv = NULL;
	struct CW1200_priv *priv = NULL;
	int32_t retval = SUCCESS;
	uint32_t channel = chan->hw_value;

	DEBUG(DBG_CIL, "%s, Called:CHANNEL:%d,DURATION:%d \n", __func__,
				channel, duration);

	c_priv = wiphy_priv(wiphy);
	priv = c_priv->driver_priv;

	DOWN(&priv->cil_sem, NULL);

	/* Check if REMAIN on CHANNEL is active */
	if (NULL == priv->rem_on_chan) {

		*cookie = random32() | 1;
		priv->rem_on_chan = chan;
		priv->rem_on_channel_type = channel_type;
		priv->rem_on_chan_duration = duration;
		priv->rem_on_chan_cookie = *cookie;

		retval = CIL_Set(priv,
				UMI_DEVICE_OID_802_11_CHANGE_CHANNEL,
				&channel, sizeof(uint32_t));

		if (retval) {
			DEBUG(DBG_CIL, "%s:UMI_DEVICE_OID_802_11_CHANGE_CHANNEL set"
					"failed \n", __func__);
			priv->rem_on_chan = NULL;
			priv->rem_on_channel_type = NULL;
			priv->rem_on_chan_duration = 0;
			retval = -EIO;
		}
	} else
		retval = -EBUSY;

	UP(&priv->cil_sem, NULL);
	return retval;
}

int cw1200_cancel_remain_on_channel(struct wiphy *wiphy,
					struct net_device *dev,
					uint64_t cookie)
{
	uint32_t retval=SUCCESS;
	struct cfg_priv *c_priv = NULL;
	struct CW1200_priv *priv = NULL;

	DEBUG(DBG_CIL, "%s, Called \n", __func__);

	c_priv = wiphy_priv(wiphy);
	priv = c_priv->driver_priv;

	cancel_delayed_work_sync(&priv->roc_work);

	retval = CIL_Set(priv,
			UMI_DEVICE_OID_802_11_RESTORE_CHANNEL,
			0,0);
	if (retval) {
		DEBUG(DBG_CIL, "%s:P2P:RESTORE_CHANNEL falied \n", __func__);
	}

	/* Restore */
	priv->rem_on_chan = NULL;
	priv->rem_on_chan_duration = 0;

	return SUCCESS;
}

int cw1200_mgmt_tx(struct wiphy *wiphy, struct net_device *dev,
                          struct ieee80211_channel *chan, bool offchan,
                          enum nl80211_channel_type channel_type,
                          bool channel_type_valid, unsigned int wait,
                          const u8 *buf, size_t len, u64 *cookie)
{
	struct cfg_priv *c_priv = NULL;
	struct CW1200_priv *priv = NULL;
	UMI_STATUS_CODE umi_status = UMI_STATUS_SUCCESS;
	uint32_t retval = SUCCESS;
	uint32_t channel = chan->hw_value;

	DEBUG(DBG_CIL, "%s, Called \n", __func__);

	c_priv = wiphy_priv(wiphy);
	priv = c_priv->driver_priv;

	DOWN(&priv->cil_sem, NULL);

	if (offchan) {
		if (NULL ==priv->rem_on_chan) {
			DEBUG(DBG_CIL, "%s, Called:OffChannel TX \n", __func__);
			*cookie = random32() | 1;
			priv->rem_on_chan_cookie = *cookie;
			priv->rem_on_chan = chan;
			priv->rem_on_channel_type = channel_type;
			priv->rem_on_chan_duration = wait;
			retval = CIL_Set(priv,
				UMI_DEVICE_OID_802_11_CHANGE_CHANNEL,
				&channel, sizeof(uint32_t));
			if (retval) {
				DEBUG(DBG_CIL, "%s:UMI_DEVICE_OID_802_11_CHANGE_CHANNEL"
						"set failed \n",__func__);
				retval = -EIO;
			}
		}
	}
	/* Pass frame to UMAC */
	umi_status = UMI_MgmtTxFrame(priv->umac_handle,
                              0, len, buf);

	if (UMI_STATUS_SUCCESS != umi_status) {
		DEBUG(DBG_CIL, "%s,UMI_MgmtTxFrame() returned error[%d] \n",
				__func__, umi_status);
		retval = -EIO;
	} else {
		*cookie = COOKIE_MGMT_TX;
		priv->mgmt_tx_cookie = *cookie;
	}
	UP(&priv->cil_sem, NULL);

	return retval;
}


int cw1200_mgmt_tx_cancel_wait(struct wiphy *wiphy,
			       struct net_device *dev,
			       u64 cookie)
{
	int32_t retval = SUCCESS;

	DEBUG(DBG_CIL, "%s, Called \n", __func__);

	retval = cw1200_cancel_remain_on_channel(wiphy,
						dev,
						cookie);

	return retval;
}


void cw1200_mgmt_frame_register(struct wiphy *wiphy,
				struct net_device *dev,
				uint16_t frame_type, bool reg)
{
	DEBUG(DBG_CIL, "%s, Called:frame_type:%x \n", __func__, frame_type);
}


void UMI_CB_TxMgmtFrmComplete(UL_HANDLE ulHandle,
			UMI_TX_MGMT_FRAME_COMPLETED *pTxMgmtData)
{
	struct CW1200_priv *priv = (struct CW1200_priv *)ulHandle;

	DEBUG(DBG_CIL, "%s, Called\n", __func__);

	if (UMI_STATUS_SUCCESS == pTxMgmtData->Status) {
		cfg80211_mgmt_tx_status(priv->netdev, priv->mgmt_tx_cookie,
			      	pTxMgmtData->pFrame, pTxMgmtData->FrameLength,
				true,GFP_ATOMIC);
	} else {
		DEBUG(DBG_CIL,"%s,P2P,Unable to transmit MGMT Frame:ERROR:"
				"%d \n",__func__, pTxMgmtData->Status);
		cfg80211_mgmt_tx_status(priv->netdev, priv->mgmt_tx_cookie,
			      	pTxMgmtData->pFrame, pTxMgmtData->FrameLength,
				false,GFP_ATOMIC);

	}
	priv->mgmt_tx_cookie = 0;
}
