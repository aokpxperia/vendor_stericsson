/* ===========================================================================
 *
 *Linux Driver for CW1200 series
 *
 *
 *Copyright (c) ST-Ericsson SA, 2010
 *
 *This program is free software; you can redistribute it and/or modify it
 *under the terms of the GNU General Public License version 2 as published
 *by the Free Software Foundation.
 *
 * ==========================================================================*/
/**
 *addtogroup Linux Driver - EIL (Ethernet Interface)Layer
 *brief
 *
 */
/**
 *file eil.c
 *- <b>PROJECT</b>			: CW1200_LINUX_DRIVER
 *- <b>FILE</b>				: eil.c
 *brief
 *This module interfaces with the Linux Kernel 802.3/Ethernet Layer.
 *ingroup EIL
 *date 25/02/2010
 */


/****************************************************************************
*					INCLUDE FILES
*****************************************************************************/

#include "eil.h"
#include "cil.h"
#include "sbus.h"
#include <linux/netdevice.h>
#include <linux/etherdevice.h>
#include <linux/vmalloc.h>
#include <linux/random.h>
#include <linux/module.h>

#include <net/ip.h>

MODULE_LICENSE("GPL");

/*****************************************************************************
*					LOCAL FUNCTION PROTOTYPES
******************************************************************************/

static int EIL_Stop(struct net_device *dev);

static int EIL_Open(struct net_device *dev);

static void EIL_Tx_Timeout(struct net_device *dev);

static void EIL_set_mc_list(struct net_device *dev);

static struct net_device_stats *EIL_Statistics(struct net_device *dev);

static void UMAC_bh(struct work_struct *work);
void  UMI_CB_Tx_Complete(UL_HANDLE UpperHandle,
		UMI_STATUS_CODE Status,
		UMI_TX_DATA *pTxData);

UMI_STATUS_CODE UMI_CB_Data_Received(UL_HANDLE	UpperHandle,
		UMI_STATUS_CODE	Status,
		uint16_t Length,
		void *pFrame,
		void *pDriverInfo,
		void *pFrmStart,
		uint16_t flags);
void UMI_CB_Schedule(UL_HANDLE	UpperHandle);

void UMI_CB_ConfigReqComplete(UL_HANDLE UpperHandle,
			UMI_CONFIG_CNF	*pConfigCnf);


static CW1200_STATUS_E Read_File(struct cw1200_firmware *frmwr,
				char *file_path);

/*
 *  * iwm AC to queue mapping
 *   *
 *    * AC_VO -> queue 3
 *     * AC_VI -> queue 2
 *      * AC_BE -> queue 1
 *       * AC_BK -> queue 0
 *       */
static const u16 cw1200_1d_to_queue[8] = { 1, 0, 0, 1, 2, 2, 3, 3 };

static u16 EIL_select_queue(struct net_device *dev, struct sk_buff *skb)
{
	skb->priority = cfg80211_classify8021d(skb);

	return cw1200_1d_to_queue[skb->priority];
}

/*******************************************************************************
*				GLOBAL VARS
******************************************************************************/

#if LINUX_VERSION_CODE >= KERNEL_VERSION(2, 6, 30)
static const struct net_device_ops cw1200_netdev_ops = {
	.ndo_open		 = EIL_Open,
	.ndo_stop		 = EIL_Stop,
	.ndo_get_stats		 = EIL_Statistics,
	.ndo_start_xmit		 = EIL_Transmit,
	.ndo_tx_timeout		 = EIL_Tx_Timeout,
	.ndo_set_multicast_list =  EIL_set_mc_list,
	.ndo_select_queue	= EIL_select_queue,
};
#endif

static unsigned int arr_count;
static unsigned char mac_addr[MACADDR_LEN];
module_param_array(mac_addr, byte , &arr_count, S_IRUGO);
MODULE_PARM_DESC(mac_addr, "MAC address for CW1200 ");

#ifndef NO_QUIESCENT_MODE
static unsigned int mode = WLAN_QUIESCENT_MODE;
#else
static unsigned int mode = WLAN_ACTIVE_MODE;
#endif
module_param(mode, uint, S_IRUGO);
MODULE_PARM_DESC(mode, "CW1200 operational power mode: "
		 "0 - ACTIVE, 1 - DOZE, 2 - QUIESCENT");

/*******************************************************************************
*				FUNCTION DEFINITIONS
*******************************************************************************/



/*! \fn  static void UMAC_BH (struct work_struct *work)
	brief This function implements the UMAC  bottom half handler
	param work 	- pointer to the Work Queue work_struct
	return 		- None
*/
static void UMAC_bh(struct work_struct *work)
{
	struct CW1200_priv *priv ;
	priv  = container_of(work, struct CW1200_priv, umac_work);

	/*Service UMAC */
	UMI_Worker(priv->umac_handle);
}

/*! \fn  static void set_mc_list(struct work_struct *work)
	brief This function sets the multicast filter if event is received
	param work 	- pointer to the Work Queue work_struct
	return 		- None
*/

static void set_mc_list(struct work_struct *work)
{
	struct CW1200_priv *priv ;
	priv  = container_of(work, struct CW1200_priv, mc_work);
	struct net_device *dev = priv->netdev;
	UMI_OID_802_11_MULTICAST_ADDR_FILTER *lmcfilter = &(priv->ste_mc_filter);
	int naddr = 0,i = 0;
#if LINUX_VERSION_CODE < KERNEL_VERSION(2,6,35)
	struct dev_mc_list *macaddr;
#else
	struct netdev_hw_addr *macaddr;
#endif

	netif_addr_lock_bh(dev);
	naddr = netdev_mc_count(dev);
	DEBUG(DBG_EIL, "%s: no. of mc addrs %d \n", __func__, naddr);
	if(naddr > MCADDR_LIST_MAXSIZE)
		lmcfilter->numOfAddresses= MCADDR_LIST_MAXSIZE;
	else
		lmcfilter->numOfAddresses= naddr;
	netdev_for_each_mc_addr(macaddr, dev) {
#if LINUX_VERSION_CODE < KERNEL_VERSION(2,6,35)
		memcpy(&(lmcfilter->AddressList[i]), macaddr->dmi_addr, ETH_ALEN);
#else
		memcpy(&(lmcfilter->AddressList[i]), macaddr->addr, ETH_ALEN);
#endif
		DEBUG(DBG_EIL, "adding mc address %pM \n"
			, lmcfilter->AddressList[i]);
		i = (i+1)%MCADDR_LIST_MAXSIZE;
		naddr--;
	}
	netif_addr_unlock_bh(dev);
	if (unlikely(naddr)) {
		/*wrong value of list size provided*/
		DEBUG(DBG_ERROR, "%s: wrong MC addr list size\n", __func__);
		lmcfilter->numOfAddresses = 0; /*The current list is unusable*/
		return ;
	}

	/* If Multicast address filtering is enabled and addrupdate is true
	 * Then set MC filter OID
	 */
	if(lmcfilter->enable) {
		i = UMI_mc_list(priv);
	}
	return ;
}



/*! \fn  int32_t EIL_Init (CW1200_bus_device_t *func)
	brief This function initialises the EIL layer.
	It registers the driver with the Linux 802.3 stack.
	param func -> pointer to the sdio_func passed by
	the Linux SDIO stack in probe function
	return -> Status code. Pass or failure code.
*/
CW1200_STATUS_E EIL_Init(CW1200_bus_device_t *func)
{
	struct net_device *netdev = NULL;
	struct CW1200_priv *priv = NULL;
	CW1200_STATUS_E retval = SUCCESS;
	struct cw1200_firmware firmware;
	UMI_START_T  umi_start = {0};
	struct wireless_dev *wdev = NULL;
	UMI_CREATE_IN umi_in   ; /*Input paramter to UMI_Create */
	UMI_CREATE_OUT umi_out ; /*Output paramter from UMI_Create */
	UMI_STATUS_CODE umac_retval = UMI_STATUS_SUCCESS;
	char *firmware_path = NULL;

	netdev = alloc_etherdev_mq(sizeof(struct CW1200_priv), 4);
	priv = netdev_priv(netdev);
	strcpy(netdev->name, DEV_NAME);

	/*Some housekeeping info */
	priv->umac_status = UMAC_NOTSTARTED;

	atomic_set(&(priv->bk_count), 0);
	atomic_set(&(priv->be_count), 0);
	atomic_set(&(priv->vi_count), 0);
	atomic_set(&(priv->vo_count), 0);

	/*Create WORK Queue for UMAC servicing */
	priv->umac_WQ = create_workqueue("umac_work");
	/*1. Register UMAC BH */
	INIT_WORK(&priv->mc_work, set_mc_list);
	INIT_WORK(&priv->umac_work, UMAC_bh);


	/*For CW1200 the WLAN function number is 1. */
#ifndef USE_SPI
	func->num = 1;
#endif
	priv->func = func;

	priv->netdev = netdev;

#if LINUX_VERSION_CODE < KERNEL_VERSION(2, 6, 30)
	netdev->open	 = EIL_Open ;
	netdev->stop	 = EIL_Stop;
	netdev->hard_start_xmit	 = EIL_Transmit;
	netdev->get_stats		 = EIL_Statistics;
#else
	netdev->netdev_ops = &cw1200_netdev_ops;
#endif

#if LINUX_VERSION_CODE < KERNEL_VERSION(2, 6, 30)
	netdev->tx_timeout	 = EIL_Tx_Timeout;
#endif
	netdev->watchdog_timeo = EIL_TX_TIMEOUT;
	netdev->hard_header_len = EXTRA_MEMORY_SIZE + 8;

	/*Populate Input/Output Params */
	umi_in.apiVersion 	 = 0;
	umi_in.flags		 = 0;
	umi_in.ulHandle		 = (UL_HANDLE)priv;

	/*UPPER LAYER CALL_BACKS */
	umi_in.ulCallbacks.txComplete_Cb = UMI_CB_Tx_Complete;
	umi_in.ulCallbacks.dataReceived_Cb = UMI_CB_Data_Received;
	umi_in.ulCallbacks.schedule_Cb = UMI_CB_Schedule;
	umi_in.ulCallbacks.configReqComplete_Cb = UMI_CB_ConfigReqComplete;
	/*Populate callbacks implemented by CIL */
	umi_in.ulCallbacks.getParameterComplete_Cb
						= UMI_CB_GetParameterComplete;
	umi_in.ulCallbacks.setParameterComplete_Cb
						= UMI_CB_SetParameterComplete;
	umi_in.ulCallbacks.indicateEvent_Cb = UMI_CB_Indicate_Event;
	umi_in.ulCallbacks.scanInfo_Cb = UMI_CB_ScanInfo;
	umi_in.ulCallbacks.txMgmtFrmComplete_Cb = UMI_CB_TxMgmtFrmComplete;
	/*LOWER LAYER CALL-BACKS implemented by SBUS */
	umi_in.llCallbacks.create_Cb = UMI_CB_Create;
	umi_in.llCallbacks.start_Cb = UMI_CB_Start;
	umi_in.llCallbacks.destroy_Cb = UMI_CB_Destroy;
	umi_in.llCallbacks.stop_Cb = UMI_CB_Stop;
	umi_in.llCallbacks.scheduleTx_Cb = UMI_CB_ScheduleTx;
	umi_in.llCallbacks.rxComplete_Cb = UMI_CB_RxComplete;

	umac_retval = UMI_Create(&umi_in, &umi_out);
	if (UMI_STATUS_SUCCESS != umac_retval) {
		DEBUG(DBG_ERROR, "EIL_Init()"
			":UMI_Create returned error:%d \n", umac_retval);
		free_netdev(netdev);
		return ERROR;
	}
	priv->umac_handle = umi_out.umiHandle;

	/*Initialise CIL Layer */
	retval = CIL_Init(priv);
	if (SUCCESS != retval) {
		free_netdev(netdev);
		UMI_Destroy(priv->umac_handle);
		/*Should we call UMI_Destroy herer */
		return ERROR;
	}
	/*UMI_RegisterEvents ()
	not implemented by UMAC yet.To be included when it is implemented.*/

	if (CUT11 == priv->cw1200_cut_no) {
		firmware_path = FIRMWARE_CUT11;
	} else if (CUT10 == priv->cw1200_cut_no) {
		firmware_path = FIRMWARE_CUT10;
	} else if (CUT2 == priv->cw1200_cut_no) {
		firmware_path = FIRMWARE_CUT20;
	} else {
		firmware_path = FIRMWARE_CUT22;
	}

	retval = Read_File(&firmware, firmware_path);
	if (SUCCESS != retval) {
		free_netdev(netdev);
		CIL_Shutdown(priv);
		UMI_Destroy(priv->umac_handle);
		/*Should we call UMI_Destroy herer */
		return ERROR;
	}

	/*Populate param to be sent to UMI_Start () */
	umi_start.pFirmware = firmware.firmware;
	umi_start.fmLength = firmware.length;

	umac_retval = UMI_Start(priv->umac_handle, &umi_start);
	if (UMI_STATUS_SUCCESS != umac_retval) {
		DEBUG(DBG_EIL,
			"UMI_Start () returned error : [%d] \n", umac_retval);
		free_netdev(netdev);
		CIL_Shutdown(priv);
		UMI_Destroy(priv->umac_handle);
		/*Should we call UMI_Destroy herer */
		return ERROR;
	}
	/*Free the firware buffer */
	kfree(firmware.firmware);
	priv->stopped = FALSE;
	priv->eil_shutdown = FALSE ;

	return SUCCESS;
}


/*! \fn  void EIL_Init_Complete (struct CW1200_priv *priv)

	brief This function receives the
	START_COMPLETED event from UMAC and completes
	the registration process with the UMAC.

	param priv -> pointer to driver private structue.

	return -> void.
*/
void EIL_Init_Complete(struct CW1200_priv *priv)
{
	struct cw1200_firmware firmware;
	CW1200_STATUS_E retval = SUCCESS;
	UMI_CONFIG_REQ  *pConfigReq = NULL;
	UMI_STATUS_CODE umi_status = UMI_STATUS_SUCCESS;
	uint32_t rand = 0;
	char *sdd_path = NULL;

	if (CUT11 == priv->cw1200_cut_no) {
		sdd_path = SDD_FILE_11;
	} else if (CUT10 == priv->cw1200_cut_no) {
		sdd_path = SDD_FILE_10;
	} else if (CUT2 == priv->cw1200_cut_no) {
		sdd_path = SDD_FILE_20;
	} else {
		sdd_path = SDD_FILE_22;
	}

	retval = Read_File(&firmware, sdd_path);
	if (SUCCESS != retval) {
		DEBUG(DBG_ERROR, "SDD File I/O error \n");
		CIL_Shutdown(priv);
		free_netdev(priv->netdev);
		UMI_Destroy(priv->umac_handle);
		return;
	}

	pConfigReq = (UMI_CONFIG_REQ *)
	kmalloc(sizeof(UMI_CONFIG_REQ) + firmware.length - 4, GFP_KERNEL);

	/*Set to ZERO so that default values can be used */
	pConfigReq->dot11MaxReceiveLifeTime = 0;
	pConfigReq->dot11MaxTransmitMsduLifeTime = 0;
	pConfigReq->dot11RtsThreshold = 0;
	if (arr_count != MACADDR_LEN) {
		DEBUG(DBG_ERROR, "CORRECT MAC address not passed as module"
			"param\nUsing random MAC address\n");
		rand = random32();
		memcpy(&CW1200_MACADDR[3], &rand, 3);
		DEBUG(DBG_ERROR, "MAC address = [%pM]\n", CW1200_MACADDR);
	} else {
		memcpy(CW1200_MACADDR, mac_addr, MACADDR_LEN);
		DEBUG(DBG_ERROR, "MAC address = [%pM]\n", CW1200_MACADDR);
	}

	if (mode < 3)
	{
		DEBUG(DBG_ERROR, "Setting operational power mode: %d\n",
				 mode);
		priv->operational_power_mode = mode;
	}
	else
	{
		DEBUG(DBG_ERROR, "Using default operational power mode.\n");
#ifndef NO_QUIESCENT_MODE
		priv->operational_power_mode = WLAN_QUIESCENT_MODE;
#else
		priv->operational_power_mode = WLAN_ACTIVE_MODE;
#endif
	}

	memcpy(&pConfigReq->dpdData.macAddress[0],
		&CW1200_MACADDR[0], MACADDR_LEN);
	memcpy(&pConfigReq->dpdData.sddData,
		firmware.firmware, firmware.length);
	pConfigReq->dpdData.length = firmware.length + sizeof(UMI_HI_DPD) - 4;
	pConfigReq->dpdData.flags = 0x0005;

	umi_status = UMI_ConfigurationRequest(priv->umac_handle,
					sizeof(UMI_CONFIG_REQ)
					+ firmware.length - 4,
					pConfigReq);
	if ((UMI_STATUS_SUCCESS != umi_status) &&
					(UMI_STATUS_PENDING != umi_status)) {
		DEBUG(DBG_ERROR, "Config Request"
			" not sent successfully:ERROR: %d\n", umi_status);
		EIL_Shutdown(priv);
		CIL_Shutdown(priv);
	}
	kfree(firmware.firmware);
	kfree(pConfigReq);
}


/*! \fn  int EIL_Transmit (struct sk_buff *skb, struct net_device *dev)

	brief  This function is called by
	the Linux 802.3 stack to transmit a packet onto the device.
	param skb -> pointer to the Linux Network Buffer.
	param dev -> Network Device describing this driver.	]
	return -> ERROR If the packet was not accepted by UMAC.
	SUCCESS -> If the packet was accepted by the UMAC.
*/
int EIL_Transmit(struct sk_buff *skb, struct net_device *dev)
{
	struct CW1200_priv *priv = netdev_priv(dev);
	UMI_TX_DATA *pTxData = NULL;
	UMI_STATUS_CODE umi_status = UMI_STATUS_SUCCESS;
	u16 queue=0;
	u8 prio = 0;
	struct ethhdr *eth = NULL;
	uint32_t i = 0 ,sta_link_id = 0;
	UMI_EVT_DATA_CONNECT_INFO *sta_list = NULL;

	/*Check if the SKB has requisite space required by UMAC */

	if (skb_headroom(skb) < EXTRA_MEMORY_SIZE) {
		/*Expand header and create requisite memory space */
		if (pskb_expand_head(skb,
			EXTRA_MEMORY_SIZE, 0, GFP_ATOMIC)) {
			DEBUG(DBG_ERROR, "EIL_Transmit():"
			" Header expand failed \n");
			return -1 ;
		}
	}
	if (!ALIGN4(skb->data)) {
		DEBUG(DBG_EIL , "EIL_Transmit()"
		":SKB-Data Unaligned [%p] \n", skb->data);
	}

	pTxData = kmalloc(sizeof(UMI_TX_DATA), GFP_ATOMIC) ;

	if (unlikely(NULL == pTxData)) {
		DEBUG(DBG_ERROR, "EIL_Transmit(): System out of memory \n");
		priv->stats.tx_errors++;
		dev_kfree_skb_any(skb);
		return 0;
	}
	pTxData->pExtraBuffer =
		(uint8_t *)((uint8_t *)(skb->data) - EXTRA_MEMORY_SIZE);
	pTxData->pEthHeader  		 = (uint8_t *) (skb->data);
	pTxData->pEthPayloadStart 	 = (uint8_t *) (skb->data + ETH_HLEN);
	pTxData->ethPayloadLen		 = skb->len   - ETH_HLEN;
	pTxData->pDriverInfo		 = (void *)skb;
	dev->trans_start = jiffies;
	/*calculate priority to be passed*/
	prio = cfg80211_classify8021d(skb);

	DEBUG(DBG_EIL, "priority prio=%02x\n", prio);
	queue = skb_get_queue_mapping(skb);

	DEBUG(DBG_EIL, "Queue=%02x\n", queue);

	/* Find the link for the destination addr */
	if (NL80211_IFTYPE_AP == priv->device_mode) {
		eth = (struct ethhdr *)skb->data;
		if (is_multicast_ether_addr(eth->h_dest)) {
			sta_link_id = 0;
		} else {
			for (i=0; i < MAX_SUPPORTED_STA; i++) {
				sta_list = (UMI_EVT_DATA_CONNECT_INFO *)(priv->sta_info + i);
				if (compare_ether_addr(eth->h_dest,
					&(sta_list->stationMacAddr[0]) ) == 0) {
					sta_link_id = sta_list->linkId;
					break;
				}
			}
			if ( i >= MAX_SUPPORTED_STA) {
				DEBUG(DBG_ERROR, "EIL_Transmit():STA not found "
					"in internal list \n");
				priv->stats.tx_errors++;
				kfree(pTxData);
				dev_kfree_skb_any(skb);
				return 0;
			}
		}
	}

	umi_status = UMI_Transmit(priv->umac_handle, prio, sta_link_id, pTxData);
	if (unlikely(UMI_STATUS_SUCCESS != umi_status)) {
		priv->stats.tx_errors++;
		kfree(pTxData);
		dev_kfree_skb_any(skb);
	} else {
		if (queue == AC_BK) {
			atomic_inc(&(priv->bk_count));
			if (atomic_read(&(priv->bk_count)) >= AC_BK_QUEUE  ) {
			DEBUG(DBG_EIL, "EIL_Transmit ():Going to stop BK Queue\n");
			netif_stop_subqueue(dev, queue);
			}
		} else if (queue == AC_BE) {
			atomic_inc(&(priv->be_count));
			if (atomic_read(&(priv->be_count)) >= AC_BE_QUEUE) {
				DEBUG(DBG_EIL, "EIL_Transmit ():Going to stop"
						"BE Queue\n");
				netif_stop_subqueue(dev, queue);
			}
		} else if (queue == AC_VI) {
			atomic_inc(&(priv->vi_count));
			if (atomic_read(&(priv->vi_count)) >= AC_VI_QUEUE) {
				DEBUG(DBG_EIL, "EIL_Transmit ():Going to stop"
						"VI Queue\n");
				netif_stop_subqueue(dev, queue);
			}
		} else if (queue == AC_VO) {
			atomic_inc(&(priv->vo_count));
			if (atomic_read(&(priv->vo_count)) >= AC_VO_QUEUE) {
				DEBUG(DBG_EIL, "EIL_Transmit ():Going to stop"
						"VO Queue\n");
				netif_stop_subqueue(dev, queue);
			}
		}
		priv->stats.tx_packets++;
	}

	return SUCCESS;
}

/*! \fn  static int EIL_Stop (struct net_device *dev)

	brief  This function is called by the Linux 802.3
	stack to stop data transfer over the device.
	param dev -> Network Device describing this driver.
	return -> SUCCESS. Always returns success.
*/
static int EIL_Stop(struct net_device *dev)
{
	struct CW1200_priv *priv = netdev_priv(dev);
	UMI_BSS_LIST_SCAN scan_req;
	CW1200_STATUS_E	retval =0;

	DEBUG(DBG_EIL, "EIL_Stop called  \n");
	netif_stop_queue(dev);
	/* If SCAN is ongoing abort it */
	mutex_lock(&priv->mutex_cil);
	if (priv->request != NULL) {
		memset(&scan_req, 0, sizeof(UMI_BSS_LIST_SCAN));
		scan_req.flags = 0;
		retval = CIL_Set(priv,
			UMI_DEVICE_OID_802_11_BSSID_LIST_SCAN,
			&scan_req, sizeof(UMI_BSS_LIST_SCAN));
		cfg80211_scan_done(priv->request , true);
		priv->request = NULL;

	}
	mutex_unlock(&priv->mutex_cil);

	return 0;
}

/*! \fn  static int EIL_Open(struct net_device *dev)

	brief  This function is called by the Linux 802.3 stack to
	indicate to the driver that stack is ready to TX data,
	param dev -> Network Device describing this driver.
	return -> SUCCESS. Always returns success.
*/
static int EIL_Open(struct net_device *dev)
{

	DEBUG(DBG_EIL, "EIL_Open called \n");
	/*Start network queues so that driver can start receiving data */
	netif_tx_start_all_queues(dev);
	return 0;
}

/*! \fn  void EIL_Shutdown(struct CW1200_priv *priv)

	brief This function stops and deletes the UMAC
	instance and calls the CIL shutdown handler.
	param priv -> pointer to driver private structue.
	return -> void.
*/
void EIL_Shutdown(struct CW1200_priv *priv)
{
	DEBUG(DBG_EIL, "EIL_Shutdown called \n");
	if (priv->eil_shutdown != TRUE) {
		UMI_Stop(priv->umac_handle);
		priv->eil_shutdown = TRUE ;
	}
}


/*! \fn  static struct net_device_stats *EIL_Statistics(struct net_device *dev)

	brief  This function is called by the Linux 802.3 stack
	to get driver statistic (RX/TX count etc)
	param dev -> Network Device describing this driver.
	return -> Pointer to net_device_stats
*/
static struct net_device_stats *EIL_Statistics(struct net_device *dev)
{
	struct CW1200_priv *priv = netdev_priv(dev);

	dev->stats.tx_errors = priv->stats.tx_errors;
	dev->stats.tx_packets = priv->stats.tx_packets;
	dev->stats.rx_packets = priv->stats.rx_packets;

	return &dev->stats;
}

/*! \fn  static void EIL_Tx_Timeout(struct net_device *dev)

	brief  This function is called by the Linux 802.3 stack
	to get driver statistic (RX/TX count etc)
	param dev -> Network Device describing this driver.
	return -> Pointer to net_device_stats
*/
static void EIL_Tx_Timeout(struct net_device *dev)
{
	struct CW1200_priv *priv = netdev_priv(dev);

	DEBUG(DBG_EIL, "Tx Timeout detected by Linux Kernel \n");
	priv->stats.tx_errors++;
        atomic_xchg(&(priv->Interrupt_From_Device), TRUE);
        queue_work(priv->sbus_WQ, &priv->sbus_work);
}


/*! \fn static void EIL_set_mc_list(struct net_device *dev)

	brief  This function is called by the Linux 802.3 stack
	to notify change in multicast address filter.
	param dev -> Network Device describing this driver.
*/
static void EIL_set_mc_list(struct net_device *dev)
{
	struct CW1200_priv *priv = netdev_priv(dev);

	DEBUG(DBG_EIL, "MCaddress list update detected by Linux Kernel \n");
	schedule_work(&priv->mc_work);
}



static CW1200_STATUS_E Read_File(struct cw1200_firmware *frmwr, char *file_path)
{

	mm_segment_t fs;
	unsigned int bcount, length;
	struct file *ifp;
	char *buffer = NULL;
#if LINUX_VERSION_CODE < KERNEL_VERSION(2, 6, 30)
	struct files_struct *files = current->files;
#endif
	loff_t pos;

	/*for file opening temporarily tell the kernel I am not a user for
	*memory management segment access */
	if (NULL == frmwr) {
		DEBUG(DBG_EIL, "Invalid firmware read buffer \n");
		return ERROR;
	}
	frmwr->length = 0;
	fs = get_fs();
	set_fs(KERNEL_DS);

	ifp = filp_open(file_path, O_RDONLY | O_LARGEFILE, 0);
	if (IS_ERR(ifp)) {
		DEBUG(DBG_EIL, "failed to open file %s\n", file_path);
		set_fs(fs);
		return ERROR;
	}
	length = ifp->f_path.dentry->d_inode->i_size;

	buffer = kmalloc(length, GFP_KERNEL);

	if (unlikely(!buffer)) {
		DEBUG(DBG_EIL, "Error in creating FW buffer \n");
		return ERROR;
	}
	pos = 0;
	bcount = vfs_read(ifp, (char __user *)buffer, length, &pos);

	if (unlikely(bcount != length)) {
		DEBUG(DBG_EIL, "ReadFirmware ():Incorrect read %s", file_path);
		return ERROR;
	}
#if LINUX_VERSION_CODE < KERNEL_VERSION(2, 6, 30)
	filp_close(ifp, current->files);
#else
	filp_close(ifp, NULL);
#endif
	set_fs(fs);

	frmwr->firmware = buffer;
	frmwr->length = bcount;

	return SUCCESS;
}

/*****************************************************************************
*			UMAC Callback Functions
******************************************************************************/

/*! \fn  void  UMI_CB_Tx_Complete(UL_HANDLE UpperHandle,
				UMI_STATUS_CODE Status,
				UMI_TX_DATA *pTxData);
	brief  This function is called by UMAC to indicate
	to the driver that TX to the device is complete and the
	driver can free the buffer.
	param UpperHandl-The upper layer driver instance for UMI callback.
	param Statu	-Completion status code.
	param pTxData- structure that contains frame which transmitted on air.
	return -> None
*/
void  UMI_CB_Tx_Complete(UL_HANDLE UpperHandle,
			UMI_STATUS_CODE Status,
			UMI_TX_DATA *pTxData)
{
	struct CW1200_priv *priv = (struct CW1200_priv *)UpperHandle;
	uint32_t queue=0;

	DEBUG(DBG_EIL, "UMI_CB_Tx_Complete Status : [%d] \n", Status);

	if (NULL != pTxData) {
		queue = skb_get_queue_mapping((struct sk_buff *)pTxData->pDriverInfo);

		DEBUG(DBG_EIL,"UMI_CB_Tx_Complete : Queue [%d] \n",queue);
		if ( netif_tx_queue_stopped(netdev_get_tx_queue(priv->netdev, queue)) ) {
			netif_tx_wake_queue(netdev_get_tx_queue(priv->netdev, queue));
		}

		if (queue == AC_BK) {
			atomic_dec(&priv->bk_count);
		} else if (queue == AC_BE) {
			atomic_dec(&priv->be_count);
		} else if (queue == AC_VI) {
			atomic_dec(&priv->vi_count);
		} else if (queue == AC_VO) {
			atomic_dec(&priv->vo_count);
		}
		dev_kfree_skb_any((struct sk_buff *)pTxData->pDriverInfo);
		kfree(pTxData);
	}
}

/*! \fn  UMI_STATUS_CODE UMI_CB_Data_Received(UL_HANDLE	UpperHandle,
				UMI_STATUS_CODE	Status,
				uint16 Length, void *pFrame);
	brief  This function is called by UMAC to indicate to
	the driver that UMAC has received a packet from the device.
	param UpperHandle-The upper layer driver instance for UMI callback.
	param Status	-Completion status code.
	param pTxData	- A structure that contains
	frame which transmitted on air.
	return -> UMI Status Code
*/
UMI_STATUS_CODE UMI_CB_Data_Received(UL_HANDLE	UpperHandle,
				UMI_STATUS_CODE	Status,
				uint16_t Length,
				void *pFrame, void *pDriverInfo,
				void *pFrmStart, uint16_t flags)
{
	struct CW1200_priv *priv = (struct CW1200_priv *)UpperHandle;
	struct sk_buff *skb = NULL;
	uint8_t *data = (uint8_t *)pFrame;
	struct sk_buff *fw_skb = NULL;


	if (UMI_STATUS_SUCCESS != Status) {
		DEBUG(DBG_ERROR, "UMI_CB_Data_Received ()"
		"Callback returned error [%x]\n", Status);
		skb = (struct sk_buff *)pDriverInfo;
		dev_kfree_skb_any(skb);
		return UMI_STATUS_SUCCESS;
	}

	if (0 == Length) {
		DEBUG(DBG_ERROR, "UMI_CB_Data_Received () Invalid Length \n");
		return UMI_STATUS_SUCCESS;
	}

	if (unlikely(flags & MORE_DATA_FLAG)) {
		DEBUG(DBG_EIL, "%s: More Data Flag is set \n", __func__);
		if (flags & FRAME_COUNT) {
			skb = dev_alloc_skb(Length);
			if (!skb) {
				DEBUG(DBG_EIL, "%s:Out of Memory \n", __func__);
				EIL_Shutdown(priv);
				return UMI_STATUS_SUCCESS;
			}
			skb_put(skb, Length);
			memcpy(skb->data, pFrame , Length);
		} else {
			DEBUG(DBG_EIL,"%s,Unknown Error:No Frame count \n",
					__func__);
			return UMI_STATUS_FAILURE;
		}
	} else {
		skb = (struct sk_buff *)pDriverInfo;
		DEBUG(DBG_EIL, "%s, Retrieved SKB pointer&SKB Len[%p],"
			"[%d], [%d] \n", __func__, skb, skb->len, Length);
		skb->data = pFrame;
		skb->len = Length;
		skb->tail = skb->data + (Length - 1);
	}

	if (unlikely(flags & BRIDGED_FRAME)) {
		fw_skb = skb_copy(skb , GFP_ATOMIC);
		if (!fw_skb) {
			DEBUG(DBG_EIL," Not able to allocate"
					"bridged frame \n");
			return UMI_STATUS_SUCCESS;
		}
		skb = fw_skb;
	}

	priv->stats.rx_packets++;
	priv->stats.rx_bytes += Length;
	skb->dev = priv->netdev;
	skb->protocol = eth_type_trans(skb, priv->netdev);
	skb->ip_summed = CHECKSUM_NONE;
	/*Give the packet to the Linux Kernel 802.3 stack */
	priv->stats.rx_packets++;
	netif_rx(skb);

	return UMI_STATUS_SUCCESS;
}

/*! \fn  void UMI_CB_Schedule(UL_HANDLE UpperHandle);
	brief  This function is called by UMAC to ask the driver for context.
	param UpperHandle-The upper layer driver
	instance for UMI callback.
	return -> None
*/
void UMI_CB_Schedule(UL_HANDLE	UpperHandle)
{

	struct CW1200_priv *priv = (struct CW1200_priv *)UpperHandle;

	/*Schedule Bottom Half to service UMAC */
	queue_work(priv->umac_WQ, &priv->umac_work);
}

/*! 	fn  oid UMI_CB_ConfigReqComplete (UL_HANDLE UpperHandle,
	UMI_CONFIG_CNF 	Status)
	brief  This callback function indicates to the drive that
	the configuration request is complete
	param UpperHandle	-	The upper layer driver
	instance for UMI callback.
	param Cnf_msg	-	UMI_CONFIG_CNF structure
	indicating stationid	and config request status
	return -> None
 */
void UMI_CB_ConfigReqComplete(UL_HANDLE UpperHandle,
			UMI_CONFIG_CNF *pConfigCnf)

{
	struct CW1200_priv *priv = (struct CW1200_priv *)UpperHandle;
	uint32_t retval = SUCCESS;
	uint32_t filter_enable = 1;
	uint32_t Disable_80211d = 0;
	uint32_t oprPowerMode = 0;
	/* Enable multiTX conf */
	uint32_t useMultiTxConfMsg = 1;
	uint32_t bg_scan=0;

	DEBUG(DBG_EIL, "ConfigReqComplete Called \n");

	if (UMI_STATUS_SUCCESS != pConfigCnf->result) {
		DEBUG(DBG_ERROR, "CONFIG Request"
		"Complete callback returned error \n");
		free_netdev(priv->netdev);
		UMI_Destroy(priv->umac_handle);
	} else {
		priv->umac_status = UMAC_STARTED;
		retval = CIL_Set(priv,
				UMI_DEVICE_OID_802_11_SET_HOST_MSG_TYPE_FILTER,
				&filter_enable, 4);
		if (retval) {
			DEBUG(DBG_CIL, "CIL_Set() for UMI_DEVICE"
			"_OID_802_11_SET_DATA_TYPE_FILTER returned error  \n");
		}
		DEBUG( DBG_EIL, "EIL:MAC address from MAC:%pM \n",
				&(pConfigCnf->dot11StationId[0]));
		/*Completed network device registration */
		memcpy(priv->netdev->dev_addr, &(pConfigCnf->dot11StationId[0]),
			 ETH_ALEN);
		register_netdev(priv->netdev);
		retval = CIL_Set(priv,
				UMI_DEVICE_OID_802_11_ENABLE_11D_FEATURE,
				&Disable_80211d, 4);
		if (retval) {
			DEBUG(DBG_EIL, "%s :CIL_Set for"
			"80211 D returned error \n", __func__);
		}
		/* Enable Awake mode  */
		oprPowerMode = WLAN_ACTIVE_MODE;
		retval = CIL_Set(priv,
				UMI_DEVICE_OID_802_11_OPERATIONAL_POWER_MODE,
				&oprPowerMode, 4);
		if (retval) {
			DEBUG(DBG_EIL, "CIL_Set() for UMI_DEVICE"
			"_OID_802_11_OPERATIONAL_POWER_MODE returned error  \n");
		}

		retval = CIL_Set(priv,
				UMI_DEVICE_OID_802_11_MULTI_TX_CONFIRMATION,
				&useMultiTxConfMsg, 4);
		if (retval) {
			DEBUG(DBG_EIL, "CIL_Set() for UMI_DEVICE"
			"_OID_802_11_MULTI_TX_CONFIRMATION returned error  \n");
		}

		/*Enable BG Scan */
		bg_scan = 0;
		retval = CIL_Set(priv,
			UMI_DEVICE_OID_802_11_DISABLE_BG_SCAN,
			&bg_scan, 4);
		if (retval) {
			DEBUG(DBG_EIL,
			"cw1200_connect():CIL_Set for BG Scan returned error \n");
			return -EIO;
		}
	}
	return;
}

