/*==========================================================
 *
 *Linux Driver for CW1200 series
 *
 *
 *Copyright (c) ST-Ericsson SA, 2010
 *
 *This program is free software; you can redistribute it and/or modify it
 *under the terms of the GNU General Public License version 2 as published
 *by the Free Software Foundation.
 *
 *=========================================================*/
/**
 *addtogroup Linux Driver SBUS Layer
 *brief
 *
 */
/**
 *file sdio_wrapper.c
 *- <b>PROJECT</b>			 : CW1200_LINUX_DRIVER
 *- <b>FILE</b>					: sdio_wrapper.c
 *brief
 *This module interfaces with the Linux Kernel MMC/SDIO stack.
 *ingroup SBUS
 *date 25/02/2010

 **LAST MODIFIED BY:	Harald Unander
 **CHANGE LOG :
 */


#include <linux/version.h>
#include <linux/module.h>
#include <linux/delay.h>
#include <linux/mmc/host.h>
#include <linux/mmc/sdio_func.h>
#include <linux/mmc/card.h>
#include <linux/regulator/consumer.h>
#include <linux/cw1200-wlan.h>

#ifndef RESOUTN_WLAN_ENABLE
#include <linux/gpio.h>
#else
#include <mach/db5500-regs.h>
#endif

#include "cw1200_common.h"
#include "sbus_wrapper.h"
#include "eil.h"
#include "sbus.h"
#include "cil.h"

#ifdef RESOUTN_WLAN_ENABLE
/* PRCMU RESOUTN registers */
#define PRCM_RESOUTN_SET_OFFSET	0x214
#define PRCM_RESOUTN_CLR_OFFSET	0x218
#define PRCM_RESOUTN_VAL_OFFSET	0x21C
#define RESOUT2_N_PIN			(1<<2)
#endif

/* Regulator support related function declarations */
static int cw1200_enable_regulator(struct sdio_func *);
static void cw1200_disable_regulator(void);


struct sdio_func *g_func;
struct mmc_host *host;
struct work_struct init_work;
struct wlan1200_platform_data *wpd;
CW1200_STATUS_E eil_init_status = SUCCESS;

/* Regulator support */
struct regulator *regulator;
int32_t regulator_enabled;

static void cw1200_device_release(struct device *dev)
{
	DEBUG(DBG_SBUS,"CW1200 device release\n");
}

static struct device cw1200_device = {
	.release = cw1200_device_release,
};

static const struct sdio_device_id if_sdio_ids[] = {
	{ SDIO_DEVICE(SDIO_ANY_ID, SDIO_ANY_ID) },
	{ /*end: all zeroes */				},
};

int sbus_memcpy_fromio(CW1200_bus_device_t *func,
		void *dst, unsigned int addr, int count)
{
	return sdio_memcpy_fromio(func, dst, addr, count);
}

int sbus_memcpy_toio(CW1200_bus_device_t *func,
		unsigned int addr, void *src, int count)
{
	return sdio_memcpy_toio(func, addr, src, count);
}

/* Function exported from mmc-u8500.c */
extern void u8500_sdio_detect_card(void);
/*Probe Function to be called by SDIO stack when device is discovered */
static int cw1200_sdio_probe(struct sdio_func *func,
		const struct sdio_device_id *id)
{

	DEBUG(DBG_MESSAGE, "CW1200-Driver Probe called \n");

	eil_init_status = cw1200_enable_regulator(func);
	if (eil_init_status) {
		DEBUG(DBG_ERROR,"Could not enable regulator framework \n");
	}
	g_func = func;
	eil_init_status = EIL_Init(func);
	DEBUG(DBG_SBUS, "EIL_Init() return status [%d] \n", eil_init_status);

	return 0;
}

/*Disconnect Function to be called by SDIO
 * stack when device is disconnected */
static void cw1200_sdio_disconnect(struct sdio_func *func)
{
	DEBUG(DBG_SBUS, "%s, called \n", __func__);
	cw1200_disable_regulator();
}

static struct sdio_driver sdio_driver = {
  .name = "cw1200_sdio",
  .id_table = if_sdio_ids,
  .probe = cw1200_sdio_probe,
  .remove = cw1200_sdio_disconnect,
};



static void init_bh(struct work_struct *work)
{
	int32_t status = 0;

#ifndef RESOUTN_WLAN_ENABLE
	wpd = wlan1200_get_platform_data();
	/* Request WLAN Enable GPIO */
	status = gpio_request(wpd->gpio_enable, "sdio_init");
	if (status) {
		DEBUG(DBG_SBUS , "INIT : Unable to request gpio_wlan_enable %d \n", wpd->gpio_enable);
		return;
	}
	gpio_direction_output(wpd->gpio_enable, 1);
	gpio_set_value(wpd->gpio_enable, GPIO_HIGH);
	mdelay(10);
#else
	void __iomem *_PRCMU_BASE = __io_address(U5500_PRCMU_BASE);
	writel(RESOUT2_N_PIN, _PRCMU_BASE + PRCM_RESOUTN_SET_OFFSET);
	mdelay(50);
#endif

	u8500_sdio_detect_card();
}

static int cw1200_enable_regulator(struct sdio_func * func)
{
#ifdef CONFIG_REGULATOR
	int err;


	cw1200_device.parent = &func->dev;
	dev_set_name(&cw1200_device, "cw1200");
	err = device_register(&cw1200_device);
	if (err) {
			DEBUG(DBG_ERROR,"Cant register CW1200 device \n");
			return err;
	}
	/* Get and enable regulator. */
	regulator = regulator_get(&cw1200_device, "wlan_1v8");
	if (IS_ERR(regulator)) {
		DEBUG(DBG_ERROR, "Not able to find regulator.");
		err = PTR_ERR(regulator);
	} else {
		err = regulator_enable(regulator);
		if (err) {
			DEBUG(DBG_SBUS, "Not able to enable regulator.");
		}
		else {
			DEBUG(DBG_SBUS, "Regulator enabled.");
			regulator_enabled = true;
		}
	}
	return err;
#else
	return 0;
#endif
}

/**
 * cw1200_disable_regulator() - Disable regulator.
 *
 */
static void cw1200_disable_regulator()
{
#ifdef CONFIG_REGULATOR

	/* Disable and put regulator. */
	if (regulator && regulator_enabled) {
		regulator_disable(regulator);
		regulator_enabled = false;
	}
	regulator_put(regulator);
	regulator = NULL;
	device_unregister(&cw1200_device);
#endif
}


/*Init Module function -> Called by insmod */
static int __init sbus_sdio_init(void)
{

	DEBUG(DBG_SBUS, "STE WLAN- Driver Init Module Called \n");

	INIT_WORK(&init_work , (void (*)(void *))init_bh);
	sdio_register_driver(&sdio_driver);
	schedule_work(&init_work);
	/* Supplicant does not start if we do not have this delay */
	 msleep(1500);

	return 0;
}


 /*Called at Driver Unloading */
static void __exit sbus_sdio_exit(void)
{
	struct CW1200_priv *priv = NULL;
	int  bound = MAX_WAIT;

	/*If func is null then probe was not called */
	if (NULL == g_func) {
		sdio_unregister_driver(&sdio_driver);
		return;
	}

	priv = sdio_get_drvdata(g_func);
	host = g_func->card->host;

	DEBUG(DBG_SBUS, "EIL Shutdown from cleanup\n");
	EIL_Shutdown(priv);

	while (priv->stopped != TRUE && (bound > 0)) {
		--bound;
		msleep(100);
	}
	if (!(bound)) {
		DEBUG(DBG_ERROR, "Driver not stopped in time\n");
		/* Netdev is valid if EIL was started */
		if (SUCCESS == eil_init_status) {
			/* Driver might get unregistered at UMI_CB_Indicate_Event()
			without priv->stopped being set */
			if (priv->netdev->reg_state == NETREG_REGISTERED)
				unregister_netdev(priv->netdev);
			CIL_Shutdown(priv);
		}
	}
	/* If EIL Init was successfull then de-init */
	if (SUCCESS == eil_init_status) {
		DEBUG(DBG_MESSAGE, "UMI_Destroy from cleanup \n");
		UMI_Destroy(priv->umac_handle);

		flush_workqueue(priv->umac_WQ);
		destroy_workqueue(priv->umac_WQ);
		DEBUG(DBG_MESSAGE, "Going to call flush WK- SBUS \n");
		/*Moved here from SBUS */
		flush_workqueue(priv->sbus_WQ);
		destroy_workqueue(priv->sbus_WQ);
		free_netdev(priv->netdev);
	}
	sdio_unregister_driver(&sdio_driver);
	DEBUG(DBG_MESSAGE, "Detect Change called \n");
	mdelay(10);
#ifndef RESOUTN_WLAN_ENABLE
	gpio_set_value(wpd->gpio_enable, GPIO_LOW);
	mdelay(10);
	gpio_free(wpd->gpio_enable);
#else
	void __iomem *_PRCMU_BASE = __io_address(U5500_PRCMU_BASE);
	writel(RESOUT2_N_PIN, _PRCMU_BASE + PRCM_RESOUTN_CLR_OFFSET);
	mdelay(50);
#endif

	u8500_sdio_detect_card();

	DEBUG(DBG_SBUS, "Unloaded WLAN driver, n");
}


module_init(sbus_sdio_init);
module_exit(sbus_sdio_exit);
