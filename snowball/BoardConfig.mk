ENABLE_ST_ERICSSON_BUILD := true
# U8500 uses STEricsson's bootloader, not one from source
#
TARGET_BOARD_PLATFORM := snowball
TARGET_CPU_ABI := armeabi-v7a
TARGET_CPU_ABI2 := armeabi
TARGET_CPU_SMP := true
TARGET_NO_BOOTLOADER := true
TARGET_NO_KERNEL := false
TARGET_KERNEL_IMAGE := uImage
TARGET_NO_RADIOIMAGE := true
TARGET_ARCH_VARIANT := armv7-a-neon
TARGET_NO_WPA_SUPPLICANT := true
TARGET_USE_ST_ERICSSON_KERNEL := true

# Enable this to get a build where 720p is the max resolution
# STE_HDMISERVICE_SET_PLATFORM := snowball_720

ARCH_ARM_HAVE_TLS_REGISTER := true


MM_PACKAGE ?= $(ANDROID_BUILD_TOP)/vendor/st-ericsson/snowball/restricted

# Set to true where MM is to be build
TARGET_USE_ST_ERICSSON_MULTIMEDIA := true


# Set to true for platforms with 32 byte L2 cache line.
# Set to false for platforms with 64 byte L2 cache line.
ARCH_ARM_HAVE_32_BYTE_CACHE_LINES := true

# Set to true for CPUs with the Control Register (c1) A-bit set to zero
# allowing unaligned access for Neon memory instructions.
ARCH_ARM_NEON_SUPPORTS_UNALIGNED_ACCESS := true

# Sets the upper size limit for Neon unaligned memory access in memcpy.
# For sizes smaller than this, no pre-alignment is made before the main loop.
# This has no effect unless ARCH_ARM_NEON_UNALIGNED_ACCESS is also set to true.
BIONIC_MEMCPY_ALIGNMENT_DIVIDER := 255

# Sets the upper size limit for Neon implementation of memset.
# For sizes larger than this, the ARM version of memset is used.
BIONIC_MEMSET_NEON_DIVIDER := 140

CROSS_COMPILE := arm-eabi-

# USE U8500 init.rc
TARGET_PROVIDES_INIT_RC := true

# USE U8500 recovery init.rc
TARGET_PROVIDES_RECOVERY_INIT_RC := true

#####################################################################
# STELP paths

ACCESS_SERVICES_PATH := $(TOP)/vendor/st-ericsson/access_services
CONNECTIVITY_PATH := $(TOP)/vendor/st-ericsson/connectivity
MULTIMEDIA_PATH := $(TOP)/vendor/st-ericsson/multimedia
PROCESSING_PATH := $(TOP)/vendor/st-ericsson/processing
STORAGE_PATH := $(TOP)/vendor/st-ericsson/storage
BOOT_PATH := $(TOP)/vendor/st-ericsson/boot
BASE_UTILITIES_PATH := $(TOP)/vendor/st-ericsson/base_utilities
TOOLS_PATH := $(TOP)/vendor/st-ericsson/tools
BUILD_PATH := $(TOP)/vendor/st-ericsson/build
APPS_PATH := $(TOP)/vendor/st-ericsson/apps
HARDWARE_PATH := $(TOP)/vendor/st-ericsson/hardware
####################################################################

####################################################################
# KERNEL, WLAN & UBOOT OUTPUT dirs

KERNEL_OUTPUT_RELATIVE := $(TARGET_OUT_INTERMEDIATES)/kernel
KERNEL_OUTPUT := $(abspath $(KERNEL_OUTPUT_RELATIVE))
UBOOT_OUTPUT := $(TARGET_OUT_INTERMEDIATES)/uboot
# If the UBOOT_SPLASH_IMAGE_OUTPUT variable is changed the copy
# in ste_uxx00.mk (vendor/st-ericsson/products) also needs to be updated
UBOOT_SPLASH_IMAGE_OUTPUT := splash.bin
WLAN_OUTPUT = $(abspath $(TARGET_OUT_INTERMEDIATES)/wlan)
####################################################################

## Build ALSA-utils
BUILD_WITH_ALSA_UTILS := true
BOARD_USES_ALSA := true

## Build ALSA-lib
BOARD_USES_ALSA_AUDIO := true

## Below line will select Lunds ANM/ADM as AudioHardwareInterface
BOARD_USES_LD_ANM := true
BOARD_BYPASSES_AUDIOFLINGER_A2DP := true

# Enable this to get a build where audio is routed to diffrent outputs
# Use Snowball policy
STE_AUDIO_SET_PLATFORM := snowball
# Set audio to HDMI
#STE_AUDIO_SET_PLATFORM_OUTPUT := hdmi
# Set audio to AUDIO OUT
STE_AUDIO_SET_PLATFORM_OUTPUT := out

################################################################################
## PV/MediaPlayer related settings
BUILD_PV_VIDEO_ENCODERS:=1
BUILD_PV_2WAY:=1
BUILD_PV_TEST_APPS:=1
BUILD_WITHOUT_PV:=false

################################################################################
## ENABLE STAGEFRIGHT and STE CODECS IN STAGEFRIGHT
BUILD_WITH_FULL_STAGEFRIGHT:=true
STE_CODECS_IN_STAGEFRIGHT:=true

################################################################################

# Kernel/Bootloader machine name
#
TARGET_BOOTLOADER_BOARD_NAME := montblanc

# To enable AT parser plugin
AT_PLUGIN_ON := false

BOARD_GPS_LIBRARIES := libgps

# Flash Partition sizes
BOARD_RAMDISKIMAGE_PARTITION_SIZE := 10485760
BOARD_BOOTIMAGE_PARTITION_SIZE := 8388608
BOARD_SYSTEMIMAGE_PARTITION_SIZE := 230686720
BOARD_USERDATAIMAGE_PARTITION_SIZE := 197132288
BOARD_CACHEIMAGE_PARTITION_SIZE := 134217728
BOARD_MISCIMAGE_PARTITION_SIZE := 4096

TARGET_USERIMAGES_USE_EXT4 := true

# Erase Unit size
BOARD_FLASH_BLOCK_SIZE := 4096

BOARD_HAVE_BLUETOOTH := true

###############################################################################

## Module configuration flags ##

#<MODULE_NAME>_ENABLE_FEATURE_<FEATURE_NAME_u8500> := true

# Enable signature verification features necessary for production fused hw
ifeq (true,$(ENABLE_FEATURE_SIGNATURE_VERIFICATION))
#Add signature verification related module flags
ISSW_ENABLE_FEATURE_SIGN_IMAGES := true
UBOOT_ENABLE_FEATURE_SECBOOT := true
endif

# Kernel configuration
KERNEL_DEFCONFIG ?= ../../../../vendor/st-ericsson/snowball/snowball_defconfig

# Kernel firmware settings
KERNEL_FIRMWARE_ENABLE_FEATURE_BT := true
KERNEL_FIRMWARE_ENABLE_FEATURE_FM := true
KERNEL_FIRMWARE_ENABLE_FEATURE_WLAN := true
KERNEL_FIRMWARE_ENABLE_FEATURE_HDMI := true

# Set Multimdia platform
MULTIMEDIA_SET_PLATFORM := u8500

# Set Bass App in build
LIBBASSAPP_ENABLE_FEATURE_LIBBASSAPP=false

# Set Liblights platform
LIBLIGHTS_SET_PLATFORM := u8500

# Set Libsensors platform 
# Need to be defined for diffrent Snowball HW versions
# boards up to v6
# LIBSENSORS_SET_PLATFORM := snowball-vx-6
# boards from v7 and above
LIBSENSORS_SET_PLATFORM := snowball-v7-x

# Camera settings
# Enable/Disable ST-Ericsson Camera
USE_CAMERA_STUB := true
# Camera product configuration
STE_CAMERA_ENABLE_FEATURE_PLATFORM := u8500
# Select Camera Sensor
CAMERA_SET_PRIMARY_SENSOR ?= IMX072
CAMERA_SET_SECONDARY_SENSOR ?= MT9V113
# Enable/Disable RAW BAYER sensor
CAMERA_ENABLE_FEATURE_RAW_SENSOR=true
# Enable/Disable YUV camera
CAMERA_ENABLE_FEATURE_YUV_CAMERA=true

# Select u-boot configuration
UBOOT_DEFCONFIG ?= u8500_snowball_config
UBOOT_SET_SPLASH_IMAGE := $(BOOT_PATH)/u-boot/tools/logos/stericsson.bmp
# Set input and output variables for u-boot environment parameter image
BUILD_UBOOT_ENV_IMG_INPUT := $(TOP)/vendor/st-ericsson/snowball/uboot_envparameters_android.cfg
BUILD_UBOOT_ENV_IMG_OUTPUT := $(UBOOT_OUTPUT)/u-boot-env.bin

# RIL configuration
RIL_ENABLE_FEATURE_RIL := true
RIL_ENABLE_FEATURE_CAIF := false
RIL_ENABLE_FEATURE_DUAL_CHANNELS := true
RIL_ENABLE_FEATURE_GPRS_AUTO_ATTACH := true
RIL_ENABLE_FEATURE_SMS_PHASE_2_PLUS := true
# FIXME: remove when RIL is merged
RIL_ENABLE_FEATURE_U8500 := true

## Add modem specific file system contents into root file system image for
## U8500 to satisfy expectations from NWM.
NWM_ENABLE_FEATURE_MODEMFS := true

## Enable Modem Storage Agent so that it can serve file system requests
## from the NWM modem.
MSA_ENABLE_FEATURE_MSA := true

## Select what ADM-version to build when building for U8500
ADM_VERSION := u8500

## Add Basic Security Services(BASS) functionalities
BASS_APP_ENABLE_FEATURE_BASS_APP := true

# Select for STE OMX JPEG decoder in SKIA and MediaServer
STE_JPEGDEC_ENABLE_FEATURE_STE_JPEGDEC := true

# Set Modem adaptation configuration for LBS module.
LBS_SET_MODEMCLIENT := NONE

# Enable speech and audio probes
# WARNING: This feature should be disabled (set to false) in production software
MMPROBE_ENABLE_FEATURE_MMPROBE := false
CSCALL_ENABLE_SILENT_REBOOT_SUPPORT := false

# Enable tuning server
# WARNING: This feature should be disabled (set to false) in production software
TUNINGSERVER_ENABLE_FEATURE_TUNINGSERVER := true

# SurfaceFlinger configuration
# Enable dim with texture
SF_ENABLE_FEATURE_DIM_WITH_TEXTURE := true
# Enable frame buffer rotation
SF_ENABLE_FEATURE_FB_ROTATION := true

# Set hwconfig to use in flashkit
FLASHKIT_SET_HWCONFIG := u8500

# Enable build of fm radio vendor library
FMRADIO_CG2900_ENABLE_FEATURE_VENDOR_DRIVE := true

# Enable STE WiFi Tethering/SoftAPController
WLAN_ENABLE_STE_WIFI_TETHERING := false

# WiFi Configuration
WLAN_ENABLE_OPEN_MAC_SOLUTION := true

WLAN_SET_PLATFORM := u8500

ifeq ($(WLAN_ENABLE_OPEN_MAC_SOLUTION), true)
WIFI_DRIVER_MODULE_PATH := /system/lib/modules/%s/kernel/net/compat-wireless-openmac/drivers/staging/cw1200/%s.ko
WLAN_SET_DRIVER_MODULE_CORE_NAME := cw1200_core
else
WIFI_DRIVER_MODULE_PATH := /system/lib/modules/%s/extra/%s.ko
endif

WIFI_DRIVER_MODULE_NAME := cw1200_wlan
WIFI_TEST_INTERFACE := wlan0
WLAN_ENABLE_FEATURE_CSPSA := false
WLAN_SET_DUALBAND := false

# ISSW Configuration
ISSW_ENABLE_FEATURE_SIGN_IMAGES ?= false

# XLOADER settings
XLOADER_SET_PLATFORM := u8500

# Documentation settings
REFMAN_ENABLE_FEATURE_REFMAN_GEN := true
REFMAN_SET_UML_EXPORT_XML := $(abspath $(TOP)/vendor/st-ericsson/u8500/uml-export/ste-uml-export.xml)

# CG29XX Configuration
STE_CG29XX_CTRL_ENABLE_FEATURE_STE_CG29XX_CTRL := true

#####################################################################
# Add/remove/etc. files just prior to generating Android's file system images

PATCHTOP := $(BUILD_PATH)/ste_image/fs_patches
APPLY_FS_PATCHES := $(BUILD_PATH)/ste_image/apply_fs_patches.sh

PATCH_VARS += TOPLEVEL=$(realpath $(TOP)) KERNELDIR=kernel INSTALL_MOD_PATH=$(PRODUCT_OUT)/system KERNEL_OUTPUT=$(KERNEL_OUTPUT)

patch-systemimage:
	$(hide) $(APPLY_FS_PATCHES) PATCHDIR=$(PATCHTOP)/system VOLUMEDIR=$(PRODUCT_OUT)/system $(PATCH_VARS)

patch-userdataimage:
	$(hide) $(APPLY_FS_PATCHES) PATCHDIR=$(PATCHTOP)/userdata VOLUMEDIR=$(PRODUCT_OUT)/userdata $(PATCH_VARS)

patch-ramdiskimage:
	$(hide) $(APPLY_FS_PATCHES) PATCHDIR=$(PATCHTOP)/ramdisk VOLUMEDIR=$(PRODUCT_OUT)/ramdisk $(PATCH_VARS)

patch-cacheimage:
	$(hide) $(APPLY_FS_PATCHES) PATCHDIR=$(PATCHTOP)/cache VOLUMEDIR=$(PRODUCT_OUT)/cache $(PATCH_VARS)

#####################################################################

# Create cache.img and misc.img
droidcore: cacheimage miscimage

# Create misc.img
.PHONY: miscimage
miscimage: $(PRODUCT_OUT)/misc.img

$(PRODUCT_OUT)/misc.img:
	dd if=/dev/zero of=$(PRODUCT_OUT)/misc.img bs=$(BOARD_MISCIMAGE_PARTITION_SIZE) count=1

$(PRODUCT_OUT)/kernel: $(PRODUCT_OUT)/zImage
	ln -sf $(notdir $<) $@

# Collect loadmodules and flashkit and place them nicely in a common directory

systemimage: collect-loadmodules

COLLECT_LOADMODULES := $(BUILD_PATH)/ste_image/collect-loadmodules.sh
COLLECT_LOADMODULES_DELIMITER := :

.PHONY: collect-loadmodules
collect-loadmodules: $(PRODUCT_OUT)/system.img $(PRODUCT_OUT)/boot.img $(PRODUCT_OUT)/recovery.img $(PRODUCT_OUT)/misc.img
	$(hide) $(COLLECT_LOADMODULES) PRODUCT_OUT=$(PRODUCT_OUT) TOP=$(TOP) \
	 KERNEL_OUTPUT=$(KERNEL_OUTPUT_RELATIVE) \
	 UBOOT_OUTPUT=$(UBOOT_OUTPUT) \
	 KERNEL_DEFCONFIG=$(KERNEL_DEFCONFIG) \
	 FLASHKIT_INSTALL_PATH=$(FLASHKIT_INSTALL_PATH) \
	 SPLASH_IMAGE_PATH=${UBOOT_SPLASH_IMAGE_OUTPUT} \
	 USB_PC_DRIVERS_SET_DRIVERS=$(USB_PC_DRIVERS_SET_DRIVERS) \
	 DEFAULT_CSPSA_IMAGES=$(subst $(COLLECT_LOADMODULES_DELIMITER) ,$(COLLECT_LOADMODULES_DELIMITER),$(addsuffix $(COLLECT_LOADMODULES_DELIMITER),$(CSPSA_SET_DEFAULT_CSPSA_IMAGES))) \
	 DELIMITER=$(COLLECT_LOADMODULES_DELIMITER)

.PHONY: collect-loadmodules-nodeps
collect-loadmodules-nodeps: systemimage-nodeps
	$(hide) $(COLLECT_LOADMODULES) PRODUCT_OUT=$(PRODUCT_OUT) TOP=$(TOP) \
	 KERNEL_OUTPUT=$(KERNEL_OUTPUT_RELATIVE) \
	 UBOOT_OUTPUT=$(UBOOT_OUTPUT) \
	 KERNEL_DEFCONFIG=$(KERNEL_DEFCONFIG) \
	 FLASHKIT_INSTALL_PATH=$(FLASHKIT_INSTALL_PATH) \
	 SPLASH_IMAGE_PATH=${UBOOT_SPLASH_IMAGE_OUTPUT} \
	 USB_PC_DRIVERS_SET_DRIVERS=$(USB_PC_DRIVERS_SET_DRIVERS) \
	 DEFAULT_CSPSA_IMAGES=$(subst $(COLLECT_LOADMODULES_DELIMITER) ,$(COLLECT_LOADMODULES_DELIMITER),$(addsuffix $(COLLECT_LOADMODULES_DELIMITER),$(CSPSA_SET_DEFAULT_CSPSA_IMAGES))) \
	 DELIMITER=$(COLLECT_LOADMODULES_DELIMITER)

#####################################################################
# Image building targets

SNOWBALL_IMAGE_BASE := snowball-android-$(shell date +%Y%m%d)-custom

### For eMMC

SNOWBALL_EMMC_IMAGE := $(abspath $(TOP))/$(SNOWBALL_IMAGE_BASE)-emmc.img

snowball-emmc:
	cd $(TOP)/vendor/st-ericsson/boot/startupfiles && ./do_image.py -a -o $(SNOWBALL_EMMC_IMAGE)
	@echo
	@echo "Snowball Android Gingerbread eMMC image ready!"
	@echo
	@echo "You can flash it to your device with the riff tool:"
	@echo
	@echo "  sudo riff -f $(SNOWBALL_EMMC_IMAGE)"
	@echo

### For microSD

SNOWBALL_MICROSD_IMAGE := $(abspath $(TOP))/$(SNOWBALL_IMAGE_BASE)-microsd.img

# By default we assume linaro-image-tools are in path
LAMC := linaro-android-media-create

snowball-fixdevice:
	# Change the card we mount filesystems from
	sed -i s/mmcblk0/mmcblk1/ $(PRODUCT_OUT)/root/init.rc

snowball-microsd: snowball-fixdevice systemtarball-nodeps userdatatarball-nodeps boottarball-nodeps
	$(LAMC) --dev snowball_sd --image_file $(SNOWBALL_MICROSD_IMAGE) --system $(PRODUCT_OUT)/system.tar.bz2 --userdata $(PRODUCT_OUT)/userdata.tar.bz2 --boot $(PRODUCT_OUT)/boot.tar.bz2
	@echo
	@echo "Snowball Android Gingerbread microSD image ready!"
	@echo
	@echo "You can write it to a 4GB or larger microSD card with:"
	@echo
	@echo "  sudo dd if=$(SNOWBALL_MICROSD_IMAGE) of=/dev/YOUR_DISK bs=64k"
	@echo
	@echo "NOTE: be very careful when selecting the disk as this command"
	@echo "      will destroy all data on the disk."
