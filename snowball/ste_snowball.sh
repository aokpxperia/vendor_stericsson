#!/system/bin/sh
#
# Copyright (C) ST-Ericsson SA 2011
# Author: Benn Porscke benn.porscke@stericsson.com for ST-Ericsson
# License terms: BSP.
#

# Add proxy if you need it to browse
#setprop net.gprs.http-proxy http://proxyserver:8080

# adb over TCP. Remove if adb over USB
setprop service.adb.tcp.port 5555
stop adbd
start adbd

# Settings for Snowball. 
sqlite3 /data/data/com.android.providers.settings/databases/settings.db "INSERT INTO system (name, value) VALUES ('stay_on_while_plugged_in', 3);"
sqlite3 /data/data/com.android.providers.settings/databases/settings.db "INSERT INTO system (name, value) VALUES ('screen_off_timeout', -1);"
sqlite3 /data/data/com.android.providers.settings/databases/settings.db "INSERT INTO system (name, value) VALUES ('accelerometer_rotation', 0);"

