/*
 * Thermal Service Manager
 *
 * List of built in mitigation actions. Each action needs to
 * be independent.
 *
 * Copyright (C) ST-Ericsson SA 2011. All rights reserved.
 * This code is ST-Ericsson proprietary and confidential.
 * Any use of the code for whatever purpose is subject to
 * specific written permission of ST-Ericsson SA.
 */
#include <sys/reboot.h>

#include <sys/types.h>
#include <sys/stat.h>

#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>

#include "config.h"
#include "log.h"

#define BATTERY_CHARGE_CONTROL "/sys/ab8500_chargalg/chargalg"

/* setting cpu0 also sets the same limits for cpu1 */
#define CPUFREQ_MIN_NODE "/sys/devices/system/cpu/cpu0/cpufreq/scaling_min_freq"
#define CPUFREQ_MAX_NODE "/sys/devices/system/cpu/cpu0/cpufreq/scaling_max_freq"

/* defines the sysfs nodes to enable/disable the STE usecase governor */
#define UCG_CONTROL_NODE "/sys/devices/system/cpu/usecase/enable"
#define UCG_ENABLE_STRING "1"
#define UCG_DISABLE_STRING "0"

/* CPU freq levels defined in kernel/arch/arm/mach-ux500/pm/cpufreq-db8500.c */
#define CPU_MINLEVEL_MHZ "200000"
#define CPU_PERFLEVEL1_MHZ "1000000"
#define CPU_PERFLEVEL2_MHZ "800000"
#define CPU_PERFLEVEL3_MHZ "400000"
#define CPU_PERFLEVEL4_MHZ CPU_MINLEVEL_MHZ

static void setcpufreq(char *freq);
static void disableucg(void);
static void enableucg(void);

void action_shutdown(void) {
	int timeout;

	timeout = config_getshutdowntimeout();

	INF("forcing platform shut down in %d seconds...\n", timeout);
	sleep(timeout);

	sync();
	reboot(RB_POWER_OFF);
}

/* set's cpu at full and enables the use case governor
 * to scale as needed */
void action_cpuperflevel1(void) {
	setcpufreq(CPU_PERFLEVEL1_MHZ);
	enableucg();
}

void action_cpuperflevel2(void) {
	disableucg();
	setcpufreq(CPU_PERFLEVEL2_MHZ);
}

void action_cpuperflevel3(void) {
	disableucg();
	setcpufreq(CPU_PERFLEVEL3_MHZ);
}

void action_cpuperflevel4(void) {
	disableucg();
	setcpufreq(CPU_PERFLEVEL4_MHZ);
}

void action_stopbatterycharge(void) {
	char disable_all_charge[] = "0\n";
	int fd;

	INF("disabling battery charging...\n");

	fd = open(BATTERY_CHARGE_CONTROL, O_WRONLY);
	if (fd < 0) {
		ERR("unable to open %s\n", BATTERY_CHARGE_CONTROL);
		return;
	}

	if (write(fd, disable_all_charge, strlen(disable_all_charge) + 1) == -1) {
		ERR("failed to write to battery controller\n");
	}

	close(fd);
}

void action_startbatterycharge(void) {
	/* values from kernel/drivers/power/ab8500_chargalg.c */
	const char enable_ac_charge[] = "1\n";
	const char enable_usb_charge[] = "2\n";
	int fd;

	INF("enabling battery charging...\n");

	fd = open(BATTERY_CHARGE_CONTROL, O_WRONLY);
	if (fd < 0) {
		ERR("unable to open %s\n", BATTERY_CHARGE_CONTROL);
		return;
	}

	if (write(fd, enable_ac_charge, strlen(enable_ac_charge) + 1) == -1) {
		ERR("failed to write to battery controller\n");
		close(fd);
		return;
	}

	if (write(fd, enable_usb_charge, strlen(enable_usb_charge) + 1) == -1) {
		ERR("failed to write to battery controller\n");
	}

	close(fd);
}

/*
 * sets cpu to specific OPP
 */
static void setcpufreq(char *freq) {
	int fd;

	INF("setting cpu speeds: max=%s min=%s\n", freq, CPU_MINLEVEL_MHZ);

	/* According to kernel/Documentation/cpu-freq/user-guide.txt we must
	 * first set the max_freq before setting the min... */

	/* set max freq */
	fd = open(CPUFREQ_MAX_NODE, O_WRONLY);
	if (fd < 0) {
		ERR("unable to open %s\n", CPUFREQ_MAX_NODE);
		return;
	}
	if (write(fd, freq, strlen(freq) + 1) == -1) {
		ERR("failed to write to cpufreq max node\n");
	}
	close(fd);

	/* set min freq */
	fd = open(CPUFREQ_MIN_NODE, O_WRONLY);
	if (fd < 0) {
		ERR("unable to open %s\n", CPUFREQ_MAX_NODE);
		return;
	}
	if (write(fd, CPU_MINLEVEL_MHZ, strlen(CPU_MINLEVEL_MHZ) + 1) == -1) {
		ERR("failed to write to cpufreq min node\n");
	}
	close(fd);
}

/*
 * Renabled use case governor, putting board back into normal cpufreq
 * opperation
 */
static void enableucg(void) {
	int fd;

	fd = open(UCG_CONTROL_NODE, O_WRONLY);
	if (fd < 0) {
		ERR("unable to open %s\n", UCG_CONTROL_NODE);
		return;
	}

	if (write(fd, UCG_ENABLE_STRING, strlen(UCG_ENABLE_STRING) + 1) == -1) {
		ERR("failed to write enable use case governor\n");
	}
	close(fd);
}

/*
 * Disables the use case governor to prevent cpufreq being modified
 * while thermal actions are ongoing
 */
static void disableucg(void) {
	int fd;

	fd = open(UCG_CONTROL_NODE, O_WRONLY);
	if (fd < 0) {
		ERR("unable to open %s\n", UCG_CONTROL_NODE);
		return;
	}

	if (write(fd, UCG_DISABLE_STRING, strlen(UCG_DISABLE_STRING) + 1) == -1) {
		ERR("failed to write disable use case governor\n");
	}
	close(fd);
}
