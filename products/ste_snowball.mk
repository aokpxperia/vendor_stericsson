
# Superclass
$(call inherit-product, build/target/product/generic.mk)

# Overrides
PRODUCT_NAME := ste_snowball
PRODUCT_DEVICE := snowball
PRODUCT_MANUFACTURER := ste
PRODUCT_BOARD := snowball
PRODUCT_LOCALES := \
	en_US \
	en_GB \
	en_CA \
	en_AU \
	en_NZ \
	en_SG \
	ja_JP \
	fr_FR \
	fr_BE \
	fr_CA \
	fr_CH \
	it_IT \
	it_CH \
	es_ES \
	de_DE \
	de_AT \
	de_CH \
	de_LI \
	nl_NL \
	nl_BE \
	cs_CZ \
	pl_PL \
	zh_CN \
	zh_TW \
	ru_RU \
	ko_KR \
	hdpi

PRODUCT_PACKAGE_OVERLAYS := vendor/st-ericsson/snowball/overlay
PRODUCT_PROPERTY_OVERRIDES := \
 ro.com.google.clientidbase=android-ste \
 ro.com.google.gmsversion=2.3_r5

## Product configration flags ##

# Enable features for signing verification needed on production fused hw
ENABLE_FEATURE_SIGNATURE_VERIFICATION ?= false

## end Product configuration flags ##

# Add packages to build.
# Module names for lights, sensors, gps, gralloc, overlay and copybit should always be
# hard coded to match TARGET_BOARD_PLATFORM in vendor/st-ericsson/snowball/BoardConfig.mk
PRODUCT_PACKAGES += STEBluetooth \
                    LiveWallpapersPicker \
                    LiveWallpapers \
                    MagicSmokeWallpapers \
                    VisualizationWallpapers \
                    libRS \
                    librs_jni \
                    lights.$(PRODUCT_BOARD) \
                    copybit.snowball \
                    STMPE-keypad.kcm \
                    tc35893-kp.kcm \
                    ux500-ske-keypad.kcm \
                    ux500-ske-keypad-qwertz.kcm \
                    cat \
                    libapdu \
                    libpc \
                    tapdu \
                    tcat \
                    tuicc \
                    uicc \
                    libmsl \
                    libcn \
                    model_id.cfg \
                    libisimessage \
                    libmalcs \
                    libmalgpds \
                    libmalgss \
                    libmalmce \
                    libmalmis \
                    libmalnet \
                    libmalnvd \
                    libmalpipe \
                    libmalsms \
                    libmaluicc \
                    libphonet \
                    libmal \
                    lbsd \
                    libclientgps \
                    gps.snowball \
					liblcsclient \
                    libamilclient \
                    lbsstelp \
                    liblbstestengine \
                    libstecom \
                    libmpl \
                    libnlcom \
                    libsterc \
                    stercd \
                    ip \
                    gen_sipc \
                    libcamera \
                    libmmhwbuffer_wrapper \
                    libste_adm \
                    ste_adm_server \
                    nwm2gdf \
                    gdflist \
                    gdf2pff \
                    pff2cspsa \
                    cspsalist \
                    cspsa2nwm \
                    cspsa2qstore \
                    STEBluetooth \
                    libnl \
                    gralloc.snowball \
                    overlay.snowball \
                    msa \
                    libril-anm \
                    libaudio \
                    libaudiopolicy \
                    libasound \
                    crda \
                    intersect \
                    regdbdump \
                    regulatory.bin \
                    85-regulatory.rules \
                    cspsa-server \
                    libcspsa \
                    cspsa.conf \
                    cspsa-cmd \
                    libcspsa-core \
                    libcspsa-ll-file \
                    sensors.$(PRODUCT_BOARD) \
                    libcalcdigest \
                    libverify \
                    libtee \
                    sap \
                    libsap_pts \
                    libsbc \
                    libasound_module_pcm_bluetooth \
                    libasound_module_ctl_bluetooth \
                    wfa_ca \
                    wfa_dut \
                    libwfa \
                    libwfa_ca \
                    libwfa_dut \
                    Stk \
                    install_flash_player \
                    libflashplayer.so \
                    libysshared.so \
                    libstagefright_froyo.so \
                    libmalmon \
                    libmalmte \
                    libmlr \
                    libthermal \
                    ThermalService \
                    libstagefrighthw \
                    uImage \
                    u-boot.bin \
                    u-boot-env.bin \
                    st-ericsson-multimedia-package \
                    cw1200_wlan.ko \
                    kernel-firmware \
                    smcl_ta.ssw \
                    libarchive \
                    libarchive_fe \
                    bsdtar \
                    chargemode \
                    factoryreset \
                    hdmi_service_st \
                    hdmid \
                    battery_params \
                    FmRadioReceiver \
                    FmRadioTransmitter \
                    sensors.$(PRODUCT_BOARD).so \
                    gps.snowball.so \
                    copybit.snowball.so \
                    gralloc.snowball.so \
                    overlay.snowball.so \
                    libstagefrighthw.so \
                    ste-cg29xx_ctrl \
                    busybox \
                    BooksPhone \
                    GenieWidget \
                    Gmail \
                    GoogleBackupTransport \
                    GoogleCalendarSyncAdapter \
                    GoogleContactsSyncAdapter \
                    GoogleFeedback \
                    GooglePartnerSetup \
                    GoogleServicesFramework \
                    GoogleQuickSearchBox \
                    Maps \
                    MediaUploader \
                    NetworkLocation \
                    SetupWizard \
                    Street \
                    Talk \
                    Talk2 \
                    Vending \
                    MarketUpdater \
                    VoiceSearch \
                    YouTube \
                    com.google.android.maps.jar \
                    com.google.android.maps.xml \
                    google_generic_update.txt \
                    libtalk_jni \
                    libvoicesearch \
                    libhdmi_service \
                    libblt_hw

# FM Radio library name is dependent on whether it is RX+TX/RX/TX
# set one of these to true to get RX or TX only
FMRADIO_CG2900_SET_TX_ONLY := false
FMRADIO_CG2900_SET_RX_ONLY := false

ifeq ($(FMRADIO_CG2900_SET_RX_ONLY), true)
	PRODUCT_PACKAGES += libfmradio.cg2900_rx
else
ifeq ($(FMRADIO_CG2900_SET_TX_ONLY), true)
	PRODUCT_PACKAGES += libfmradio.cg2900_tx
else
	PRODUCT_PACKAGES += libfmradio.cg2900
endif
endif

# Pick up some sounds
include frameworks/base/data/sounds/OriginalAudio.mk

# Added resources for TextToSpeech utility.
include external/svox/pico/lang/all_pico_languages.mk

# Copy the blue-up.sh script to system/bin
PRODUCT_COPY_FILES += \
	vendor/st-ericsson/snowball/blue-up.sh:system/bin/blue-up.sh \
    vendor/st-ericsson/snowball/init.rc:root/init.rc \
	vendor/st-ericsson/snowball/init_bd_addr.sh:system/etc/init_bd_addr.sh \
	vendor/st-ericsson/snowball/media_profiles_snowball.xml:system/etc/media_profiles.xml

# CG2900 initialization script
PRODUCT_COPY_FILES += \
	vendor/st-ericsson/snowball/cg2900-channel_init.sh:system/bin/cg2900-channel_init.sh

# USB Device ID initialization script
PRODUCT_COPY_FILES += \
	vendor/st-ericsson/snowball/usbid_init.sh:system/bin/usbid_init.sh

# copy AGPS configuration files for lbs_core module
PRODUCT_COPY_FILES += \
	vendor/st-ericsson/snowball/LbsConfig.cfg:system/etc/LbsConfig.cfg \
	vendor/st-ericsson/snowball/LbsLogConfig.cfg:system/etc/LbsLogConfig.cfg \
	vendor/st-ericsson/snowball/LbsPgpsConfig.cfg:system/etc/LbsPgpsConfig.cfg \
	vendor/st-ericsson/snowball/cacert.txt:system/etc/cacert.txt \
	vendor/st-ericsson/snowball/LbsPltConfig.cfg:system/etc/LbsPltConfig.cfg

# copy dhcpcd configuration file with eth0 info
PRODUCT_COPY_FILES += \
	vendor/st-ericsson/snowball/dhcpcd.conf:system/etc/dhcpcd/dhcpcd.conf

# Added platform feature permissions
PRODUCT_COPY_FILES += \
        frameworks/base/data/etc/android.hardware.touchscreen.multitouch.distinct.xml:system/etc/permissions/android.hardware.touchscreen.multitouch.distinct.xml \
        frameworks/base/data/etc/handheld_core_hardware.xml:system/etc/permissions/handheld_core_hardware.xml \
        frameworks/base/data/etc/android.hardware.camera.autofocus.xml:system/etc/permissions/android.hardware.camera.autofocus.xml \
        frameworks/base/data/etc/android.hardware.camera.flash-autofocus.xml:system/etc/permissions/android.hardware.camera.flash-autofocus.xml \
        frameworks/base/data/etc/android.hardware.camera.front.xml:system/etc/permissions/android.hardware.camera.front.xml \
        frameworks/base/data/etc/android.hardware.sensor.gyroscope.xml:system/etc/permissions/android.hardware.sensor.gyroscope.xml \
        frameworks/base/data/etc/android.hardware.sensor.light.xml:system/etc/permissions/android.hardware.sensor.light.xml \
        frameworks/base/data/etc/android.hardware.sensor.proximity.xml:system/etc/permissions/android.hardware.sensor.proximity.xml \
        frameworks/base/data/etc/android.hardware.telephony.gsm.xml:system/etc/permissions/android.hardware.telephony.gsm.xml \
        frameworks/base/data/etc/android.hardware.location.gps.xml:system/etc/permissions/android.hardware.location.gps.xml \
        frameworks/base/data/etc/android.hardware.wifi.xml:system/etc/permissions/android.hardware.wifi.xml \
        packages/wallpapers/LivePicker/android.software.live_wallpaper.xml:system/etc/permissions/android.software.live_wallpaper.xml \
        frameworks/base/data/etc/com.stericsson.hardware.fm.receiver.xml:system/etc/permissions/com.stericsson.hardware.fm.receiver.xml \
        frameworks/base/data/etc/com.stericsson.hardware.fm.transmitter.xml:system/etc/permissions/com.stericsson.hardware.fm.transmitter.xml

# Copy ALSA configuration files to rootfs
PRODUCT_COPY_FILES += \
	external/alsa-lib/src/conf/alsa.conf:system/usr/share/alsa/alsa.conf \
	external/alsa-lib/src/conf/cards/aliases.conf:system/usr/share/alsa/cards/aliases.conf \
	external/alsa-lib/src/conf/pcm/center_lfe.conf:system/usr/share/alsa/pcm/center_lfe.conf \
	external/alsa-lib/src/conf/pcm/default.conf:system/usr/share/alsa/pcm/default.conf \
	external/alsa-lib/src/conf/pcm/dmix.conf:system/usr/share/alsa/pcm/dmix.conf \
	external/alsa-lib/src/conf/pcm/dpl.conf:system/usr/share/alsa/pcm/dpl.conf \
	external/alsa-lib/src/conf/pcm/dsnoop.conf:system/usr/share/alsa/pcm/dsnoop.conf \
	external/alsa-lib/src/conf/pcm/front.conf:system/usr/share/alsa/pcm/front.conf \
	external/alsa-lib/src/conf/pcm/iec958.conf:system/usr/share/alsa/pcm/iec958.conf \
	external/alsa-lib/src/conf/pcm/modem.conf:system/usr/share/alsa/pcm/modem.conf \
	external/alsa-lib/src/conf/pcm/rear.conf:system/usr/share/alsa/pcm/rear.conf \
	external/alsa-lib/src/conf/pcm/side.conf:system/usr/share/alsa/pcm/side.conf \
	external/alsa-lib/src/conf/pcm/surround40.conf:system/usr/share/alsa/pcm/surround40.conf \
	external/alsa-lib/src/conf/pcm/surround41.conf:system/usr/share/alsa/pcm/surround41.conf \
	external/alsa-lib/src/conf/pcm/surround50.conf:system/usr/share/alsa/pcm/surround50.conf \
	external/alsa-lib/src/conf/pcm/surround51.conf:system/usr/share/alsa/pcm/surround51.conf \
	external/alsa-lib/src/conf/pcm/surround71.conf:system/usr/share/alsa/pcm/surround71.conf

# Copy the U-Boot splash screen
# If the U-boot splash image output path need is changed, the UBOOT_SPLASH_IMAGE_OUTPUT variable
# in BoardConfig.mk (vendor/st-ericsson/uxx00) also needs to be updated
PRODUCT_COPY_FILES += \
        vendor/st-ericsson/boot/u-boot/tools/logos/stericsson.bmp:splash.bin

# keyboard layouts
PRODUCT_COPY_FILES += \
	vendor/st-ericsson/snowball/STMPE-keypad.kl:system/usr/keylayout/STMPE-keypad.kl \
	vendor/st-ericsson/snowball/STMPE-keypad.kl:system/usr/keylayout/tc35893-kp.kl \
	vendor/st-ericsson/snowball/ux500-ske-keypad.kl:system/usr/keylayout/ux500-ske-keypad.kl \
	vendor/st-ericsson/snowball/ux500-ske-keypad-qwertz.kl:system/usr/keylayout/ux500-ske-keypad-qwertz.kl \
	vendor/st-ericsson/snowball/AB8500_Hs_Button.kl:system/usr/keylayout/AB8500_Hs_Button.kl

# product specific ueventd.rc
PRODUCT_COPY_FILES += \
	vendor/st-ericsson/snowball/ueventd.st-ericsson.rc:root/ueventd.st-ericsson.rc

# Snowball specific initialization script
PRODUCT_COPY_FILES += \
	vendor/st-ericsson/snowball/ste_snowball.sh:system/etc/ste_snowball.sh

# DBus config
PRODUCT_COPY_FILES += vendor/st-ericsson/snowball/dbus.conf:system/etc/dbus.conf

# Automount SD-Card
PRODUCT_COPY_FILES += \
	vendor/st-ericsson/snowball/vold.fstab:system/etc/vold.fstab

# boot commands for uboot (when booting from SD card)
PRODUCT_COPY_FILES += \
	vendor/st-ericsson/snowball/boot.txt:root/boot.txt

