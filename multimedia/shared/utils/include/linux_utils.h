/*
 * Copyright (C) ST-Ericsson SA 2010. All rights reserved.
 * This code is ST-Ericsson proprietary and confidential.
 * Any use of the code for whatever purpose is subject to
 * specific written permission of ST-Ericsson SA.
 */

#ifndef _LINUX_UTILS_H_
#define _LINUX_UTILS_H_

/** Abstract the API between Android and Linux environment for various usefull services */

/********************************************************************************
 * LOG services using Android API and providing an implementation for Linux:
 * LOG_TAG CPP macro must define the String to use
 */

#ifndef LOG_TAG
#define LOG_TAG "?"
#endif

#if defined(ANDROID) && !defined(ENABLE_FEATURE_BUILD_HATS)
#include <cutils/log.h>
#else
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define LOGE(...) printf("E/" LOG_TAG ": " __VA_ARGS__)
#define LOGI(...) printf("I/" LOG_TAG ": " __VA_ARGS__)
#define LOGD(...) printf("D/" LOG_TAG ": " __VA_ARGS__)
#define LOGV(...) printf("V/" LOG_TAG ": " __VA_ARGS__)
#define LOGW(...) printf("W/" LOG_TAG ": " __VA_ARGS__)
#endif // ANDROID

/********************************************************************************
 * property services using Android API and providing an implementation for Linux:
 * using getenv
 */

#ifdef ANDROID
#include <cutils/properties.h>
#endif

#ifndef PROPERTY_KEY_MAX
#define PROPERTY_KEY_MAX   32
#endif

#ifndef PROPERTY_VALUE_MAX
#define PROPERTY_VALUE_MAX  92
#endif

#ifdef ANDROID
#define GET_PROPERTY(key, value, default_value) \
    char value[PROPERTY_VALUE_MAX];		\
    property_get(key, value, default_value);
#else
#define GET_PROPERTY(key, value, default_value)			\
    char * value = getenv(key);					\
    if (value == NULL) {					\
	value = (char *) default_value;				\
    }
#endif

#ifdef ANDROID
#define SET_PROPERTY(key, value)				\
    property_set((const char *) key, (const char *) value);
#else
#define SET_PROPERTY(key, value)				\
    setenv((const char *) key, (const char *) value, 1);
#endif


#endif /* _LINUX_UTILS_H_ */


