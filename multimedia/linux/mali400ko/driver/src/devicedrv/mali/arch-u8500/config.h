/*
 * Copyright (C) ST-Ericsson 2010
 *
 * Architecture configuration for ST-Ericsson U8500 platform
 *
 * Author: Magnus Wendt <magnus.wendt@stericsson.com>
 */

/*
 * Copyright (C) 2010 ARM Limited. All rights reserved.
 *
 * This program is free software and is provided to you under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation, and any use by you of this program is subject to the terms of such GNU licence.
 *
 * A copy of the licence is included with the program, and can also be obtained from Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef __ARCH_CONFIG_H__
#define __ARCH_CONFIG_H__

/* #include <mach/hardware.h> */
#define U8500_SGA_BASE 0xa0300000
#define MEGABYTE (1024*1024)
#define MALI_MEM_BASE_END (256 * MEGABYTE)
#define MALI_MEM_SIZE (32 * MEGABYTE)
#define OS_MEM_SIZE (96 * MEGABYTE)

/* Hardware revision v1: GX570-BU-00000-r0p1
 * Hardware revision v2: GX570-BU-00000-r1p0
 * configuration registers: 0xA0300000-0xA031FFFFF  (stw8500v1_usermanual p269)
 *
 * Shared Peripheral Interrupt assignments:     (stw8500v1_usermanual p265-266)
 * Nb  | Interrupt Source
 * 116 | Mali400 combined
 * 115 | Mali400 geometry processor
 * 114 | Mali400 geometry processor MMU
 * 113 | Mali400 pixel processor
 * 112 | Mali400 pixel processor MMU
 *
 * irq offset: 32
 */

static _mali_osk_resource_t arch_configuration [] =
{
	{
		.type = MALI400GP,
		.description = "Mali-400 GP",
		.base = U8500_SGA_BASE + 0x0000,
		.irq = 115+32,
		.mmu_id = 1
	},
	{
		.type = MALI400PP,
		.base = U8500_SGA_BASE + 0x8000,
		.irq = 113+32,
		.description = "Mali-400 PP",
		.mmu_id = 2
	},
#if USING_MMU
	{
		.type = MMU,
		.base = U8500_SGA_BASE + 0x3000,
		.irq = 114+32,
		.description = "Mali-400 MMU for GP",
		.mmu_id = 1
	},
	{
		.type = MMU,
		.base = U8500_SGA_BASE + 0x4000,
		.irq = 112+32,
		.description = "Mali-400 MMU for PP",
		.mmu_id = 2
	},
#endif
	{
		.type = MEMORY,
		.description = "Mali SDRAM",
		.alloc_order = 0, /* Highest preference for this memory */
		.base = MALI_MEM_BASE_END - MALI_MEM_SIZE,
		.size = MALI_MEM_SIZE,
		.flags = _MALI_CPU_WRITEABLE | _MALI_CPU_READABLE | _MALI_PP_READABLE | _MALI_PP_WRITEABLE |_MALI_GP_READABLE | _MALI_GP_WRITEABLE
	},
#if USING_OS_MEMORY
	{
		.type = OS_MEMORY,
		.description = "Linux kernel memory",
		.alloc_order = 5, /* Medium preference for this memory */
		.size = OS_MEM_SIZE,
		.flags = _MALI_CPU_WRITEABLE | _MALI_CPU_READABLE | _MALI_MMU_READABLE | _MALI_MMU_WRITEABLE
	},
#endif
	{
		.type = MEM_VALIDATION,
		.description = "Framebuffer",
		.base = 0x00000000,
		.size = (1024 * MEGABYTE), /* Validate all memory for now. (HREF+ has 512MB)*/
		.flags = _MALI_CPU_WRITEABLE | _MALI_CPU_READABLE | _MALI_PP_WRITEABLE | _MALI_PP_READABLE
	},
	{
		.type = MALI400L2,
		.base = U8500_SGA_BASE + 0x1000,
		.description = "Mali-400 L2 cache"
	},
};

#endif /* __ARCH_CONFIG_H__ */
